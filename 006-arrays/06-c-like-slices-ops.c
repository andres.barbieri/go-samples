#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define A_LEN   10
#define uint    unsigned int


int *dupslice(int s[], uint nmemb, uint *nn) {
  *nn = (nmemb+1)*2;
  int *aux = calloc((*nn), sizeof(int));
  for(int i=0;i<nmemb;i++) {aux[i] = s[i];};
  return aux;
}

int *dupslice_and_fill(int s[], uint nmemb, uint *nn, int fill) {
  *nn = (nmemb+1)*2;
  int *aux = calloc((*nn), sizeof(int));
  for(int i=0;i<nmemb;i++) {aux[i] = s[i];};
  for(int i=nmemb;i<(*nn);i++) {aux[i] = fill;};
  return aux;
}

int *fastdupslice(int s[], uint nmemb, uint *nn) {
  *nn = (nmemb+1)*2;  
  int *aux = calloc((*nn), sizeof(int)); // +1 in case cap(s) == 0
  memcpy(aux, s, sizeof(int)*nmemb);
  return aux;
}

int *append(int s[], uint nmemb, uint *nn, int elem) {
  *nn = (nmemb+1);  
  int *aux = calloc((*nn), sizeof(int)); // +1 
  memcpy(aux, s, sizeof(int)*nmemb);
  aux[nmemb] = elem;
  return aux;
}

void println_array(int s[], uint nmemb) {
  printf("[");
  for(int i=0;i<nmemb;i++) {printf("%d,",s[i]);};
  printf("]\n");
}

int main() {
  // Array
  int a[A_LEN] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};   /* Full init literal array*/
  uint ls1,ls2,ls3,ls4;

  int *s1 = dupslice(a,A_LEN,&ls1);
  int *s2 = fastdupslice(a,A_LEN,&ls2);
  int *s3 = dupslice_and_fill(a,A_LEN,&ls3,1);
  int *s4 = append(s3,ls3,&ls4,10000);  
  printf("a=%d=",A_LEN);println_array(a,A_LEN);
  printf("s1=%d=",ls1);println_array(s1,ls1);
  printf("s2=%d=",ls2);println_array(s2,ls2);
  printf("s3=%d=",ls3);println_array(s3,ls3);
  printf("s4=%d=",ls4);println_array(s4,ls4);    
  free(s1);free(s2);free(s3);free(s4);
}
