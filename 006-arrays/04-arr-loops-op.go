package main

import "fmt"



const A_LEN   = 10
const B_LEN   = 10

func min(x, y int) int {
	if x > y {
		return y
	}
	return x
}

func main() {
	a := [A_LEN]int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}   
	b := [...]int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12}    
        c := [B_LEN]int{0, 1, 2} 
	var d []int           // Slice
	e := []int{1,2}       // Slice
	
	fmt.Println("lens ",len(a),len(b),len(c),len(d),len(e))
	// Array loops
	fmt.Printf("##\n");  for i:=0;i<A_LEN;i++ {  fmt.Printf("a[%d]=%d,",i,a[i]) }
	fmt.Println()
	fmt.Printf("##\n");  for i:=0;i<len(a);i++ {  fmt.Printf("a[%d]=%d,",i,a[i]) }
	fmt.Println()
	fmt.Printf("##\n");  fmt.Println("a=",a)
	fmt.Printf("##\n");  for i:=A_LEN-1;i>=0;i-- { fmt.Printf("a[%d]=%d,",i,a[i]) }	
	fmt.Println()
	fmt.Printf("##\n");  for i:=len(a)-1;i>=0;i-- { fmt.Printf("a[%d]=%d,",i,a[i]) }
	fmt.Println()	
	fmt.Printf("##\n");  for i, value := range b { fmt.Printf("b[%d]=%d,",i,value) }
	fmt.Println()
	// b = a + a2  // invalid operation: operator + not defined on a (variable of type [10]int)
	fmt.Printf("##\n");  for _, value := range b { fmt.Printf("%d,",value) }
	for i:=0;i< min(len(a), len(b));i++ { c[i] = a[i] + b[i] }
	fmt.Println()	
	fmt.Printf("##\n");  fmt.Println("c=a+b",c,"=",a,"+",b)
	sum := 0
	fmt.Printf("##\n");  for i:=0;i< len(c);i++ { sum+=c[i] }
	fmt.Println("sum = ",sum)
        // append not allowed over arrays	
	//append(a,10) // first argument to append must be a slice; have a (variable of type [10]int)
}
