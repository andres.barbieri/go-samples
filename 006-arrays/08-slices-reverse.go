package main

import "fmt"

func reverse(s []int) []int {
    r := s
    for i, j := 0, len(r)-1; i < len(r)/2; i, j = i+1, j-1 {
        r[i], r[j] = r[j], r[i]
    }
    return r
}

func reverse_no_ret(s []int) {
	for i, j := 0, len(s)-1; i < j; i, j = i+1, j-1 {
		s[i], s[j] = s[j], s[i]
	}
}


func reverse2(s []int) []int {
	r := s
	for i:=0; i<(len(r)/2);i++ {
	aux := r[i]
        r[i] = r[len(r)-i-1]
	r[len(r)-i-1] = aux
	}
	return r
}

func reverse_rec(s []int) {
	var aux int
	if (len(s) > 1) {
		aux = s[0]
		s[0] = s[len(s)-1]
		s[len(s)-1] = aux
		reverse(s[1:len(s)-1])
	}
}

func main() {
	a := [...]int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 }
	b := a // array copy
	c := a[:] // reference
	
	fmt.Println(a)
	fmt.Println(b)
	fmt.Println(c)	
	reverse_no_ret(a[:])
	fmt.Println(a)
	fmt.Println(b)
	fmt.Println(c)	
	fmt.Println(reverse2(a[:]))
	reverse_rec(a[:])
        fmt.Println(a)
	fmt.Println(reverse(a[:]))
	fmt.Println(reverse(a[1:1]))
	reverse(a[0:1])
	reverse(a[0:2]) // swap first 2 elements	
	reverse(a[len(a)/2:len(a)/2+2]) // swap middle 	
        fmt.Println(a)
}
	
	
