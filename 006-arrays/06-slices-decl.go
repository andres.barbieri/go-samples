// Array: sequence of elements of a single type with a fixed length
// Slice: ptr to array spaces
// https://go.dev/blog/slices-intro

package main

import "fmt"
import "unsafe"

const A_LEN   = 10
const B_LEN   = 10


func main() {
	// Arrays
	a := [A_LEN]int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}   /* Array Full init literal array */
	b := [B_LEN]int{0, 1, 2 }                       
	c := [...]int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}     

	// Slices
	s1 := []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9} 
	s2 := []int{0, 1, 2}
	s3 := []int{}         // empty slice
	var s4 []int          // nil slice
	//var s5 []int = {0,1,2} //syntax error: unexpected {, expected expression
	var s5 = []int{0,1,2} 
	
        fmt.Println(len(a),len(b),len(c),"-",cap(a),cap(b),cap(c))
        fmt.Println(len(s1),len(s2),len(s3),len(s4),len(s5),"-",cap(s1),cap(s2),cap(s3),cap(s4),cap(s5))
	fmt.Println(unsafe.Sizeof(a),unsafe.Sizeof(b),unsafe.Sizeof(c))
	fmt.Println(unsafe.Sizeof(s1),unsafe.Sizeof(s2),unsafe.Sizeof(s3),unsafe.Sizeof(s4))
}
