package main

import "fmt"


const TOP = 2

func print(a  [TOP]int) {
	fmt.Print("[")
	for _,v := range(a) {
		fmt.Print(v)
		fmt.Print(" ")
	}
	fmt.Println("]")
}

func prints(a  []int) {
	fmt.Print("[")
	for _,v := range(a) {
		fmt.Print(v)
		fmt.Print(" ")
	}
	fmt.Println("]")
}


// Copy parameter
func read_wrong(a  [TOP]int) {
	for i,_ := range(a) {
		fmt.Scanln(&(a[i]))
	}
	fmt.Print("inside");print(a)
}

// v is a copy 
func read_wrong2(a  *[TOP]int) {
	for _,v := range(a) {
		fmt.Scanln(&v)
	}
	fmt.Print("inside");print(*a)
}

// Reference parameter
func read(a  *[TOP]int) {
	for i,_ := range(a) {		
		fmt.Scanln(&(a[i]))
	}
	fmt.Print("inside");print(*a)
}

func reads(a  []int) {
	for i,_ := range(a) {
		fmt.Scanln(&(a[i]))
	}
	fmt.Print("inside");prints(a)
}
func main() {
	var a [TOP]int
	read_wrong(a)
	print(a)
	read_wrong2(&a)
	prints(a[:])
	read(&a)
	print(a)
	reads(a[:])
	prints(a[:])
	
}

// https://stackoverflow.com/questions/38731467/pass-array-by-reference-in-golang
