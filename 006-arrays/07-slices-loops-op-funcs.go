package main

import "fmt"


const LEN   = 10

func min(x, y int) int {
	if x > y {
		return y
	}
	return x
}

func sum(a, b []int) []int {
        aux := make([]int,min(len(a),len(b))) 
	for i:=0;i<len(aux);i++ { aux[i] = a[i] + b[i] }
	return aux
}

func avg(a []int) int {
        sum := 0
	
	for i:=0;i< len(a);i++ { sum+=a[i] }
	return sum/len(a)
}

func avg2(a []int) int {
        sum := 0
	if (len(a)==0) { return 0 }		
	for element := range a { sum+=element }
	return sum/len(a)
}

func main() {
	a := [LEN]int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
        b := [LEN]int{0, 1, 2}
	c := [...]int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	
	fmt.Println(a,b,c)
	fmt.Println(sum(a[:],b[:]))
	fmt.Println(sum(a[:5],b[:]))
	
	e := sum(a[1:8],c[1:8])
	fmt.Println("e =",e)
 	
	fmt.Println(sum(c[:],c[:])) 
	fmt.Println(avg(a[:]))
	fmt.Println(avg2(a[:]))	
}
