package main

import "fmt"

func reverse(s []int) []int {
    r := s
    for i, j := 0, len(r)-1; i < len(r)/2; i, j = i+1, j-1 {
        r[i], r[j] = r[j], r[i]
    }
    return r
}

func main() {
	a := [...]int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 }
	var y []int
	var z []int
	
	fmt.Println(a)
	//z = append(a[:],reverse(a[:])...) // side effect
	//fmt.Println(z)
	y = make([]int, len(a[:]))
	copy(y,a[:])
	z = append(a[:],reverse(y)...)
	fmt.Println(y)			
	fmt.Println(a)		
	fmt.Println(z)
	z =  append(z,a[:]...)
	z =  append(z,1000,1001,1002,1003)	
	fmt.Println(z)	
}
	
	
