// Array: sequence of elements of a single type with a fixed length
// Slice: ptr to array spaces 
// https://go.dev/blog/slices-intro
// Slice built-in ops:
// len, cap, make, copy, append, range[x:y]

/****
  built-in funcs:
           func cap(v Type) int  https://pkg.go.dev/builtin#cap
           func len(v Type) int  https://pkg.go.dev/builtin#len
           func copy(dst, src []T) int
           func make([]T, len, cap) []T
           func append(s []T, x ...T) []T
****/

package main

import "fmt"

const A_LEN   = 10
const B_LEN   = 10


func dupslice(s []int) []int {
	aux := make([]int, len(s), (cap(s)+1)*2) // +1 in case cap(s) == 0
	for i := range s {aux[i] = s[i]}
	return aux
}

func dupslice_and_fill(s []int, x int) []int {
	aux := make([]int, (cap(s)+1)*2)  // +1 in case cap(s) == 0
	for i := range s {aux[i] = s[i]}
	for i :=len(s);i<cap(aux);i++ {aux[i] = x}	
	return aux
}

func fastdupslice(s []int) []int {
	aux := make([]int, len(s), (cap(s)+1)*2) // +1 in case cap(s) == 0
	// built-in func copy(dst, src []T) int
	copy(aux,s)
	return aux
}

func insert(s []int, elem int, pos int) []int {
	// slice... converts slice to elements, unpacks slice.
	//     append(slice, e1,e2,e3,...)
	return append(s[:pos], append([]int{elem}, s[pos:]...)...)
}       

func insert2(s []int, elem int, pos int) []int {
	s = append(s, 0)            // Making space for the new element
	copy(s[pos+1:], s[pos:])    // Shifting elements
        s[pos] = elem               // Copying/inserting the value
	return s
}

func main() {
	// Array
	a := [A_LEN]int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}   /* Full init literal array*/

	// Slices
	s1 := []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9} 
	s2 := []int{0, 1, 2}
	s3 := []int{}         // empty slice
	var s4 []int          // nil slice                       


	// all share the same space over "a" array
        s5  := a[0:]   // [0:len(a)]
        s6  := a[:len(a)]
	s7  := a[1:1]  //empty
	s8  := a[1:2]  // one element
        s9  := a[:]    // slice af all the elements
	s10 := s9[2:5] //slice of a slice
	d  := a        // array
	
	fmt.Println(len(d),cap(d),d)
        fmt.Println(len(s1),len(s2),len(s3),len(s4),"-",cap(s1),cap(s2),cap(s3),cap(s4))	
        fmt.Println(len(s5),len(s6),len(s7),len(s8),"-",cap(s5),cap(s6),cap(s7),cap(s8))
        fmt.Println(len(s9),"-",cap(s9))
        fmt.Println(s1,s2,s3,s4,s5,s6,s7,s8,s9,s10)
	a[0] = 1000
        fmt.Println(a,s5,s6,s7,s8,s9,s10)
	
	// A slice can be created with the built-in function called make, which has the signature,
	// func make([]T, len, cap) []T
	s11 := make([]int,10,10)
	fmt.Println(cap(s11),s11)
	s12 := dupslice(a[:])
	s13 := fastdupslice(s5)
	s14 := dupslice_and_fill(a[:],1)	
	fmt.Println(len(s12),cap(s12),s12,len(s13),cap(s13),s13,len(s14),cap(s14),s14)

	// The append function appends the elements x to the end of the slice s, and grows the slice
	// if a greater capacity is needed.
        // func append(s []T, x ...T) []T
	s12 = append(s12,10)
	s12 = append(s12,11)
	s13 = append(s12,11)
	fmt.Println(len(s12),cap(s12),s12,len(s13),cap(s13),s13,len(s14),cap(s14),s14)

	fmt.Println(s12)
	s12 = insert(s12,15,3)
	fmt.Println(s12)
	s12 = insert2(s12,15,3)
	fmt.Println(s12)
	fmt.Println("**************")
	s15 := a[:]
	a[0] = 3000
	fmt.Println(s15)
	fmt.Println(a)
	s15 = append(s15,1200)
	fmt.Println(s15)
	fmt.Println(a)
	a[0] = 0
	fmt.Println(s15)
	fmt.Println(a)
	//s12 = insert(s12,15,100)	 // slice bounds out of range
	//s12 = insert2(s12,15,100)	 // slice bounds out of range
}
