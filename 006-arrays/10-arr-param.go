package main

import "fmt"
//import "unsafe"


const LEN = 10

func param(a [10]int) {
	fmt.Printf("a[0]=%d a=%p\n",a[0],&a)
	a[0] = 10
}

func params(a []int) {
	fmt.Printf("a[0]=%d a=%p\n",a[0],&a)
	a[0] = 20	
}

func main() {
	var ar [LEN]int
	//var s  []int
	
	fmt.Printf("ar[0]=%d ar=%p arr=%p\n",ar[0],ar,&ar)
	param(ar)
	param(ar)
	params(ar[:])
	params(ar[:])
	//params(s) // runtime error: index out of range [0] with length 0
}
