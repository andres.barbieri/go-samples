#include <stdio.h>

#define LEN 10

void param1(int a[10]) {
  printf("a[0]=%d a=%p %p\n",a[0],a,&a);  
  a[0] = 10;
}

void param2(int a[]) {
  printf("a[0]=%d a=%p %p\n",a[0],a,&a);
  a[0] = 20;
}

int main() {
  int ar[LEN];
    
  printf("ar[0]=%d ar=%p %p %p\n",ar[0],ar,&ar,&(*ar));
  param1(ar);
  param1(ar);  
  param2(ar);
  param2(ar);  
  return 0;
}
