package main

import "fmt"

func remove_order(s []int, index int) []int {
	return append(s[:index], s[index+1:]...)
}

func remove(s []int, i int) []int {
    s[i] = s[len(s)-1]
    return s[:len(s)-1]
}

func main() {
	a := [...]int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 }
	var y []int
	//var z []int
	
	fmt.Println(a)
	fmt.Println(y)	
	y = remove(a[:],0)
	fmt.Println(a) // side effects over a	
	fmt.Println(y)	
	y = remove(y,0)
	fmt.Println(a) // side effects over a			
	fmt.Println(y)		
}
	
	
