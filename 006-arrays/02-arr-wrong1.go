package main

import "fmt"

const MAX_LEN = 30
const A_LEN   = 10
const B_LEN   = 10

func main() {
	var int_vector2 [MAX_LEN]int;                      /* No init */
	a:= [A_LEN]int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};      /* Full init */
	b:= [B_LEN]int{0, 1, 2};                           /* Incomplete Init will be filled with 0 */
	var i int;

	/*  Checking for array boundary at compile time */
	i = int_vector2[8];
	fmt.Printf("i=int_vector2[8]=%d\n",i);
	//i = a[A_LEN]; // invalid argument: index 10 out of bounds [0:10]
	i = a[B_LEN-1]; 
	fmt.Printf("i=a[%d]=%d\n",A_LEN,i);
	//i = a[A_LEN+1]; // invalid argument: index 11 out of bounds [0:10]
	fmt.Printf("i=a[%d]=%d\n",A_LEN+1,i);
	//i = b[-1]; // index -1 (constant of type int) must not be negative
	i = b[0]; 
	fmt.Printf("i=b[%d]=%d\n",0,i);
	//i = b[A_LEN]; //index 10 out of bounds [0:10]
	fmt.Printf("i=b[%d]=%d\n",A_LEN,i);
	//i = b[B_LEN+1]; //index 10 out of bounds [0:10]
	i = b[B_LEN-1]; 
	fmt.Printf("i=b[%d]=%d\n",B_LEN+1,i);
}
