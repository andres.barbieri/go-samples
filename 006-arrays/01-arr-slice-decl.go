// Array: sequence of elements of a single type with a fixed length

package main

import "fmt"
import "unsafe"

const MAX_LEN = 30
const A_LEN   = 10
const B_LEN   = 10

var global_vector [MAX_LEN]int
// Go does not support const array or slice. It is because in go constant
// value is computed at compile time.
// Arrays or slices are always evaluated at run time.
// So below program would raise a compilation error
// const global_vector2 = [2]int{1,2} //[2]int{...} (value of type [2]int) is not constant

// func as const array
func const_global_vector2() [2]int {
    return [2]int{1,2} 
}

func main() {
	var int_vector [30]int            // 30 slots 0..29
	var flt_vector [30]float32
        var int_vector2 [MAX_LEN]int
	a := [A_LEN]int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}   /* Array Full init literal array*/   
	a2 := [...]int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}    /* Array Full init - length computed at compile time */
	//s3 := []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9,}    /* Slice Full init */
	s3 := []int{                                    /* Slice Full init */
		0,
		1,
		2, 3, 4, 5, 6,
		7, 8,
	//	9,
	} 
	
	s4 := []int{0}           /* slice array with one element*/
	s5 := []int{}	         /* empty slice array */
	//s6 := []int	         /* compile error: []int (type) is not an expression */	
	var s6 []int	         /* nil slice array */
        b := [B_LEN]int{0, 1, 2} /* Incomplete init will be filled with 0 */
	var b2 [B_LEN]int        /* Incomplete init will be filled with 0 */
        var i int

	// Array dimmensions
	fmt.Printf("sizeof(int_vect)==%d\n",unsafe.Sizeof(int_vector))
	fmt.Printf("sizeof(int_vect2)==%d==%d\n",unsafe.Sizeof(int_vector2),(MAX_LEN*unsafe.Sizeof(i)))
	fmt.Printf("sizeof(a)=%d\n",unsafe.Sizeof(a))

	// Array print
	fmt.Println("const_global_vector2 = ",const_global_vector2())
	
	// Array accessing
	fmt.Printf("global_vector[%d]=%d..global_vector[%d]=%d\n",0, global_vector[0], MAX_LEN-1, global_vector[MAX_LEN-1])    
	fmt.Printf("int_vector2[%d]=%d..int_vector2[%d]=%d\n"    ,0, int_vector[0]   , MAX_LEN-1, int_vector[MAX_LEN-1])
	fmt.Printf("flt_vector[%d]=%f\n"                         ,0, flt_vector[0])
	// A's elements    
	fmt.Printf("##\n");  for i:=0;i<A_LEN;i++ {  fmt.Printf("a[%d]=%d,",i,a[i]) }
	fmt.Println()
	fmt.Printf("##\n");  fmt.Println("a=",a)
	// A's elements in reverse order
	fmt.Printf("##\n"); for i:=A_LEN-1;i>=0;i-- { fmt.Printf("a[%d]=%d,",i,a[i]) }
	fmt.Println()
	// B's elements
	fmt.Printf("##\n"); for i:=0;i<B_LEN;i++    { fmt.Printf("b[%d]=%d,",i,b[i]) }
	fmt.Println()
	// A2,B2's elements	
	fmt.Printf("##\n");  fmt.Println("b2=",b2)
	fmt.Printf("##\n");  fmt.Printf("sizeof(a2)=%d ",unsafe.Sizeof(a2));fmt.Println(",a2 =",a2)

	// size of the slice descriptor
	/***
         The "slice descriptor", as implied by the name, is all the data that describes a slice. 
         That is what is actually stored in a slice variable.
         Slices in Go have 3 attributes: 
              The underlying array (memory address), 
              the length of the slice (memory offset), 
              and the capacity of the slice (memory offset). 
          In a 64-bit application, memory addresses and offsets tend to be stored in 64-bit (8-byte) values. 
          That is why you see a size of 24 (= 3 * 8 ) bytes.
        ***/
	fmt.Printf("##\n");  fmt.Printf("sizeof(s3)=%d ",unsafe.Sizeof(s3));fmt.Println(",s3 =",s3)
	fmt.Printf("##\n");  fmt.Printf("sizeof(s4)=%d ",unsafe.Sizeof(s4));fmt.Println(",s4 =",s4)
	fmt.Printf("##\n");  fmt.Printf("sizeof(s5)=%d ",unsafe.Sizeof(s5));fmt.Println(",s5 =",s5)	
	fmt.Printf("##\n");  fmt.Printf("sizeof(s6)=%d ",unsafe.Sizeof(s6));fmt.Println(",s6 =",s6)		

}
