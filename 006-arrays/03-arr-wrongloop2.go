package main

import "fmt"

const A_LEN   = 10

func main() {
	a:= [A_LEN]int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9} /* Full init */
	var i int

	for i=0;i<A_LEN+2;i++ { fmt.Printf(" a[%d]=%d",i,a[i]) } // panic: runtime error: index out of range [10] with length 10
}
