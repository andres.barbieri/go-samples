package main

import stack "intstack"
import "fmt"

func main() {
	// Create a new stack
	s := stack.New()
	fmt.Println(s)
	stack.Push(&s,4)
	stack.Push(&s,1)
	stack.Push(&s,343)
	fmt.Println(s)
	fmt.Println(stack.Top(s))
	fmt.Println(s)
	fmt.Println(stack.Pop(&s))
	fmt.Println(s)
	stack.Push(&s,4)
	stack.Push(&s,5)
	fmt.Println(stack.Top(s))
	fmt.Println(s)
	for i:=100;i<200;i++ {stack.Push(&s,i)}
	fmt.Println(s)	
	//for i:=100;i<1000;i++ { fmt.Println(Pop(&s));fmt.Println(s) }
	for i:=100;i<1000;i++ { fmt.Println(stack.PopTest(&s));fmt.Println(s) }
}
