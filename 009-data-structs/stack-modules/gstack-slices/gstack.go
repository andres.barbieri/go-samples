/*
 * Generic Stack as OOP build over slices
 */

package gstack

import "fmt"
import "strconv"

type Stack struct {
	elements   []interface{} //any
	pos        int
}

const LEN =   0
const CAP = 256

func New() Stack {
	var aux Stack
	aux.elements = make([]any,LEN,CAP)
	aux.pos = 0
	return aux
}

func (this Stack) IsEmpty() bool {
  return (this.pos == 0)
}

func (this Stack) Len() int {
      return this.pos
} 

func (this *Stack) Pop() any {
	this.pos--
	return this.elements[this.pos]
}

func (this *Stack) PopTest() bool {
	if (!this.IsEmpty()) {
		this.pos--
		return true
	}
	return false
}


func (this Stack) Top() any {
	return this.elements[this.pos-1]
}

func  (this Stack) TopTest() (any,bool) {
	if (!this.IsEmpty()) {
		return this.Top(),true
	} else {
		return nil,false
	}	
}

func (this *Stack) Push(elem any) *Stack {
	if (len(this.elements) == this.pos) {
		this.elements = append(this.elements, elem)
		this.pos++
	} else {
		this.elements[this.pos] = elem
		this.pos++
	}
	return this
}

func (this Stack) ToString() string {
	if (this.IsEmpty()) {
		return strconv.Itoa(this.Len())+"[]"
	} else {
		aux := strconv.Itoa(this.Len())+"["
		for i:=0;i<this.Len();i++ {
			s := fmt.Sprintf("(%s)",this.elements[i])
			aux=aux+s+","
		}
		aux=aux+"]"
		return aux
	}
}

func (this Stack) String() string {
	return this.ToString()
}

