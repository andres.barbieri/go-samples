/*
 * Integer Stack using Single Linked List
 */

package intstack

//import "fmt"
import "strconv"
import "intlist" 

type Stack struct {
          list intlist.List
}

func New() Stack {
	var aux Stack
	aux.list = intlist.New()
	return aux
}

func  IsEmpty(this Stack) bool {
	return intlist.IsEmpty(this.list)
}

func Len(this Stack) int {
	return intlist.Len(this.list)
} 

func Pop(this *Stack) int {
	return intlist.Remove(&(this.list))
}

func PopTest(this *Stack) bool {
	if (!IsEmpty(*this)) {
		Pop(this)
		return true
	} else {
		return false
	}
}

func  Top(this Stack) int {
	return intlist.FrontElement(this.list)
}

func TopTest(this Stack) (int,bool) {
	if (!IsEmpty(this)) {
		return intlist.FrontElement(this.list),true
	} else {
		return 0,false
	}
}

func Push(this *Stack, elem int) {
	intlist.PushFront(&(this.list),elem)
}

func ToString(this Stack) string {
	if (IsEmpty(this)) {
		return strconv.Itoa(Len(this))+"[]"
	} else {
		aux := strconv.Itoa(Len(this))+"["
		for i:=this.list;!intlist.IsEmpty(i);i=intlist.Next(i) {
			aux=aux+strconv.Itoa(intlist.FrontElement(i))+","}
		aux=aux+"]"
		return aux
	}
}

func (this Stack) String() string {
	return ToString(this)
}
