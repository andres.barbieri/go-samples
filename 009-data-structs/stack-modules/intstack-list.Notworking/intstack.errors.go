/*
 * Integer Stack as OOP using Single Linked List
 * Not working Errors
 */

package intstack

//import "fmt"
import L "intlist" 

type Stack L.List


func New() Stack {
	// cannot use L.New() (value of type intlist.List) as Stack value in return statement
	//return L.New()
	return Stack(L.New())
}

//invalid receiver type Stack (pointer or interface type)
func (this Stack) IsEmpty() bool {
	//return L.IsEmpty(this)
	return L.IsEmpty(L.List(this))
}

//cannot define new methods on non-local type intlist.Node
func (this *L.Node) IsEmpty() bool {
	//return L.IsEmpty(this)
	return L.IsEmpty(L.List(this))
}


// ...
