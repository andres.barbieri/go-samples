package main

import stack "intstack"
import "fmt"

func main() {
	// Create a new stack
	s := stack.New()
	fmt.Println(s)
	s.Push(4)
	s.Push(1)
	s.Push(343)
	fmt.Println(s.Top())
	fmt.Println(s)
	fmt.Println(s.Pop())
	fmt.Println(s)
	s.Push(4)
	s.Push(5)
	fmt.Println(s.Top())
	fmt.Println(s)
	for i:=100;i<200;i++ {s.Push(i)}
	fmt.Println(s)	
	//for i:=100;i<1000;i++ { fmt.Println(s.Pop());fmt.Println(s) }
	for i:=100;i<1000;i++ { fmt.Println(s.PopTest());fmt.Println(s) }
}
