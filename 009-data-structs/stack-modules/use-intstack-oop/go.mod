module main

go 1.20

replace intstack => ../../stack-modules/intstack-oop/
replace intlist  => ../../list-modules/intlist2/ //indirect
							
require intstack v0.0.0-00010101000000-000000000000
//require intlist  v0.0.0-00010101000000-000000000000 //indirect
