package main

import stack "gstack"
import "fmt"

func main() {
	// Create a new stack
	s := stack.New()
	fmt.Println(s)
	s.Push(4)
	fmt.Println(s.TopTest())
	s.Push(1)
	x := s.Top()
	// invalid operation: x + 1 (mismatched types any and int)
	// x = x + 1
	x = x.(int) + 1
	fmt.Println(x)
	s.Push(343)
	fmt.Println(s.Top())
	fmt.Println(s)
	fmt.Println(s.Pop())
	fmt.Println(s)
	s.Push(4)
	s.Push("HOLA")
	s.Push(s)	
	s.Push(45.33)	
	s.Push(5)
	fmt.Println(s.Top())
	fmt.Println(s)
	for i:=100;i<200;i++ {s.Push(i)}
	fmt.Println(s)	
	//for i:=100;i<1000;i++ { fmt.Println(s.Pop());fmt.Println(s) }
	for i:=100;i<1000;i++ { fmt.Println(s.PopTest());fmt.Println(s) }
	fmt.Println(s.TopTest())
}
