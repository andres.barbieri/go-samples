/*
 * Integer Stack using Single Linked List as OOP
 */

package intstack

//import "fmt"
import "strconv"
import "intlist" 

type Stack struct {
          list intlist.List
}

func New() Stack {
	var aux Stack
	aux.list = intlist.New()
	return aux
}

func  (this Stack) IsEmpty() bool {
	return intlist.IsEmpty(this.list)
}

func (this Stack) Len() int {
	return intlist.Len(this.list)
} 

func (this *Stack) Pop() int {
	return intlist.Remove(&(this.list))
}

func (this *Stack) PopTest() bool {
	if (!this.IsEmpty()) {
		this.Pop()
		return true
	} else {
		return false
	}
}

func  (this Stack) Top() int {
	return intlist.FrontElement(this.list)
}

func (this Stack) TopTest() (int,bool) {
	if (!this.IsEmpty()) {
		return intlist.FrontElement(this.list),true
	} else {
		return 0,false
	}
}

func (this *Stack) Push(elem int) {
	intlist.PushFront(&(this.list),elem)
}

func (this Stack) ToString() string {
	if (this.IsEmpty()) {
		return strconv.Itoa(this.Len())+"[]"
	} else {
		aux := strconv.Itoa(this.Len())+"["
		for i:=this.list;!intlist.IsEmpty(i);i=intlist.Next(i) {
			aux=aux+strconv.Itoa(intlist.FrontElement(i))+","}
		aux=aux+"]"
		return aux
	}
}

func (this Stack) String() string {
	return this.ToString()
}
