package intbintree

import "strconv"

type NodeBinTree struct {
	elem int
	left  *NodeBinTree
	right *NodeBinTree
}

type IntBinTree struct {
	tree *NodeBinTree
}

type Order int32

const (
	Inorder    = 0	
	Postorder  = 1
	Preorder   = 2
	Reverse    = 3
)

func (this IntBinTree) IsEmpty() bool {
  return (this.tree == nil)
}

func (this IntBinTree) GetElem() int {
	return this.tree.elem
}

func (this IntBinTree) GetRight() IntBinTree {
	return IntBinTree{this.tree.right}
}


func (this IntBinTree) GetLeft() IntBinTree {
	return IntBinTree{this.tree.left}
}

func (this IntBinTree) Len() int {
  if (this.IsEmpty()) {
      return 0;
    } else {
	    return 1 + (this.GetLeft()).Len() + (this.GetRight()).Len() 
    }
}


func nodeAdd(this **NodeBinTree, elem int) {
	if ((*this) == nil) {
		aux := new(NodeBinTree)
		aux.elem = elem
		aux.left = nil
		aux.right = nil
		(*this) = aux
	} else if ((*this).elem > elem) {
		nodeAdd( &((*this).left),elem)
	} else {
		//Cannot take address of (*this).getRight() (value of type *NodeBinTree)
		//Add( &((*this).getRight()) ,elem)
		nodeAdd( &((*this).right) ,elem)
	}
}

func (this IntBinTree) Add(elem int) IntBinTree {
	nodeAdd(&(this.tree), elem)
	return this
}


func (this IntBinTree) AddWrong(elem int) IntBinTree {
	if (this.IsEmpty()) {
		aux := new(NodeBinTree)
		aux.elem = elem
		aux.left = nil
		aux.right = nil
		return  IntBinTree{aux}
	} else if (this.GetElem() > elem) {
		(this.GetLeft()).Add(elem) //Lost Ref
		
		return this
	} else {
	        (this.GetRight()).Add(elem) //Lost ref
		return this
	}
}

func (this IntBinTree) Traverse(fp func(int), o Order) {
	if (!this.IsEmpty()) {
		switch (o) {
		case Inorder:
			(this.GetLeft()).Traverse(fp,o)
		        fp(this.tree.elem)
			(this.GetRight()).Traverse(fp,o)
		case Postorder:
			(this.GetLeft()).Traverse(fp,o)
			(this.GetRight()).Traverse(fp,o)
			fp(this.tree.elem)
		case Preorder:
			fp(this.tree.elem)
			(this.GetLeft()).Traverse(fp,o)
			(this.GetRight()).Traverse(fp,o)
		case Reverse:
			(this.GetRight()).Traverse(fp,o)
			fp(this.tree.elem)
			(this.GetLeft()).Traverse(fp,o)
		}
	}
}


func (this IntBinTree) Apply(fp func(int)int) {
	if (!this.IsEmpty()) {
		(this.GetLeft()).Apply(fp)
		this.tree.elem  = fp(this.tree.elem)
		(this.GetRight()).Apply(fp)
	}
}

func (this IntBinTree) Includes(elem int) bool {
	if (this.IsEmpty()) {
		return false
	} else if (this.tree.elem == elem) {
		return true
	} else if (this.tree.elem > elem)  {
		return (this.GetLeft()).Includes(elem)
	} else {
		return (this.GetRight()).Includes(elem)		

	}
}

func (this IntBinTree) Find(fp func(int) bool) bool {
	if (this.IsEmpty()) {
		return false
	} else if (fp(this.tree.elem)) {
		return true
	} else {
		return ((this.GetLeft()).Find(fp)) || ((this.GetRight()).Find(fp))
	}
}

func (this IntBinTree) String() string {
	if (this.IsEmpty()) {
		return " "
	} else {
		return (this.GetLeft()).String()+strconv.Itoa(this.tree.elem)+(this.GetRight()).String()
	}
}

