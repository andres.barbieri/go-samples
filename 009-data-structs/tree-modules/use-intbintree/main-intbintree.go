package main

import ("fmt"
	ibt "intbintree")

func main() {
	//var t *NodeBinTree
	var t ibt.IntBinTree

	var m      = func(i int) int { return i*1000}
	var p      = func(i int) { fmt.Print("**",i,"**") }
	var e25000 = func(i int) bool { return (i == 25000) }
	
	fmt.Println(t.Len(), t.IsEmpty())
	// Lost ref
	//t.Add(10); t.Add(20); t.Add(3)
	t = t.Add(10); t = t.Add(20); t = t.Add(3)
	for i:=-2;i<30;i++ { t = t.Add(i) }
	fmt.Println(t.Len(), t.IsEmpty())
	fmt.Println(t)
	t.Apply(m)
	fmt.Println(t)	
	t.Traverse(p,ibt.Inorder);fmt.Println()
	t.Traverse(p,ibt.Postorder);fmt.Println()
	t.Traverse(p,ibt.Preorder);fmt.Println()
	t.Traverse(p,ibt.Reverse);fmt.Println()
	fmt.Println(t.Includes(25000),t.Includes(-25000))
	fmt.Println(t.Find(e25000))	
}

