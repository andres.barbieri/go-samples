package anybintree

//import "fmt"
//import "strconv"


type NodeBinTree struct {
	elem  any
	left  *NodeBinTree
	right *NodeBinTree
}

type AnyBinTree struct {
	tree *NodeBinTree
}

func (this AnyBinTree) IsEmpty() bool {
  return (this.tree == nil)
}

func (this AnyBinTree) GetElem() any {
	return this.tree.elem
}

func (this AnyBinTree) GetRight() AnyBinTree {
	return AnyBinTree{this.tree.right}
}


func (this AnyBinTree) GetLeft() AnyBinTree {
	return AnyBinTree{this.tree.left}
}

func (this AnyBinTree) Len() int {
  if (this.IsEmpty()) {
      return 0;
    } else {
	    return 1 + (this.GetLeft()).Len() + (this.GetRight()).Len() 
    }
}


func nodeAdd(this **NodeBinTree, elem any) {
	if ((*this) == nil) {
		aux := new(NodeBinTree)
		aux.elem = elem
		aux.left = nil
		aux.right = nil
		(*this) = aux
	} else if ((*this).elem > elem) {
		nodeAdd( &((*this).left),elem)
	} else {
		//Cannot take address of (*this).getRight() (value of type *NodeBinTree)
		//Add( &((*this).getRight()) ,elem)
		nodeAdd( &((*this).right) ,elem)
	}
}

func (this AnyBinTree) Add(elem any) AnyBinTree {
	nodeAdd(&(this.tree), elem)
	return this
}

func (this AnyBinTree) Includes(elem any) bool {
	if (this.IsEmpty()) {
		return false
	} else if (this.tree.elem == elem) {
		return true
	} else if (elem < (this.tree.elem)) {
		return (this.GetLeft()).Includes(elem)
	} else {
		return (this.GetRight()).Includes(elem)		

	}
}

func (this AnyBinTree) String() string {
	if (this.IsEmpty()) {
		return " "
	} else {
		//return (this.GetLeft()).String()+strconv.Itoa(this.tree.elem)+(this.GetRight()).String()
		return (this.GetLeft()).String()+this.tree.elem.String()+(this.GetRight()).String()
	}
}



