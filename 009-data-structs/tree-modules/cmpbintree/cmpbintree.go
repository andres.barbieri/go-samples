package  cmpbintree

type Comparable interface {
	Less(j Comparable) bool
	String() string
}

type NodeBinTree struct {
	elem  Comparable
	left  *NodeBinTree
	right *NodeBinTree
}

type CmpBinTree struct {
	tree *NodeBinTree
}

func (this CmpBinTree) IsEmpty() bool {
  return (this.tree == nil)
}

func (this CmpBinTree) GetElem() Comparable {
	return this.tree.elem
}

func (this CmpBinTree) GetRight() CmpBinTree {
	return CmpBinTree{this.tree.right}
}


func (this CmpBinTree) GetLeft() CmpBinTree {
	return CmpBinTree{this.tree.left}
}

func (this CmpBinTree) Len() int {
  if (this.IsEmpty()) {
      return 0;
    } else {
	    return 1 + (this.GetLeft()).Len() + (this.GetRight()).Len() 
    }
}


func nodeAdd(this **NodeBinTree, elem Comparable) {
	if ((*this) == nil) {
		aux := new(NodeBinTree)
		aux.elem = elem
		aux.left = nil
		aux.right = nil
		(*this) = aux
//	} else if ((*this).elem > elem) {
	} else if elem.Less(((*this).elem))  {
		nodeAdd( &((*this).left),elem)
	} else {
		//Cannot take address of (*this).getRight() (value of type *NodeBinTree)
		//Add( &((*this).getRight()) ,elem)
		nodeAdd( &((*this).right) ,elem)
	}
}

func (this CmpBinTree) Add(elem Comparable) CmpBinTree {
	nodeAdd(&(this.tree), elem)
	return this
}

func (this CmpBinTree) Includes(elem Comparable) bool {
	if (this.IsEmpty()) {
		return false
	} else if (this.tree.elem == elem) {
		return true
	} else if (elem.Less(this.tree.elem)) {
		return (this.GetLeft()).Includes(elem)
	} else {
		return (this.GetRight()).Includes(elem)		

	}
}

func (this CmpBinTree) String() string {
	if (this.IsEmpty()) {
		return " "
	} else {
		//return (this.GetLeft()).String()+strconv.Itoa(this.tree.elem)+(this.GetRight()).String()
		return (this.GetLeft()).String()+this.tree.elem.String()+(this.GetRight()).String()
	}
}

