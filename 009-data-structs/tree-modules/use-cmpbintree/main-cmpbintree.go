package main

import ( "fmt"
	 "strconv"
	cbt "cmpbintree")

type MyInt int
//func (i MyInt) Less(j MyInt) bool { return i<j }
func (i MyInt) Less(j cbt.Comparable) bool { return i < (j.(MyInt))   }
func (i MyInt) String() string { return strconv.Itoa(int(i)) }

type MyFlt float32
func (i MyFlt) Less(j cbt.Comparable) bool { return i < (j.(MyFlt))   }
func (i MyFlt) String() string { return strconv.FormatFloat(float64(i), 'f', -1, 32) }

func main() {
	//var t *NodeBinTree
	var t cbt.CmpBinTree
	fmt.Println(t.Len(), t.IsEmpty())
	// Lost ref
	//t.Add(10); t.Add(20); t.Add(3)
	var j MyInt = 10
	t = t.Add(j)
	j = 20; t = t.Add(j)
	j = 3; t = t.Add(j)
	var i MyInt	
	for i=-2;i<30;i++ { t = t.Add(i) }
	fmt.Println(t.Len(), t.IsEmpty())
	fmt.Println(t)

	//fmt.Println(t.Includes(MyInt(3.343))) // cannot convert
	fmt.Println(t.Includes(MyInt(3)))
	fmt.Println(t.Includes(MyInt(33)))
	fmt.Println(t.Includes(MyInt(20)))	
	
	//panic: interface conversion: main.Comparable is main.MyInt, not main.MyFlt
        //t.Add(MyFlt(233.2233))
	
	var v cbt.CmpBinTree	
	fmt.Println(v.Len(), v.IsEmpty())
	var f MyFlt = 10.234
	v = v.Add(f)
	f = 20.4544; v = v.Add(f)
	f = 3.343; v = v.Add(f)
	f = 20; v = v.Add(f)	
	fmt.Println(v.Len(), v.IsEmpty())
	fmt.Println(v)
	//fmt.Println(v.Includes(3.343)) //cannot use 3.343 (constant of type float64) as Comparable value in argument to v.Includes: float64 does not implement Comparable (missing method Less)
	fmt.Println(v.Includes(MyFlt(3.343)))
	fmt.Println(v.Includes(MyFlt(3.344)))
	fmt.Println(v.Includes(MyFlt(20)))	
}

