/*
 * Any/Generic Queue as OOP build over container/list
 */

package listqueue

import ("container/list"
	"fmt"
	"strconv")

type Queue struct {
	elements   *list.List
}

func New() *Queue {
	return &Queue{list.New()}
}

func (this Queue) IsEmpty() bool {
	return this.elements.Len() == 0
}

func (this Queue) Len() int {
	return this.elements.Len()
} 

func (this *Queue) Pop() interface{} {
	return (*this).elements.Remove((*this).elements.Front())
}

func (this *Queue) Dequeue() interface{} {
	return this.Pop()
}

func (this Queue) Top() interface{} {
	return this.elements.Front().Value
}

func  (this Queue) TopTest() (interface{},bool) {
	if (!this.IsEmpty()) {
		return this.Top(),true
	} else {
		return nil,false
	}	
}

func (this *Queue) Push(elem interface{}) *Queue {
        this.elements.PushBack(elem)
	return this
}

func (this *Queue) Enqueue(elem interface{}) *Queue {
	return this.Push(elem)
}
	
func (this Queue) ToString() string {
	aux := "{"+strconv.Itoa(this.Len())+"}Q["
	l := this.elements
	for e := l.Front(); e != nil; e = e.Next() {
		aux=aux+fmt.Sprintf("(%s)",e.Value)
	}
	aux=aux+"]"
	return aux
}

func (this Queue) String() string {
	return this.ToString()
}
