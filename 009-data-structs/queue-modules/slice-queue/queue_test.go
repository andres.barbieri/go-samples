/*
 * Any/Generic Queue test
 */

// run go test
package genqueue

import (
	"fmt"
	"testing"
)

func TestGenQueue_Empty(t *testing.T) {
	q := New()
	if !q.IsEmpty() {
		t.Fatalf("New queue not empty")
    }
}

func TestGenQueue_Add(t *testing.T) {
	const LEN = 20
	q := New()
	for i:=0;i<LEN;i++ {
		q.Push(i)
	}
	//if (q.Len() != LEN+1) {
	if (q.Len() != LEN) {
		t.Fatalf("Len Failed!!")
    }
}

func TestGenQueue(t *testing.T) {
 	q := New()
	fmt.Println(q)
	q = q.Push(1)
	q = q.Enqueue(2)
	fmt.Println(q.Top())
	fmt.Println(q)
	for i:=10;i<20;i++ {
		q.Push(i)
	}
	fmt.Println(q)
	fmt.Println(q.Pop())
	for i:=0;i<10;i++ {
		fmt.Println(q.Dequeue())
		fmt.Println(q)
	}
}
