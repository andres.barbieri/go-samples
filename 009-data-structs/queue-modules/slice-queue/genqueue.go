/*
 * Any/Generic Queue as OOP build over slices
 */

package genqueue

import ("fmt"
	"strconv")

type Queue struct {
	elements   []interface{}
}

func New() *Queue {
	return &Queue{}
}

func (this Queue) IsEmpty() bool {
	return len(this.elements) == 0
}

func (this Queue) Len() int {
	return len(this.elements)
} 

func (this *Queue) Pop() interface{} {
	auxslice := this.elements
	var result interface{}
	auxlen := len(auxslice)
	result = auxslice[0]
	(*this).elements = auxslice[1:auxlen]
	return result
}

func (this *Queue) Dequeue() interface{} {
	return this.Pop()
}

func (this Queue) Top() interface{} {
	return this.elements[0]
}

func  (this Queue) TopTest() (interface{},bool) {
	if (!this.IsEmpty()) {
		return this.Top(),true
	} else {
		return nil,false
	}	
}

func (this *Queue) Push(elem interface{}) *Queue {
	this.elements = append(this.elements, elem)
	return this
}

func (this *Queue) Enqueue(elem interface{}) *Queue {
	return this.Push(elem)
}

func (this Queue) ToString() string {
	aux := "{"+strconv.Itoa(this.Len())+"}Q["
	for i:=0;i<this.Len();i++ {
		aux=aux+fmt.Sprintf("(%s)",this.elements[i])
	}
	aux=aux+"]"
	return aux
}

func (this Queue) String() string {
	return this.ToString()
}
