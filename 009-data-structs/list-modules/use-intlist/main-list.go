/*
 *  Program using intlist package
 *  Check go.mod file references intlist
 *
 * Errors:
 * package intlist is not in std (/home/andres/.go/go/src/intlist)
 *
 * intlist@v0.0.0-00010101000000-000000000000: replacement directory ./intlist22 does not exist
 *
 * Could be run:
 * go env | grep GOPATH
 * GOPATH=$PWD/intlist/ go run main-list.go 
 * Not eny more
 * go: warning: ignoring go.mod in $GOPATH 
 *
 */

package main

import "fmt"
import L "intlist" 


func dec(x int) int {
	return x-1
}

func main() {
	// Not needed
	//l := L.New()
	var l L.List
	//l = L.New()
	var inc = func(x int) int { return x+1 }
	
	fmt.Println(L.ToString(l),L.Len(l))	
	L.PushFront(&l,1)

	L.PushFront(&l,2)
	L.PushFront(&l,3)
	L.PushBack(&l,3)
	L.PushBack(&l,2)
	L.PushBack(&l,1)	
	fmt.Println(L.IsEmpty(l))
	fmt.Println(L.ToString(l), L.Len(l))
	L.Iterate(l, inc)
	fmt.Println(L.ToString(l), L.Len(l))
	L.Iterate(l, dec)
	fmt.Println(L.ToString(l), L.Len(l))	
	L.Remove(&l)
	L.Remove(&l)
	fmt.Println(L.ToString(l),L.Len(l))
	//for aux:=l;!L.IsEmpty(aux);aux=L.Next(aux) {
	for aux:=l;aux!=nil;aux=L.Next(aux) {		
		fmt.Println(L.FrontElement(aux))
	}
}

