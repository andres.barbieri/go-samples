/*
 *  Program using intlist package
 *  Check go.mod file references intlist2
 */
package main

import "fmt"
import L "intlist" 


func dec(x int) int {
	return x-1
}

func main() {
	l := L.New()
	var inc = func(x int) int { return x+1 }
	
	fmt.Println(L.ToString(l),L.Len(l))	
	L.PushFront(&l,1)

	L.PushFront(&l,2)
	L.PushFront(&l,3)
	L.PushBack(&l,3)
	L.PushBack(&l,2)
	L.PushBack(&l,1)	
	fmt.Println(L.IsEmpty(l))
	fmt.Println(L.ToString(l), L.Len(l))
	L.Iterate(l, inc)
	fmt.Println(L.ToString(l), L.Len(l))
	L.Iterate(l, dec)
	fmt.Println(L.ToString(l), L.Len(l))	
	L.Remove(&l)
	L.Remove(&l)
	fmt.Println(L.ToString(l),L.Len(l))
	//for aux:=l;!L.IsEmpty(aux);aux=L.Next(aux) {
	var aux L.List
	var i int
	// panic: runtime error: invalid memory
	//for i,aux = 0, l  ; i < 10; i,aux  = i+1 , L.Next(aux) {
	//	fmt.Println(L.FrontElement(aux))
	//}

	//for i,aux = 0, l; i < 10 ; i, aux, _  = i+1 , L.NextTest(aux) {			
	for i,aux = 0, l; i < 10 ; aux, _  = L.NextTest(aux) {		
		fmt.Println(L.FrontElementTest(aux))
		i++
	}
	for ii, auxl := 0,l ; ii<10 ; auxl,_=L.NextTest(auxl) {		
		fmt.Println(L.FrontElementTest(auxl))
		ii++
	}
	L.Push(&l,1000,2000,3000)
	fmt.Println(L.String(l))
}

