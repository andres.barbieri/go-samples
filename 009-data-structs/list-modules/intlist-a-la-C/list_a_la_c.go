package main

import "fmt"
import "strconv"

type node_t struct {
	elem int
	next *node_t
}

type list_t *node_t

func ls_create() list_t {
	return (list_t(nil));
}

func ls_is_empty(self list_t) bool {
  return (self == nil)
}

func ls_length(self list_t) int {
  if (ls_is_empty(self)) {
      return 0;
    } else {
    return 1 + ls_length(ls_tail(self));
  }
} 

func ls_head(self list_t) int {
	//return (*self).elem
	return self.elem
}

func ls_tail(self list_t) list_t {
  return self.next
}

func ls_to_string(l list_t) string {
	if (ls_is_empty(l)) {
		return "[]"
	} else {
		return strconv.Itoa(l.elem)+"->"+ls_to_string(ls_tail(l))
	}
}

func ls_add_front(self *list_t, elem int) {
	aux := new(node_t)
	aux.elem = elem
	aux.next = *self
	*self = aux
}

func ls_add_back(self *list_t, elem int) {
	if (ls_is_empty(*self)) {
		aux := new(node_t)
		aux.elem = elem
		aux.next = nil
		*self = aux
	} else {
		// cannot use (&(*self).next) (value of type **node_t) as *list_t value in argument
		// to ls_add_back
		//ls_add_back( (&((*self).next)) ,elem )

		// cannot use (&(*self).next) (value of type **node_t) as *list_t value in argument to ls_add_back
		//ls_add_back(        (&((*self).next)) ,elem )

		// invalid operation: cannot take address of ls_tail(*self) (value of type list_t)
		// ls_add_back( (*list_t)(&(ls_tail(*self))) ,elem )
		ls_add_back( (*list_t)(&((*self).next)) ,elem )
	}
}


func ls_remove(self *list_t) int {
	elem := (*self).elem
	//aux  := (*self)
	*self = (list_t)(((*self).next))
	return elem;
}


func ls_iterate(self list_t, fp func(int)int) {
  aux := self;
  for (!ls_is_empty(aux)) {
      aux.elem = (fp(aux.elem));
      aux = ls_tail(aux);
  }
}


func dec(x int) int {
	return x-1
}

func main() {
	var l list_t
	var inc = func(x int) int { return x+1 }
	
	fmt.Println(ls_to_string(l),ls_length(l))	
	ls_add_front(&l,1)
	ls_add_front(&l,2)
	ls_add_front(&l,3)
	ls_add_back(&l,3)
	ls_add_back(&l,2)
	ls_add_back(&l,1)	
	fmt.Println(ls_is_empty(l))
	fmt.Println(ls_to_string(l), ls_length(l))
	ls_iterate(l, inc)
	fmt.Println(ls_to_string(l), ls_length(l))
	ls_iterate(l, dec)
	fmt.Println(ls_to_string(l), ls_length(l))	
	ls_remove(&l)
	ls_remove(&l)
	fmt.Println(ls_to_string(l),ls_length(l))
}

