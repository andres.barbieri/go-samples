#include "list.h"

int print(int e)
{
  printf("%d ",e);
  return e;
}

int inc(int e)
{
  return (e+1);
}

int ge8(int e)
{
  return (e>=8);
}

int plus(int e1,int e2)
{
  return (e1+e2);
}

int main()
{
  list_t l  = NULL;
  list_t c  = NULL;

   ls_show(l);printf("\n");
   ls_add_front(&l, 200);
   ls_show(l);printf("\n");

  for (int i = 0; i < 10; i++) {
    ls_add_front(&l, i);
  }
  ls_show(l);
  printf("\n");
  ls_remove(&l);
  ls_show(l);
  printf("\n");
  ls_remove(&l);
  ls_show(l);
  printf("\n");
  for (int i = 100; i <= 102; i++) {
    ls_add_back(&l, i);
  }
  ls_add_front(&l,20);
  ls_show(l);
  printf("\n");
  c = ls_select(l,ge8);
  ls_show(c);
  printf("\n");
  ls_iterate(l,inc);
  ls_iterate(l,print);
  printf("\nsum=%d\n",ls_fold(l,plus,0));
  ls_free(&l);
  ls_free(&c);
  return 0;
}
