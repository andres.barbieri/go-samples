
/*
 *  Program using intlist package
 *  Check go.mod file references genlist2
 */
package main

import "fmt"
import L "genlist"


func greatInt(i any) bool {
	val, ok := (i).(int)
	if ok {
		return (val>=30)
	} else {
		return false
	}
}

func describe(v interface{}) {
	fmt.Printf("(%v, %T)\n", v, v)
}

func main() {
	var l L.List

	fmt.Println(l.IsEmpty(),l.Len()) // Empty
	fmt.Println(l)
	l = l.PushFront(1)
	l = l.PushFront(10.23)
	l = l.PushFront(20)
	l = l.PushFront(30)
	l = l.PushFront(33.23)
	l = l.PushFront(l)	
	l = l.PushFront("HOLA")
	l = l.PushFront(444)
	l = l.PushFront(555)
	fmt.Println(l.IsEmpty(),l.Len())
	fmt.Println(l)
	l = l.PushBack(40)
	l = l.PushBack(50)
	fmt.Println(l)
	fmt.Println("##",l.IsEmpty(),l.Len())	
	l = l.Remove()
	l = l.Remove()
	fmt.Println("##",l.IsEmpty(),l.Len())
	fmt.Println(l)
	l.Iterate(describe)
	aux := l
	for ;!aux.IsEmpty(); aux = aux.Next() {
		val, ok := (aux.GetElement()).(int)
		if ok { fmt.Println(val) }
	}
	aux = l
	for ;!aux.IsEmpty(); aux = aux.Next() {
		val, ok := (aux.GetElement()).(float64)
		if ok { fmt.Println(val) }
	}
	l2 := l.Collect(greatInt)
	fmt.Println(l2)
	fmt.Println(l2.Includes(34.455))
	fmt.Println(l2.Includes(30))
	fmt.Println(l.Includes("HOLA"))
	fmt.Println(l.Includes(l))	
}
