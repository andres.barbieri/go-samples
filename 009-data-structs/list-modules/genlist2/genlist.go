/*
 * Generic Single Linked List as OOP
 */

package genlist

import "fmt"

type Node struct {
	elem    any // interface{}
	// invalid recursive type: Node refers to itself
	//next    Node
	next *Node
}

type List struct {
	fst *Node
}

func (this List) IsEmpty() bool {
  return (this.fst == nil)
}

func (this List) Len() int {
  if (this.IsEmpty()) {
	  return 0;
  } else {
	  return 1 + List{this.fst.next}.Len()
  }
} 

func (this List) GetElement() any {
	return this.fst.elem
}

func  (this List) FrontElement() any {
	return this.fst.elem
}


func  (this List) FrontElementTest() (any,bool) {
	if (!this.IsEmpty()) {
		return this.fst.elem,true
	} else {
		return nil,false
	}	
}

func (this List) GetNext() List {
	return List{this.fst.next}
}

func  (this List) Next() List {
	return List{this.fst.next}
}

func  (this List) NextTest() (List,bool) {
	if (!this.IsEmpty()) {
		return List{this.fst.next},true
	} else {
		return List{nil},false
	}	
}

func (this List) PushFront(elem any) List {
	aux := new(Node)
	aux.elem = elem
	aux.next = this.fst
	return List{aux}
}


func (this List) PushBack(elem any) List {

	var prev,cur  List
	// Search last node
	for  cur = this ;!cur.IsEmpty(); prev, cur = cur, cur.GetNext() {}
	// Create new node
	aux := new(Node)
	aux.elem = elem
	aux.next = nil
	// Link new node
	if !prev.IsEmpty() { 
		prev.fst.next = aux
		return this
	} else {
		return List{aux}
	}
}

func (this List) Remove() List {
	return List{this.fst.next}
}

func (this List) ToString() string {
	if (this.IsEmpty()) {
		return "[]"
	} else {
		//return "an element" +"->"+ToString(Next(l))
		s := fmt.Sprintf("(%s)",this.fst.elem)
		return s +"->"+this.GetNext().ToString()
	}
}

func (this List) String() string {
	return this.ToString()
}


func (this List) Includes(elem any) bool {
	if (this.IsEmpty()) {
		return false
	} else {
		return  (this.FrontElement()==elem)||((this.GetNext()).Includes(elem))
	}
}	

func (this List) Iterate(fp func(any)) {
	aux := List{this.fst}
	for (!aux.IsEmpty()) {
		(fp(aux.fst.elem))
		aux = aux.Next()
	}
}

func (this List) Apply(fp func(any)any) {
	aux := List{this.fst}
	for (!aux.IsEmpty()) {
		aux.fst.elem = (fp(aux.fst.elem))
		aux = aux.Next()
	}
}

func (this List) Collect(fp func(any)bool) List{
 var coll List	
  for ;!this.IsEmpty(); this = this.Next() {
	  if (fp(this.fst.elem)) {
		  coll = coll.PushFront(this.fst.elem)
	  }
  }
  return coll	
}



