
/*
 *  Program using intlist package
 *  Check go.mod file references intlist3
 */
package main

import "fmt"
import L "intlist"

func main() {
	var l *L.Node

	var inc = func(x int) int { return x+1 }
	var gt40  = func(x int) bool { return x>40 }
	var lt100  = func(x int) bool { return x<100 }	
	fmt.Println(l.IsEmpty(),l.Len())
	// Copy parameter does not work
	l.PushFrontNO(10)
	fmt.Println(l.IsEmpty(),l.Len()) // Empty
	l = l.PushFront(1)
	l = l.PushFront(10)
	l = l.PushFront(20)
	l = l.PushFront(30)
	fmt.Println(l.IsEmpty(),l.Len())
	fmt.Println(l)
	l = l.PushBack(40)
	l = l.PushBack(50)
	fmt.Println(l)
	l = l.Remove()
	l = l.Remove()
	fmt.Println(l)
	l.Iterate(inc)
	fmt.Println(l)
	l3 := l.Collect(gt40)
	fmt.Println(l3)
	l2 := l.Collect(lt100)
	fmt.Println(l2)	
}
