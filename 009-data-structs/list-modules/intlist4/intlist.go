/*
 * Integer Single Linked List as OOP
 */

package intlist

import "strconv"

type Node struct {
	elem    int
	// invalid recursive type: Node refers to itself
	//next    Node
	next *Node
}

type List struct {
	fst *Node
}

func (this List) IsEmpty() bool {
  return (this.fst == nil)
}

func (this List) Len() int {
  if (this.IsEmpty()) {
	  return 0;
  } else {
	  return 1 + List{this.fst.next}.Len()
  }
} 

func (this List) GetElement() int {
	return this.fst.elem
}

func  (this List) FrontElement() int {
	return this.fst.elem
}


func  (this List) FrontElementTest() (int,bool) {
	if (!this.IsEmpty()) {
		return this.fst.elem,true
	} else {
		return 0,false
	}	
}

func (this List) GetNext() List {
	return List{this.fst.next}
}

func  (this List) Next() List {
	return List{this.fst.next}
}

func  (this List) NextTest() (List,bool) {
	if (!this.IsEmpty()) {
		return List{this.fst.next},true
	} else {
		return List{nil},false
	}	
}

func (this List) PushFront(elem int) List {
	aux := new(Node)
	aux.elem = elem
	aux.next = this.fst
	return List{aux}
}


func (this List) PushBack(elem int) List {

	//      var prev *Node = nil
	//      var cur  *Node = this
	//	for  !cur.IsEmpty() {
	//		prev = cur
	//		cur  = cur.GetNext()
	//	}
	var prev,cur  List
	// Search last node
	for  cur = this ;!cur.IsEmpty(); prev, cur = cur, cur.GetNext() {}
	// Create new node
	aux := new(Node)
	aux.elem = elem
	aux.next = nil
	// Link new node
	if !prev.IsEmpty() { 
		prev.fst.next = aux
		return this
	} else {
		return List{aux}
	}
}

func (this List) Remove() List {
	return List{this.fst.next}
}

func (this List) ToString() string {
	if (this.IsEmpty()) {
		return "{}"
	} else {
		return strconv.Itoa(this.fst.elem)+"->"+List{this.fst.next}.ToString()
	}
}

func (this List) String() string {
	return this.ToString()
}


func (this List) Includes(elem int) bool {
	if (this.IsEmpty()) {
		return false
	} else {
		return  (this.FrontElement()==elem)||((this.GetNext()).Includes(elem))
	}
}	

func (this List) Iterate(fp func(int)) {
	aux := List{this.fst}
	for (!aux.IsEmpty()) {
		(fp(aux.fst.elem))
		aux = aux.Next()
	}
}

func (this List) Apply(fp func(int)int) {
	aux := List{this.fst}
	for (!aux.IsEmpty()) {
		aux.fst.elem = (fp(aux.fst.elem))
		aux = aux.Next()
	}
}

func (this List) Collect(fp func(int)bool) List{
 var coll List	
  for ;!this.IsEmpty(); this = this.Next() {
	  if (fp(this.fst.elem)) {
		  coll = coll.PushFront(this.fst.elem)
	  }
  }
  return coll	
}



