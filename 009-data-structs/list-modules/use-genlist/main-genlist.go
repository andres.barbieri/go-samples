package main

import "fmt"
import "reflect" //typeOf
import L "list" 


func main() {
	l := L.New()

	var l2 L.List
	l2 = L.New()
	L.PushFront(&l2,1)
	
	fmt.Println(L.ToString(l),L.Len(l))	
	L.PushFront(&l,1)

	L.PushFront(&l,2)
	L.PushFront(&l,3)
	L.PushBack(&l,3)
	L.PushBack(&l,"HOLA")
	L.PushBack(&l,1.34)	
	fmt.Println(L.IsEmpty(l))
	fmt.Println(L.ToString(l), L.Len(l))
	L.Remove(&l)
	val := L.Remove(&l)
	fmt.Println(">>>",val,"<<<")
	fmt.Println(">>>",reflect.TypeOf(val),"<<<")	
	//invalid operation: val + 10 (mismatched types any and int)
	//val = val + 10
	//cannot convert val (variable of type any) to type int: need type assertion
	//val = int(val) + 10
	fmt.Println(">>>",val.(int),"<<<") // Type Assertion
	val  = val.(int) + 10 // Type Assertion
	fmt.Println(">>>",val,"<<<")
        //panic: interface conversion: interface {} is int, not float32
	//fmt.Println(">>>",val.(float32),"<<<")
	t, ok := val.(float32)
	if (ok == true) {
		fmt.Println("float32",t)
	} else {
		fmt.Println("error, not float32")
	}
        ////////////////////////////////////////////
	fmt.Println(L.ToString(l),L.Len(l))
	//for aux:=l;!L.IsEmpty(aux);aux=L.Next(aux) {
	for aux:=l;aux!=nil;aux=L.Next(aux) {		
		fmt.Println(L.FrontElement(aux))
	}
	fmt.Println((L.Node(*l)))
}

