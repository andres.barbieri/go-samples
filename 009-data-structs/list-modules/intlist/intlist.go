/*
 * Integer Single Linked List as ADT, not as OOP
 */

package intlist

//import "fmt"
import "strconv"

type Node struct {
	elem    int
	next    *Node
}

type List *Node

// We can discard/ignore this
func New() List {
	return (List(nil));
}

func IsEmpty(self List) bool {
  return (self == nil)
}

func Len(self List) int {
  if (IsEmpty(self)) {
      return 0;
    } else {
    return 1 + Len(Next(self));
  }
} 

func FrontElement(self List) int {
	return self.elem
}

func Next(self List) List {
  return self.next
}

func ToString(self List) string {
	if (IsEmpty(self)) {
		return "{}"
	} else {
		return strconv.Itoa(self.elem)+"->"+ToString(Next(self))
	}
}

//invalid receiver type List (pointer or interface type
//func (self List) String() string {
//	return ToString(self)
//
//}

func String(self List) string {
	return ToString(self)

}

func PushFront(self *List, elem int) {
	aux := new(Node)
	aux.elem = elem
	aux.next = *self
	*self = aux
}

func PushBack(self *List, elem int) {
	if (IsEmpty(*self)) {
		aux := new(Node)
		aux.elem = elem
		aux.next = nil
		*self = aux
	} else {
		PushBack( (*List)(&((*self).next)) ,elem )
	}
}


func Remove(self *List) int {
	elem := (*self).elem
	// If we need to do dispose/free	
	//aux  := (*self)
	//free(aux)

	// (List) cast not  needed
	//*self = (List)(((*self).next))
	*self = ((*self).next)
	return elem;
}

func Iterate(self List, fp func(int)int) {
  aux := self;
  for (!IsEmpty(aux)) {
      aux.elem = (fp(aux.elem));
      aux = Next(aux);
  }
}

