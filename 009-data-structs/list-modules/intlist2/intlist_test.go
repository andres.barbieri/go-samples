/*
 *  intlist Test
 */

package intlist

import "fmt"
import "testing"

func dec(x int) int {
	return x-1
}

func TestGenList(tt *testing.T) {
	l := New()
	var inc = func(x int) int { return x+1 }
	
	fmt.Println(ToString(l),Len(l))	
	PushFront(&l,1)

	PushFront(&l,2)
	PushFront(&l,3)
	PushBack(&l,3)
	PushBack(&l,2)
	PushBack(&l,1)	
	fmt.Println(IsEmpty(l))
	fmt.Println(ToString(l), Len(l))
	Iterate(l, inc)
	fmt.Println(ToString(l), Len(l))
	Iterate(l, dec)
	fmt.Println(ToString(l), Len(l))	
	Remove(&l)
	Remove(&l)
	fmt.Println(ToString(l),Len(l))
	//for aux:=l;!IsEmpty(aux);aux=Next(aux) {
	var aux List
	var i int
	// panic: runtime error: invalid memory
	//for i,aux = 0, l  ; i < 10; i,aux  = i+1 , Next(aux) {
	//	fmt.Println(FrontElement(aux))
	//}

	//for i,aux = 0, l; i < 10 ; i, aux, _  = i+1 , NextTest(aux) {			
	for i,aux = 0, l; i < 10 ; aux, _  = NextTest(aux) {		
		fmt.Println(FrontElementTest(aux))
		i++
	}
	for ii, auxl := 0,l ; ii<10 ; auxl,_=NextTest(auxl) {		
		fmt.Println(FrontElementTest(auxl))
		ii++
	}
	Push(&l,1000,2000,3000)
	fmt.Println(String(l))
}

