/*
 * Integer Single Linked List as ADT, not as OOP
 * Impl2, with test operations and variadic Push

 */
package intlist

//import "fmt"
import "strconv"

type Node struct {
	elem    int
	next    *Node
}

type List *Node

func New() List {
	return (List(nil));
}

func IsEmpty(self List) bool {
  return (self == nil)
}

func Len(self List) int {
  if (IsEmpty(self)) {
      return 0;
    } else {
    return 1 + Len(Next(self));
  }
} 

func FrontElement(this List) int {
	return this.elem
}

func FrontElementTest(this List) (int,bool) {
	if (!IsEmpty(this)) {
		return this.elem,true
	} else {
		return 0,false
	}
}

func Next(this List) List {
  return this.next
}

func NextTest(this List) (List,bool) {
	if (!IsEmpty(this)) {
		return this.next,true
	} else {
		return nil,false
	}	
}

func ToString(this List) string {
	if (IsEmpty(this)) {
		return "{}"
	} else {
		return strconv.Itoa(this.elem)+"->"+ToString(Next(this))
	}
}

func String(this List) string {
	return ToString(this)

}

func Push(this *List, elems ...int) {
	for _, e := range elems {
		PushFront(this,e)
	}
}

func PushFront(this *List, elem int) {
	aux := new(Node)
	aux.elem = elem
	aux.next = *this
	*this = aux
}

func PushBack(this *List, elem int) {
	if (IsEmpty(*this)) {
		aux := new(Node)
		aux.elem = elem
		aux.next = nil
		*this = aux
	} else {
		PushBack( (*List)(&((*this).next)) ,elem )
	}
}


func Remove(this *List) int {
	elem := (*this).elem
	*this = ((*this).next)
	return elem;
}

func RemoveTest(this *List) (int,bool) {
	if (IsEmpty(*this)) {
		return 0,false
	}	
	elem := (*this).elem
	*this = ((*this).next)
	return elem,true
}

func Iterate(this List, fp func(int)int) {
  aux := this;
  for (!IsEmpty(aux)) {
      aux.elem = (fp(aux.elem));
      aux = Next(aux);
  }
}

