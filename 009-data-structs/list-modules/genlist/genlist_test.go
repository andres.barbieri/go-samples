/*
 * genlist test
 */

package genlist

import "fmt"
import "reflect" //typeOf
import "testing"

func TestGenList(tt *testing.T) {
	l := New()

	var l2 List
	l2 = New()
	PushFront(&l2,1)
	
	fmt.Println(ToString(l),Len(l))	
	PushFront(&l,1)

	PushFront(&l,2)
	PushFront(&l,3)
	PushBack(&l,3)
	PushBack(&l,"HOLA")
	PushBack(&l,1.34)	
	fmt.Println(IsEmpty(l))
	fmt.Println(ToString(l), Len(l))
	Remove(&l)
	val := Remove(&l)
	fmt.Println(">>>",val,"<<<")
	fmt.Println(">>>",reflect.TypeOf(val),"<<<")	
	//invalid operation: val + 10 (mismatched types any and int)
	//val = val + 10
	//cannot convert val (variable of type any) to type int: need type assertion
	//val = int(val) + 10
	fmt.Println(">>>",val.(int),"<<<") // Type Assertion
	val  = val.(int) + 10 // Type Assertion
	fmt.Println(">>>",val,"<<<")
        //panic: interface conversion: interface {} is int, not float32
	//fmt.Println(">>>",val.(float32),"<<<")
	t, ok := val.(float32)
	if (ok == true) {
		fmt.Println("float32",t)
	} else {
		fmt.Println("error, not float32")
	}
        ////////////////////////////////////////////
	fmt.Println(ToString(l),Len(l))
	//for aux:=l;!IsEmpty(aux);aux=Next(aux) {
	for aux:=l;aux!=nil;aux=Next(aux) {		
		fmt.Println(FrontElement(aux))
	}
	fmt.Println((Node(*l)))
}

