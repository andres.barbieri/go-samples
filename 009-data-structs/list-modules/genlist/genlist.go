package genlist

import "fmt"

type Node struct {
	elem    any // interface{}
	next    *Node
}

type List *Node

func New() List {
	return (List(nil));
}

func IsEmpty(self List) bool {
  return (self == nil)
}

func Len(self List) int {
  if (IsEmpty(self)) {
      return 0;
    } else {
    return 1 + Len(Next(self));
  }
} 

func FrontElement(self List) any {
	return self.elem
}

func FrontElementTest(self List) (any,bool) {
	if (!IsEmpty(self)) {
		return self.elem,true
	} else {
		return 0,false
	}
}

func Next(self List) List {
  return self.next
}

func NextTest(self List) (List,bool) {
	if (!IsEmpty(self)) {
		return self.next,true
	} else {
		return nil,false
	}	
}

func PushFront(self *List, elem any) {
	aux := new(Node)
	aux.elem = elem
	aux.next = *self
	*self = aux
}

func PushBack(self *List, elem any) {
	if (IsEmpty(*self)) {
		aux := new(Node)
		aux.elem = elem
		aux.next = nil
		*self = aux
	} else {
		PushBack( (*List)(&((*self).next)) ,elem )
	}
}

func ToString(l List) string {
	if (IsEmpty(l)) {
		return "[]"
	} else {
		//return "an element" +"->"+ToString(Next(l))
		s := fmt.Sprintf("(%s)",l.elem)
		return s +"->"+ToString(Next(l))
	}
}

//invalid receiver type List (pointer or interface type)
//func (l List) String() string {
func (n Node) String() string {	
	return ToString(&n)
}

func Remove(self *List) any {
	elem := (*self).elem
	//aux  := (*self)
	*self = (List)(((*self).next))
	return elem;
}



