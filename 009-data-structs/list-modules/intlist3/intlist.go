/*
 * Integer Single Linked List as OOP
 */

package intlist

import "strconv"

type Node struct {
	elem    int
	// invalid recursive type: Node refers to itself
	//next    Node
	next *Node
}


func (this *Node) IsEmpty() bool {
  return (this == nil)
}

func (this *Node) Len() int {
  if (this.IsEmpty()) {
      return 0;
    } else {
    return 1 + this.next.Len()
  }
} 

func (this *Node) GetElement() int {
	return this.elem
}

func  (this *Node) FrontElement() int {
	return this.elem
}

func  (this *Node) FrontElementTest() (int,bool) {
	if (!this.IsEmpty()) {
		return this.elem,true
	} else {
		return 0,false
	}	
}

func (this *Node) GetNext() *Node {
	return this.next
}

func  (this *Node) Next() *Node {
	return this.next
}

func  (this *Node) NextTest() (*Node,bool) {
	if (!this.IsEmpty()) {
		return this.next,true
	} else {
		return nil,false
	}	
}

// Copy parameter does not work
func (this *Node) PushFrontNO(elem int) {
	aux := new(Node)
	aux.elem = elem
	aux.next = this
	this = aux
}

func (this *Node) PushFront(elem int) *Node {
	aux := new(Node)
	aux.elem = elem
	aux.next = this
	return aux
}

func (this *Node) PushBack(elem int) *Node {

	//      var prev *Node = nil
	//      var cur  *Node = this
	//	for  !cur.IsEmpty() {
	//		prev = cur
	//		cur  = cur.GetNext()
	//	}
	var prev,cur  *Node
	// Search last node
	for  prev,cur = nil, this ;!cur.IsEmpty(); prev, cur = cur, cur.GetNext() {}
	// Create new node
	aux := new(Node)
	aux.elem = elem
	aux.next = nil
	// Link new node
	if !prev.IsEmpty() { 
		prev.next = aux
		return this
	} else {
		return aux
	}
}

func (this *Node) Remove() *Node {
	return this.next
}

func (this *Node) ToString() string {
	if (this.IsEmpty()) {
		return "{}"
	} else {
		return strconv.Itoa(this.elem)+"->"+this.next.ToString()
	}
}
func (this *Node) String() string {
	return this.ToString()
}

func (this *Node) Iterate(fp func(int)int) {
  aux := this
  for (!aux.IsEmpty()) {
      aux.elem = (fp(aux.elem))
      aux = aux.Next()
  }
}

func (this *Node) Collect(fp func(int)bool) *Node{
 var coll *Node = nil	
  for ;!this.IsEmpty(); this = this.Next() {
	  if (fp(this.elem)) {
		  coll = coll.PushFront(this.elem)
	  }
  }
  return coll	
}

