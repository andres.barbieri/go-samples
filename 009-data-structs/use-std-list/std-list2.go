// See: https://pkg.go.dev/container/list
//      https://cs.opensource.google/go/go/+/refs/tags/go1.20.2:src/container/list/list.go

package main

import "container/list"
import "fmt"


type Algo struct {
	x int
	s string
}

func main() {
	// Create a new list and put some numbers in it.
	l := list.New()
	e4 := l.PushBack(4)
	e1 := l.PushFront(1)
	l.InsertBefore(3, e4)
	l.InsertAfter(2, e1)
	var algo Algo
	algo.s = "pepe"
	l.PushFront(algo)
	l.PushFront("hola")
	l.PushBack(45.35)
	// Iterate through list and print its contents.
	for e := l.Front(); e != nil; e = e.Next() {
		fmt.Println(e.Value)
	}
	l.Remove(e4)
	fmt.Println(l)
	//cannot convert (l.Front()).Value (variable of type any) to type string: need type assertion
        //var s string = string( (l.Front()).Value )

	// A type assertion provides access to an interface value's underlying
	// concrete value.
        // t := i.(T)
	// t, ok := i.(T) // if not ok, ok == false
	var s string = ( (l.Front()).Value ).(string)
        fmt.Println(s)

	if s, ok := ( (l.Front()).Value ).(string); ok {
		/* act on int */
		fmt.Println(s)
	} else {
		/* not int */
		fmt.Println("not string")
	}
	
	//panic: interface conversion: interface {} is string, not int
	//var i int = ( (l.Front()).Value ).(int)
        //fmt.Println(i)

	if i, ok := ( (l.Front()).Value ).(int); ok {
		/* act on int */
		fmt.Println(i)
	} else {
		/* not int */
		fmt.Println("not int")
	}

	
}
