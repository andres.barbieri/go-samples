package main

import  (
	"fmt"
	"time"
	"log"
	//"io"
	"bufio"
	"os"
)

type Bird struct {
	Species     string    `txt:"species"`
	Count       int       `txt:"count"`
	Description string    `txt:"description"`
	CreatedAt   time.Time `txt:"createdAt"`
}

func main() {
	fileName := "input/birds.txt"
        var birds []Bird = make([]Bird,0)
	var bird  Bird
	
	if (len(os.Args) > 1) {
		fileName = os.Args[1]
	}
	txtFile, err := os.Open(fileName)
	defer txtFile.Close()
	
	// if we os.Open returns an error then handle it
	if (err != nil) {
		log.Println(err)
		os.Exit(1)
		//panic(err)
	}

	scanner := bufio.NewScanner(txtFile)
	result := true
	count  := 1
	
	for ( (result) && (err == nil) && (count!=0) ) {
		var strbuf string
		var intbuf int
		
		//// first field, Species ////
		result := scanner.Scan()
		err  = scanner.Err()
		count,_ = fmt.Sscanf(scanner.Text(),"%s",&strbuf)
		if ((!result)||(err != nil)||(count == 0)) {
			//log.Println(err)
			continue
			//log.Fatal(err)
		}
		bird.Species = strbuf
		//// second field, Count ////
		result = scanner.Scan()
		err  = scanner.Err()
		count,_ = fmt.Sscanf(scanner.Text(),"%d",&intbuf)
		if ((!result)||(err != nil)||(count == 0)) {
			//log.Println(err)
			continue
			//log.Fatal(err)
		}
		bird.Count = intbuf
		//// third field, Desc ////
		result = scanner.Scan()
		err  = scanner.Err()
		count,_ = fmt.Sscanf(scanner.Text(),"%s",&strbuf)
		if ((!result)||(err != nil)||(count == 0)) {		
			//log.Println(err)
			continue
			//log.Fatal(err)

		}		
		bird.Description = strbuf
		//// last field, Date ////
		result = scanner.Scan()
		err  = scanner.Err()
		count,_ = fmt.Sscanf(scanner.Text(),"%s",&strbuf)
		if ((!result)||(err != nil)||(count == 0)) {
			//log.Println(err)
			continue
			//log.Fatal(err)

		}
		bird.CreatedAt, err = time.Parse("2006-01-02",strbuf)
		if ((result)&&(err == nil)&&(count > 0)) { birds = append(birds,bird) }
	}
	//// Show results ////
	fmt.Println(birds)
}
