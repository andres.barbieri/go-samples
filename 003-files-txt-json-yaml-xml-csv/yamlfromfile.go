package main

import  (
	"fmt"
	"time"
	"log"
	"gopkg.in/yaml.v2"
	"io"
	"os"
)

type Bird struct {
	Species     string    `yaml:"species"`
	Count       int       `yaml:"count"`
	Description string    `yaml:"description"`
	CreatedAt   time.Time `yaml:"createdAt"`
}

func main() {

	// fileName := "input\\birds.yml"
	fileName := "input/birds.yml"
	if (len(os.Args) > 1) {
		fileName = os.Args[1]
	}
	yamlFile, err := os.Open(fileName)
	defer yamlFile.Close()
	
	// if we os.Open returns an error then handle it
	if (err != nil) {
		log.Println(err)
		os.Exit(1)
		//panic(err)
	}

	// read our opened yamlFile as a byte array.
	byteValue, _ := io.ReadAll(yamlFile)
	
	var birds []Bird
	err =  yaml.Unmarshal([]byte(byteValue), &birds)
	fmt.Println(err, birds)
}
