package main

import  (
	"fmt"
	"time"
	"log"
	//"io"
	"bufio"
	"os"
	"strconv"
	"strings"
)

type Bird struct {
	Species     string    `txt:"species"`
	Count       int       `txt:"count"`
	Description string    `txt:"description"`
	CreatedAt   time.Time `txt:"createdAt"`
}

func main() {
	fileName := "input/birds.csv"
        var birds []Bird = make([]Bird,0)
	var bird Bird
        fieldSeparator := [...]string{";",",",":"}
	
	if (len(os.Args) > 1) {
		fileName = os.Args[1]
	}
	
	csvFile, err := os.Open(fileName)
	defer csvFile.Close()
	
	// if we os.Open returns an error then handle it
	if (err != nil) {
		log.Println(err)
		os.Exit(1)
		//panic(err)
	}

	reader := bufio.NewReader(csvFile)
	var line string
	var lineSlice []string
	
	for ( err == nil ) {	
		line, err = reader.ReadString('\n')
		line = strings.TrimSuffix(line, "\n")
		//fmt.Println(line)
		if err != nil {
			//log.Println(err)
			//log.Fatal(err)
			continue
		}
		//lineSlice = strings.Split(line, ";")
		//lineSlice = strings.Split(line, fieldSeparator[0])
		// Try to discover field separator
		lineSlice = nil
		for i:=0;(i<len(fieldSeparator))&&(len(lineSlice)!=4);i++ {
			lineSlice = strings.Split(line, fieldSeparator[i])
		}
		if (len(lineSlice) == 4) {
			bird.Species = lineSlice[0]
			bird.Count, err = strconv.Atoi(lineSlice[1])
			bird.Description = lineSlice[2]		
			bird.CreatedAt, err = time.Parse("2006-01-02",lineSlice[3])
			birds = append(birds,bird) 
		}
	}
	fmt.Println(birds)
}
