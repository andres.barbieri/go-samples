package main

import  "fmt"
import  "time"
import  "log"
//import "container/list"
import  "encoding/json"
import  "io"
import  "os"


type Bird struct {
	Species     string
	Description string
	CreatedAt   time.Time
}

func main() {
	//birdJson := `{"species": "pigeon","description": "likes to perch on rocks", "createdAt": "2021-10-18T11:08:47.577Z"}`

	jsonFile, err := os.Open("input/bird.json")

	// if we os.Open returns an error then handle it
	if (err != nil) {
		log.Println(err)
	}

	// read our opened jsonFile as a byte array.
	byteValue, _ := io.ReadAll(jsonFile)
	
	var bird Bird
	//json.Unmarshal([]byte(birdJson), &bird)
	fmt.Println(err, bird)	
	err = json.Unmarshal([]byte(byteValue), &bird)
	fmt.Println(err, bird)
	// {pigeon likes to perch on rocks 2021-10-18 11:08:47.577 +0000 UTC}
}

