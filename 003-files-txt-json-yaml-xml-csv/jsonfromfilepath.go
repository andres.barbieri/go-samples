package main

import  (
	"fmt"
	"time"
	"log"
	"encoding/json"
	"io"
	"os"
	"path/filepath"
)

type Bird struct {
	Species     string   `json:"species"`
	Count       int
	Description string
	CreatedAt   time.Time
}

func main() {
	
	fileName := filepath.Join("input","birds.json")
	//fileName := filepath.FromSlash("input/birds.json")
	//fileName := "input/birds.json"
	//fileName := "input\\birds.json"
	if (len(os.Args) > 1) {
		fileName = os.Args[1]
	}
	jsonFile, err := os.Open(fileName)
	defer jsonFile.Close()
	
	// if we os.Open returns an error then handle it
	if (err != nil) {
		log.Println(err)
		os.Exit(1)
		//panic(err)
	}

	// read our opened jsonFile as a byte array.
	byteValue, _ := io.ReadAll(jsonFile)
	
	var birds []Bird
	err = json.Unmarshal([]byte(byteValue), &birds)
	fmt.Println(err, birds)
}
