package main

import  (
	"fmt"
	"time"
	"log"
	"encoding/json"
	"encoding/xml"	
	//"io/ioutil" //deprecated
	"io"
	"os"
)

type Bird struct {
	Species     string   `json:"species"`
	Count       int
	Description string
	CreatedAt   time.Time
}

func main() {
	
	fileName := "input/birds.json"
	if (len(os.Args) > 1) {
		fileName = os.Args[1]
	}
	jsonFile, err := os.Open(fileName)
	defer jsonFile.Close()
	
	// if we os.Open returns an error then handle it
	if (err != nil) {
		log.Println(err)
		os.Exit(1)
		//panic(err)
	}

	// read our opened jsonFile as a byte array.
	//byteValue, _ := ioutil.ReadAll(jsonFile)
	byteValue, _ := io.ReadAll(jsonFile)
	
	var birds []Bird
	err = json.Unmarshal([]byte(byteValue), &birds)
	byteValue, err =  xml.Marshal(birds)
	//fmt.Println(err, birds)
	fmt.Println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>")
	fmt.Println("<Birds>")
	fmt.Println(string(byteValue))
	fmt.Println("</Birds>")
}
