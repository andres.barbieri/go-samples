package main

import  (
	"fmt"
	"time"
	"log"
	"encoding/xml"
	"io"
	"os"
)

type Bird struct {
	XMLName     xml.Name  `xml:"bird"`
	Species     string    `xml:"species,attr"`
	Count       int       `xml:"count,attr"`
	Description string    `xml:"description"`  
	CreatedAt   time.Time `xml:"createdAt"`
}

type Birds struct {
	XMLName     xml.Name  `xml:"birds"`
	Birds       []Bird    `xml:"bird"`
}

func main() {

	fileName := "input/birds.xml"
	if (len(os.Args) > 1) {
		fileName = os.Args[1]
	}
	xmlFile, err := os.Open(fileName)
	defer xmlFile.Close()
	
	// if we os.Open returns an error then handle it
	if (err != nil) {
		log.Println(err)
		os.Exit(1)
	}

	// read our opened xmlFile as a byte array.
	byteValue, _ := io.ReadAll(xmlFile)
	
	var birds Birds
	err =  xml.Unmarshal([]byte(byteValue), &birds)
	fmt.Println(err, birds.Birds)
}
