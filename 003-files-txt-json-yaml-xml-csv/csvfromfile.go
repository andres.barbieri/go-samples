package main

import  (
	"fmt"
	"time"
	"log"
	"encoding/csv"
	//"io/ioutil" or "io"
	"os"
	"strconv"
)

type Bird struct {
	Species     string   `csv:"species"`
	Count       int
	Description string
	CreatedAt   time.Time
}

func main() {
	
	fileName := "input/birds.comma.csv"
	var birds []Bird = make([]Bird,0)
	var bird Bird
	
	if (len(os.Args) > 1) {
		fileName = os.Args[1]
	}
	csvFile, err := os.Open(fileName)
	defer csvFile.Close()
	
	// if we os.Open returns an error then handle it
	if (err != nil) {
		log.Println(err)
		os.Exit(1)
		//panic(err)
	}
	// read our opened csvFile as a byte array.
	csvReader := csv.NewReader(csvFile)
	records, err := csvReader.ReadAll() // return [][]string 
	if err != nil {
		log.Fatal("Unable to read CSV file ", err)
	}
	for _, record := range records {
		//for _,field := range record {
		//	fmt.Println(field)
		//}
		bird.Species = record[0]
		bird.Count, err = strconv.Atoi(record[1])
		bird.Description = record[2]		
		bird.CreatedAt, err = time.Parse("2006-01-02",record[3])
		birds = append(birds,bird) 
	}
	fmt.Println(err, birds)
}
