2006-01-02

Go’s time formatting unique and different than what you would do in other languages. Instead of having a conventional format to print the date, Go uses the reference date 20060102150405 which seems meaningless but actually has a reason, as it’s 1 2 3 4 5 6 in the Posix date command:

Mon Jan 2 15:04:05 -0700 MST 2006
0   1   2  3  4  5              6
In Go layout string is:

Jan 2 15:04:05 2006 MST
1   2  3  4  5    6  -7


invoking:

go run txtfromfileorstdin.go < input/birds.txt

go run txtfromfileorstdin.go input/birds.txt

go run txtfromfileorstdin.go 

go run jsonfromfile.go 

or:

go run jsonfromfile.go  input/birds.json

CSV: comma-separated values

go run craftcsvfromfile.go 

or:

go run craftcsvfromfile.go input/{birds.colon.csv | birds.comma.err2.csv | birds.csv  | birds.err.csv | birds.comma.csv  | birds.comma.err.csv | birds.err2.csv}



