/*
 * Programa para ilustrar los no controles en "C"
 * entender su uso, funcionamiento y problemas posibles. 
 *
 */

#include <stdint.h>
#include <stdio.h>
int main()
{
    
    int32_t  i1;  
    uint32_t i2; // = 0xffffffff;
    
    i2 = 0xffffffff;
    i2 = i2 + 1; 
    printf("%16X\n",i2);
    
    i2 = 0;
    i2 = i2 -1; 
    printf("%16X\n",i2);
    
    i1 = 0xffffffff; 
    printf("%16X\n",i1);
    
    i1 = -0xffffffff;
    printf("%16X\n",i1);

    return 0;
}
