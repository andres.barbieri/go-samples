#include <stdio.h>

/*
 * Programa que declara variables de diferentes tipos b'asicos
 * e inspecciona los tama~nos de los tipos
 * 
 * Ver tama~nos en include/limits.h 
 *
 * NOTES:
 *
 * Compilar con gcc -m32|-m64 ...  -m96bit-long-double
 * Otras opciones:
 * -m128bit-long-double -m96bit-long-double -mlong-double-64 -mlong-double-80 
 *
 */

/* Unsigned raw binary 0..2^(n)-1          */
/* Signed   Ca2 binary -2^(n-1)..2^(n-1)-1 */

/* Float       IEEE 754 single precision   */
/* Double      IEEE 754 double precision   */
/* Long Double IEEE 754 double precision   */

/* Try -Wall and without it */

int main()
{
   
                 int i1;
  unsigned       int i2; 
  signed         int i3;
           short int i4;         
	   long  int i5;         
  unsigned short int i6;
  signed   short int i7;
  signed   long  int i8;
  unsigned long  int i9;
  long     long  int i10;
  /* long long long int i11; */  /* `long long long' is too long for GCC */

  unsigned           ia; /* Default int */
  signed             ib;
  long               ic;
  short              id;

                     char c1;
  signed             char c2;
  unsigned           char c3;
           /*short*/ char c4; /* short modifier NOT applied to chars */
           /*long*/  char c5; /* long modifier  NOT applied to chars */

               float f1;
  /*unsigned*/ float f2; /* long, short, signed or unsigned invalid for float*/
      
	       double d1;
  /*unsigned*/ double d2; /* short, signed or unsigned invalid for double */
  long         double d3; /* the only valid combination is long double */   
 	    
  size_t              ss;
  
  // printf("sizeof(void)          = %6d\n",(int) sizeof(void)); What does it mean?
  printf("sizeof(char)          = %6d\n",(int) sizeof(char));
  printf("sizeof(int)           = %6d\n",(int) sizeof(int));
  printf("sizeof(long int)      = %6d\n",(int) sizeof(long int));
  printf("sizeof(long long int) = %6d\n",(int) sizeof(long long int));
  printf("sizeof(short int)     = %6d\n",(int) sizeof(short int));
  printf("sizeof(float)         = %6d\n",(int) sizeof(float));
  printf("sizeof(double)        = %6d\n",(int) sizeof(double));
  printf("sizeof(long double)   = %6d\n",(int) sizeof(long double));
  printf("sizeof(int*)          = %6d\n",(int) sizeof(int*));
  printf("sizeof(size_t)        = %6d\n",(int) sizeof(size_t)); 
  printf("sizeof(var d3)        = %6d\n",(int) sizeof(d3));  
  return(0);
}
