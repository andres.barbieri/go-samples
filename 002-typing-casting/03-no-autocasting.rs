/*
 * Programa para ilustrar las conversiones en Rust
 * entender su uso, funcionamiento y problemas posibles. 
 *
 */

// Suppress all warnings from casts which overflow.
#![allow(overflowing_literals)]
#[allow(unused_mut)]
#[allow(unused_variables)]

fn main()
{
    
    let mut i1:i32;  
    let mut i2:u32; // = 0xffffffff;
    
    let mut i3:i16;          
    let mut i4:i64;
    
    let mut c1:i8;
    let mut c2:u8; 

    let mut f1:f32;
    let mut d1:f64;
    
    i2 = 0xffffffff;
    //i1 = i2; // expected `i32`, found `u32`
    i1 = i2 as i32;
    println!("Cast uint int");
    println!("int ({})   = uint ({})",i1, i2);
    // i2 = i1; // expected `u32`, found `i32`    
    i2 = i1 as u32;
    println!("uint ({})   = int ({})",i2, i1);
    println!("uint ({:X})   = int ({:X})",i2, i1);
    
    i1 = 0x0fffffff;
    //i2 = i1;
    i2 = i1 as u32;
    
    println!("uint ({})   = int ({}) \n",i2, i1);
    println!("uint ({:X})   = int ({:X}) \n",i2, i1);
    
    println!("***");
    println!("Cast sint int, uint");
    i1 = 0xffff0000;
    i2 = 0xffff0000;
    //i3 = i1; //expected `i16`, found `i32`
    i3 = i1 as i16;
    
    println!("sint ({})   = int ({})\n",i3,i1);
    println!("sint ({:X})   = uint({:X})\n",i3,i2);
    println!("sint ({})   = int ({})\n",i3,i1);
    println!("sint ({:X})   = uint({:X})\n",i3,i2);

    i1 = -0x000f;
    i2 =  0x000f;
    // i3 = i1; expected `i16`, found `i32`
    i3 = i1 as i16;
    println!("sint ({})   = int ({})\n",i3,i1);
    println!("sint ({:X})   = int ({:X})\n",i3,i1);
    // i3 = i2; expected `i16`, found `u32`
    i3 = i2 as i16;
    println!("sint ({})   = uint({})\n",i3,i2);
    println!("sint ({:X})   = uint({:X})\n",i3,i2);
    println!("***\n");
    
    i3 = 0xffff;
    //i1 = i3; expected `i32`, found `i16`
    i1 = i3 as i32; 
    //i2 = i3; expected `u32`, found `i16`
    i2 = i3 as u32;
    println!("int  ({})   = sint({})",i1,i3);
    println!("int  ({:X})   = sint({:X})",i1,i3);
    println!("uint ({})   = sint({})",i2,i3);
    println!("uint ({:X})   = sint({:X})",i2,i3);
    println!("***");
    
   
    println!("Cast byte int , int byte , char byte\n");
    i1 = 0xff00;
    //c1 = i1; expected `i8`, found `i32`

    c1 = i1 as i8;
    println!("byte ({})     = int ({})",c1,i1);
    println!("byte ({:X})   = int ({:X})",c1,i1);
    
    // c1 = 'a'; expected `i8`, found `char`
    c1 = 'a' as i8;

    //i1 = c1; expected `i32`, found `i8
    i1 = c1 as i32;
    println!("int  ({})   = char({})({:?})",i1,c1,std::char::from_u32(c1 as u32));

    //c1 = '}' as i8;
    c1 = '}' as i8;
    //c1 = c1 + 10; Runtime error: thread 'main' panicked at 'attempt to add with overflow'
    c1 = c1 + 1;
    
    //c2 = c1 + 10;
    c2 = c1 as u8 + 10;
    println!("char ({})   = uchar({})",c1,c2);
    println!("***");
    println!("Cast int float, float int");

    f1 = 134.56;
    // i1 = f1;  expected `i32`, found `f32`
    i1 = f1 as i32;
    println!("int  ({})   = float({})",i1,f1);
    println!("int  ({})   = float({})",i1,f1);
    // f1 = i1; expected `f32`, found `i32`
    f1 = i1 as f32;
    println!("float({})   = int  ({})",f1,i1);  

    println!("***");
    println!("Autocast float double, double float \n");
    d1 = 134.56;
    //f1 = d1; expected `f32`, found `f64`
    f1 = d1 as f32;
    println!("float ({})   = double ({})\n",f1,d1);
    //d1 = f1;  expected `f64`, found `f32`
    d1 = f1 as f64;
    println!("double({})   = float  ({})\n",d1,f1);
}
