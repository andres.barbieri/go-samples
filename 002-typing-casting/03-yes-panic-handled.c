/*
 * Programa para ilustrar los no controles en "C"
 * entender su uso, funcionamiento y problemas posibles. 
 *
 * Try compiling with:
 *
 * gcc -ftrapv 03-yes-panic.c 
 */

#include <stdint.h>
#include <stdio.h>

#include <signal.h>

void handler(int i/*signal*/)
{
  printf("Overflow'd\n!");
}

  
int main()
{
    
    int32_t  i1;  
    uint32_t i2; // = 0xffffffff;


    // when we get a SIGABRT, call handler
    signal(SIGABRT, &handler);
    
    i2 = 0xffffffff;
    i2 = i2 + 1; 
    printf("%16X\n",i2);
    
    i2 = 0;
    i2 = i2 -1; 
    printf("%16X\n",i2);
    
    i1 = 0xffffffff; 
    printf("%16X\n",i1);
    
    i1 = 0x7fffffff;
    printf("%16X %d\n",i1,i1);    
    //i1 = i1 + 1;
    printf("%16X %d\n",i1,i1);

    i1 = 0x80000000;
    printf("%16X %d\n",i1,i1);    
    i1 = i1 - 1;
    printf("%16X %d\n",i1,i1);
    
    return 0;
}
