#include <stdio.h>

/*
 * Programa que declara literales de diferentes tipos
 */

int main()
{
   
  printf("%ld\n",12 // decimal
	 + 0xAF  // hex
	 + 0b1010 // bin
	 + 034  + 034 // octal
	 + 12L  // long
	 + 23U  // unsigned
	 + 34UL // unsigned long
	 + 3400 
	 );

  printf("%f\n",(314E-2
	 + 3.14159
	 + 314159e-5
	 + .1416	 
	 + 3
	 + 0.031415e+2)/5
	 );
  return(0);
}
