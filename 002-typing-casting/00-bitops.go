package main

import "fmt"

func main() {
	var a uint8 = 0b10100001  // 0xA1
	var b uint8 = 0b01000001

	fmt.Printf("a=%08b, a=%X, b=%08b, b=%X\n",a,a,b,b)
	fmt.Printf("a|b=%08b\n",a|b);
	fmt.Printf("a&b=%08b\n",a&b)
	fmt.Printf("a^b=%08b\n",a^b)
	//fmt.Printf("!(a^b)=%08b\n",!(a^b)) //invalid operation: operator ! not defined on (a ^ b) (value of type uint8)
	//fmt.Printf("~(a^b)=%08b\n",~(a^b)) //cannot use ~ outside of interface or type constraint
	fmt.Printf("^(a^b)=%08b\n",^(a^b))
	fmt.Printf("b>>1=%08b\n",b>>1)
	fmt.Printf("b<<1=%08b\n",b<<1) 	
}
