package main

import "fmt"

type num_t int32

const (
   cero = 0	
   uno  = 1
   dos  = 2
   tres = 3
)

/**
const (
   cero = iota	
   uno  
   dos   
   tres  
)

const (
   cero = iota + 1
   uno   
   dos   
   tres  
)
**/

// Stringer func (work as toString())
// map enum to string
// %!v(PANIC=String method: runtime error: index out of range [N] with length 4)
func (n num_t) String() string {
	return [...]string{"cero", "uno", "dos", "tres"}[n]
}

type weight_t float64
type height_t float64

func main() {
	// var w weight_t = 34.22
	var w weight_t = 34.22
	var h height_t = 34.34

	var x num_t

	//w = w + h * 34 // mismatched types weight_t and height_t
	w = w + weight_t(h) * weight_t(34) 
	fmt.Printf("%f.3\n",w)

	fmt.Scanln(&h,&w)
	fmt.Println(h*height_t(w))
	
	x = 0
	if (x == cero) { fmt.Printf("Equals\n") }
	if (x == 0)    { fmt.Printf("Equals\n") }
	x = 1
	if (x == uno)  { fmt.Printf("Equals\n") }
	
	fmt.Println(cero,uno,dos,tres)
	
	fmt.Println(x)
	fmt.Println("--->",x)
	{	
		fmt.Scanln(&x)
		fmt.Println(x)
		fmt.Println("-->",x,x)
	}
}

