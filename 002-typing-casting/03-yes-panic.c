/*
 * Programa para ilustrar los no controles en "C"
 * entender su uso, funcionamiento y problemas posibles. 
 *
 * Try compiling with:
 *
 * gcc -ftrapv 03-yes-panic.c 
 *
 * Or:
 *
 * gcc -Wall -fsanitize=undefined 03-yes-panic.c
 *           -fsanitize=signed-integer-overflow
 * runtime error: signed integer overflow: -2147483648 - 1 cannot be represented * in type 'int'
 *
 *
 */

#include <stdint.h>
#include <stdio.h>

int main()
{
    
    int32_t  i1;  
    uint32_t i2; // = 0xffffffff;

    i2 = 0xffffffff;
    i2 = i2 + 1; 
    printf("%16X\n",i2);

    return 0;
    
    i2 = 0;
    i2 = i2 -1; 
    printf("%16X\n",i2);
    
    i1 = 0xffffffff; 
    printf("%16X\n",i1);
    
    i1 = 0x7fffffff;
    printf("%16X %d\n",i1,i1);    
    //i1 = i1 + 1;
    printf("%16X %d\n",i1,i1);

    i1 = 0x80000000;
    printf("%16X %d\n",i1,i1);    
    i1 = i1 - 1;
    printf("%16X %d\n",i1,i1);
    
    return 0;
}
