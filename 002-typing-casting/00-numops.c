#include <stdio.h>


//#define FMT "%d"
#define FMT "%f"

int main() {
  //int a = 20;
  //int b = 30;

  float a  = 20.1;
  float b  = 30.2;

  a++;	
  printf("a++="FMT"\n",a);
  a--;
  printf("a--="FMT"\n",a);
  a+=2;
  printf("a+2="FMT"\n",a);	
  printf("a+b="FMT"\n",a+b);
  printf("a-b="FMT"\n",a-b);	
  printf("a*b="FMT"\n",a*b);
  printf("a/b="FMT"\n",a/b);
  //printf("a%%b="FMT"\n",a%b); //error: invalid operands to binary % (have ‘float’ and ‘float’)
}
