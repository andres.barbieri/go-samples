fn main()
{
    let     a:u8 = 0b10100001;  // 0xA1
    let     b:u8 = 0b01000001;

    // ! bitwise and logical are the same !!!! 
    println!("a={:0>8b}, a={:0>2X}, b={:0>8b}, b={:0>2X}",a,a,b,b);
    print!("a|b={:0>8b}\n",a|b);
    print!("a&b={:0>8b}\n",a&b);
    print!("a^b={:0>8b}\n",a^b);
    print!("!(a^b)={:0>8b}\n",!(a^b)); 
    //print!("~(a^b)={:0>8b}\n",~(a^b));  //use `!` to perform bitwise not
    print!("~(a^b)={:0>8b}\n",!(a^b));  
    // print!("^(a^b)={:0>8b}\n",^(a^b)); //expected expression
    print!("^(a^b)={:0>8b}\n",!(a^b)); //expected expression 
    print!("b>>1={:0>8b}\n",b>>1);
    print!("b<<1={:0>8b}\n",b<<1); 	
}

