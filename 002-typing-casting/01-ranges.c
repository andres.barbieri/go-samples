#include <stdio.h>
#include <limits.h> // Implementation specific values C99 std
#include <float.h>  // Implementation specific values proposed by ANSI C

/*
 * Programa que muestra algunos l'imites de los tipos de datos
 * dependientes de la plataforma
 *
 * NOTES:
 * Compilar con -m32 o -m64
 *
 * Ver tama~nos en limits.h 
 * http://pubs.opengroup.org/onlinepubs/009604499/basedefs/limits.h.html
 * 
 * http://pubs.opengroup.org/onlinepubs/009695399/basedefs/float.h.html
 * is float.h std ?
 */

int main()
{

  printf("Sht. Integer interval:      %d..%d\n",SHRT_MIN,SHRT_MAX);
  printf("Integer interval:           %d..%d\n",INT_MIN,INT_MAX);
  printf("Integer interval:           %X..%X\n",INT_MIN,INT_MAX);
  printf("Uns. Integer interval:      %u..%u\n",0,UINT_MAX);
  printf("Uns. Integer interval:      %X..%X\n",0,UINT_MAX);  
  printf("Long Integer interval:      %ld..%ld\n",LONG_MIN,LONG_MAX);
  printf("Long Long Integer interval: %lld..%lld\n",LLONG_MIN,LLONG_MAX);
  printf("******\n");
  printf("Float interval:             %f..%f\n",FLT_MIN,FLT_MAX);
  printf("Float interval:             %60.63f..%2f\n",FLT_MIN,FLT_MAX);
  printf("Float interval:             %e..%e\n",FLT_MIN,FLT_MAX);
  printf("Dobule interval:            %f..%f\n",DBL_MIN,DBL_MAX);
  printf("Dobule interval:            %e..%e\n",DBL_MIN,DBL_MAX);
  return (0);
}


