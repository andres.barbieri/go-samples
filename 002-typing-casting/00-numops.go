package main

import "fmt"

func main() {
	//var a int = 20
	//var b int = 30

	var a float32 = 20.1;
	var b float32  = 30.2;

	a++	
	fmt.Println("a++=",a)
	a--
	fmt.Println("a--=",a)
	a+=2
	fmt.Println("a+2=",a)	
	fmt.Println("a+b=",a+b)
	fmt.Println("a-b=",a-b)	
	fmt.Println("a*b=",a*b)
	fmt.Println("a/b=",a/b)
	//fmt.Println("a%b=",a%b) // invalid operation: operator % not defined on a (variable of type float32)
}
