use std::mem;

fn main()
{
    //ints
    let i1: u8;  // byte
    let i2: i8;
    let i3: u16;
    let i4: i16;
    let i5: u32;
    let i6: i32;
    let i7: u64;    
    let i_8: i64;

    let x1: usize; // equivalent C size_t
    let x2: isize;
    
    // chars

    let c: char = 'z'; // unicode 32bits/4bytes

    // bool

    let b: bool = false;
    
    // floats

    let f1: f32;
    let d1: f64;    
    //let d2: f128;

    
    println!("size_of(nothing)      = {}", mem::size_of::<()>());    
    println!("size_of(i8)           = {}", mem::size_of::<i8>());
    println!("size_of(u8)           = {}", mem::size_of::<u8>());
    println!("size_of(i16)          = {}", mem::size_of::<i16>());        
    println!("size_of(u16)          = {}", mem::size_of::<u16>());    
    println!("size_of(i32)          = {}", mem::size_of::<i32>());        
    println!("size_of(u32)          = {}", mem::size_of::<u32>());    
    println!("size_of(i64)          = {}", mem::size_of::<i64>());        
    println!("size_of(u64)          = {}", mem::size_of::<u64>());

    println!("size_of(usize)        = {}", mem::size_of::<usize>());         
    println!("size_of(isize)        = {}", mem::size_of::<isize>());

    println!("size_of(f32)          = {}", mem::size_of::<f32>());        
    println!("size_of(f64)          = {}", mem::size_of::<f64>());
    
    println!("size_of(&i32)         = {}",mem::size_of::<&i32>());


    println!("size_of(bool)         = {}",mem::size_of::<bool>());
    println!("size_of(char)         = {} {}",mem::size_of::<char>(),c);
    
    //println!("size_of(var x2)         = {}",mem::size_of::type_of(x1)());    
}
