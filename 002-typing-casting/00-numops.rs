fn main()
{
    //let mut a:i32 = 20;
    //let     b:i32 = 30;

    let mut a:f32 = 20.1;
    let     b:f32 = 30.2;
    
    //a++; //not a valid postfix operator
    //a+=1; no implementation for `f32 += {integer}`
    a+=1.0;
    println!("a++={}",a);
    //a--;
    a-=1.0;
    //a-=1; no implementation for `f32 += {integer}`
    println!("a--={}",a);
    //a+=2; no implementation for `f32 += {integer}`
    println!("a+2={}",a);	
    println!("a+b={}",a+b);
    println!("a-b={}",a-b);	
    println!("a*b={}",a*b);
    println!("a/b={}",a/b);
    println!("a%b={}",a%b); 
}
