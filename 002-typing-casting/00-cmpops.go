package main

import "fmt"

func main() {
	var a int = 20
	var b int = 30

	fmt.Println("!(a<b||b!=a) ",!(a<b||b!=a))
	fmt.Println("!(a>=b&&b==a)",!(a>=b&&b==a))
	fmt.Println("!((a>=b)&&(b==a))",!((a>=b)&&(b==a)))			
}
