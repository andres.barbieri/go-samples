/*
 * Programa que declara literales de diferentes tipos
 */

package main

import "fmt"

func main() {   
	fmt.Printf("%d\n",12 + 0xAF + 0b1010 + 0o34 + 034 + 12  + 23  + 34 + 3_400)
	fmt.Printf("%f\n",(314E-2 + 3.14159 + 314159e-5 + .1416 + 3 + 0.031415e+2)/5)
	fmt.Println('a' == 97)
	fmt.Println('a' == '\141')
	fmt.Println('a' == '\x61')
	fmt.Println('a' == '\u0061')
	fmt.Println('a' == '\U00000061')
	fmt.Println(0x61 == '\x61')
	fmt.Println('\u4f17' == '众')
}
