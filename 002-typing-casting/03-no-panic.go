/*
 *
 */
package main

import "fmt"

func main() {
    
	var i1 int32  
	var i2 uint32 // = 0xffffffff
    
	i2 = 0xffffffff
	fmt.Println(i2)	
	i2 = i2 + 1 
	fmt.Println(i2)		
	i2 = 0
	i2 = i2 -1 

	i1 = 0x7fffffff
	fmt.Println(i1)		
	i1 = i1 + 2
	fmt.Println(i1)
	
    //i1 = 0xffffffff // cannot use 0xffffffff (untyped int constant 4294967295) as int32 value in assignment (overflows)

    //i1 = -0xffffffff // cannot use -0xffffffff (untyped int constant -4294967295) as int32 value in assignment (overflows)
}
