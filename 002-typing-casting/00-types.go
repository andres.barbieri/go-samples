package main

import "fmt"
import "unsafe"

func main() {
	//ints
	var i1  uint8;  // byte // char
	//var b   byte;   // char
	var i2  int8;
	var i3  uint16;
	var i4  int16;
	var i5  uint32;
	var i6  int32;
	var i7  uint64;    
	var i8  int64;

	var x1  uint; // equivalent C size_t
	var x2  int;
    
	// chars
        
	// floats
	var f1  float32;
	var d1  float64;    
	
	fmt.Printf("size_of(i8)           = %d\n", unsafe.Sizeof(i2));
	fmt.Printf("size_of(u8)           = %d\n", unsafe.Sizeof(i1));
	fmt.Printf("size_of(i16)          = %d\n", unsafe.Sizeof(i4));        
	fmt.Printf("size_of(u16)          = %d\n", unsafe.Sizeof(i3));    
	fmt.Printf("size_of(i32)          = %d\n", unsafe.Sizeof(i6));        
	fmt.Printf("size_of(u32)          = %d\n", unsafe.Sizeof(i5));    
	fmt.Printf("size_of(i64)          = %d\n", unsafe.Sizeof(i8));        
	fmt.Printf("size_of(u64)          = %d\n", unsafe.Sizeof(i7));

	fmt.Printf("size_of(usize)        = %d\n", unsafe.Sizeof(x2));         
	fmt.Printf("size_of(isize)        = %d\n", unsafe.Sizeof(x1));

	fmt.Printf("size_of(f32)          = %d\n", unsafe.Sizeof(f1));        
	fmt.Printf("size_of(f64)          = %d\n", unsafe.Sizeof(d1));
	
	fmt.Printf("size_of(&i8)          = %d\n",unsafe.Sizeof(&i2));
	fmt.Printf("size_of(&i32)         = %d\n",unsafe.Sizeof(&i6));	
}
