#include <stdio.h>

inline static void println_bin(unsigned char value)
{
    for (int i = sizeof(char) * 7; i >= 0; i--)
        printf("%d", (value & (1 << i)) >> i );
    putc('\n', stdout);
}

int main() {
  unsigned char a = 0b10100001;  // 0xA1
  // char a = 0b10100001; // Dup sign/high order bit
  unsigned char b = 0b01000001;

  //printf("a|b=%08b a=%X\n",a|b,a); warning: unknown conversion type character ‘b’ in format [-Wformat=]
  printf("a=%02X, b=%02X\n",a,b); 
  printf("a|b=%02X a|b=",a|b);
  println_bin(a|b);  
  printf("a&b=%02X\n",a&b);
  printf("a^b=%02X\n",a^b);
  printf("~(a^b)=%02X\n", (~(a^b)));  // Dup sign/high order bit
  printf("~(a^b)=%02X\n",(unsigned char) (~(a^b)));
  printf("!(a^b)=%02X\n",!(a^b));
  printf("b>>1=%02X\n",b>>1);
  printf("b<<1=%02X\n",b<<1);
}
