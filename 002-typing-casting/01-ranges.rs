/*
 * Programa que muestra algunos l'imites de los tipos de datos
 * dependientes de la plataforma y no.
 *
 */

fn main()
{

    println!("Sht. Integer interval:      {}..{}",i16::MIN,i16::MAX);
    println!("Integer interval:           {}..{}",i32::MIN,i32::MAX);
    println!("Uns. Integer interval:      {}..{}",u32::MIN,u32::MAX); 
    println!("Uns. Integer interval:      {}..{}",usize::MIN,usize::MAX);
    println!("Long Integer interval:      {}..{}",i64::MIN,i64::MAX);
    println!("******");
    println!("Float interval:             {}..{}\n",f32::MIN,f32::MAX);
    println!("Dobule interval:            {}..{}\n",f64::MIN,f64::MAX);
    println!("Min abs non zero float32:   {}\n", f32::MIN_POSITIVE,);
}


