/*
 * Programa que muestra algunos l'imites de los tipos de datos
 * dependientes de la plataforma y no.
 *
 */

package main

import "fmt"
import "math"

func  main() {

	fmt.Printf("Byte interval:              %d..%d\n",math.MinInt8,math.MaxInt8)	
	fmt.Printf("Sht. Uns. Integer interval: %d..%d\n",0,math.MaxUint16)	
	fmt.Printf("Integer interval:           %d..%d\n",math.MinInt32,math.MaxInt32)
	fmt.Printf("Integer interval:           %d..%d\n",math.MinInt,math.MaxInt)		
	fmt.Printf("Uns. Integer interval:      %d..%d\n",0,math.MaxUint32)	
	//fmt.Printf("Uns. Integer interval:      %d..%lld\n",0,math.MaxUint) //  overflow. Issue here is that the constant is untyped.
	fmt.Printf("Uns. Integer interval:      %d..%d\n",0,(uint64(math.MaxUint))) //  overflow. Issue here is that the constant is untyped.
	fmt.Printf("Long Integer interval:      %d..%d\n",math.MinInt64,math.MaxInt64)	
	fmt.Printf("******\n");
	fmt.Printf("Float interval:             %f..%f\n",-math.MaxFloat32,math.MaxFloat32)	
	fmt.Printf("Dobule interval:            %f..%f\n",-math.MaxFloat64,math.MaxFloat64)
	fmt.Printf("Min abs non zero float32:   %60.63f\n", math.SmallestNonzeroFloat32)
}


