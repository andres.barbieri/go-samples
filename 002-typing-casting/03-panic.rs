/*
 * Programa para ilustrar los controles en Rust
 * entender su uso, funcionamiento y problemas posibles. 
 *
 * in debug mode, arithmetic (+, -, etc.) on signed and unsigned primitive 
 * integers is checked for overflow, panicking if it occurs.
 *
 * in release mode, overflow is not checked and is specified to wrap as 
 * two's complement.
 *
 * Debug mode:   rustc, cargo build [--debug]
 * Release mode: rustc -O, rustc -C opt-level=3, o cargo build --relsease 
 *
 *
 */

#[allow(overflowing_literals)]
#[allow(arithmetic_overflow)]
//#[allow(unused_mut)]
#[allow(unused_variables)]
#[allow(unused_assignments)]

fn main()
{
    
    let mut i1:i32;  
    let mut i2:u32; // = 0xffffffff;
    
    i2 = 0xffffffff;
    i2 = i2 + 1; // attempt to compute `u32::MAX + 1_u32`, which would overflow
    // panicked at 'attempt to add with overflow

    i2 = 0;
    i2 = i2 -1; // attempt to compute `0_u32 - 1_u32`, which would overflow
    // panicked at 'attempt to add with overflow

    i1 = 0xffffffff; // the literal `0xffffffff` (decimal `4294967295`) does not fit into the type `i32` and will become `-1i32`

    i1 = -0xffffffff; //the literal `0xffffffff` (decimal `4294967295`) does not fit into the type `i32`nd the value `-0xffffffff` will become `1i32` 
}
