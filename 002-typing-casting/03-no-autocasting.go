/*
 * Programa para ilustrar las conversiones en Go
 * entender su uso, funcionamiento y problemas posibles. 
 *
 */

package main

import "fmt"

func main() {    
	var i1 int32  
	var i2 uint32 // = 0xffffffff
	
	var i3 int16          
	//var i4 int64
	
	var c1 int8
	var c2 uint8 
	
	var f1 float32
	var d1 float64
    
	i2 = 0xffffffff
	//i1 = i2 // cannot use i2 (variable of type uint32) as int32 value in assignment
	i1= int32(i2)
	
	fmt.Printf("Cast uint int\n")
	fmt.Printf("int (%d)   = uint (%d)\n",i1, i2)
	//i2 = i1 // cannot use i1 (variable of type int32) as uint32 value in assignment
	i2 = uint32(i1) 
	fmt.Printf("uint (%d)   = int (%d)\n",i2, i1)
	fmt.Printf("uint (%X)   = int (%X)\n",i2, i1)

	i1 = 0x0fffffff
	//i2 = i1 `
	i2 = uint32(i1)
	
	fmt.Printf("uint (%d)   = int (%d) \n",i2, i1)
	fmt.Printf("uint (%X)   = int (%X) \n",i2, i1)
    
	fmt.Printf("***\n")
	fmt.Printf("Cast sint int, uint\n")
	//i1 = 0xffff0000 // cannot use 0xffff0000 (untyped int constant 4294901760) as int32 value in assignment (overflows)
	i2 = 0xffff0000
        //i3 = i1 
        i3 = int16(i1)
    
	fmt.Printf("sint (%d)   = int (%d)\n",i3,i1)
	fmt.Printf("sint (%X)   = uint(%X)\n",i3,i2)
	fmt.Printf("sint (%d)   = int (%d)\n",i3,i1)
	fmt.Printf("sint (%X)   = uint(%X)\n",i3,i2)

	i1 = -0x000f
	i2 =  0x000f
	// i3 = i1 // cannot use i1 (variable of type int32) as int16 value in assignment

	i3 = int16(i1)
	fmt.Printf("sint (%d)   = int (%d)\n",i3,i1)
	fmt.Printf("sint (%X)   = int (%X)\n",i3,i1)
        //i3 = i2 //cannot use i2 (variable of type uint32) as int16 value in assignment
        i3 = int16(i2)
	fmt.Printf("sint (%d)   = uint(%d)\n",i3,i2)
	fmt.Printf("sint (%X)   = uint(%X)\n",i3,i2)
	fmt.Printf("***\n")
    
	//i3 = 0xffff // cannot use 0xffff (untyped int constant 65535) as int16 value in assignment (overflows)
	// i1 = i3 //cannot use i3 (variable of type int16) as int32 value in assignment  
	i1 = int32(i3) 
	//i2 = i3  // cannot use i3 (variable of type int16) as uint32 value in assignment
	i2 = uint32(i3)
	fmt.Printf("int  (%d)   = sint(%d)\n",i1,i3)
	fmt.Printf("int  (%X)   = sint(%X)\n",i1,i3)
	fmt.Printf("uint (%d)   = sint(%d)\n",i2,i3)
	fmt.Printf("uint (%X)   = sint(%X)\n",i2,i3)
	fmt.Printf("***\n")
    
   
	fmt.Printf("Cast byte int , int byte , char byte\n")
	i1 = 0xff00 
	// c1 = i1 // cannot use i1 (variable of type int32) as int8 value in assignment 
	c1 = int8(i1)
	fmt.Printf("byte (%d)     = int (%d)\n",c1,i1)
	fmt.Printf("byte (%X)   = int (%X)\n",c1,i1)
	
	c1 = 'a' 
	c1 = int8('a') 

	// i1 = c1 // cannot use c1 (variable of type int8) as int32 value in assignment
	i1 = int32(c1)
	fmt.Printf("int  (%d)   = char(%c)\n",i1,c1)

        c1 = '}' 
	c1 = int8('}')
        c1 = c1 + 10 
        c1 = c1 + 1
    
        // c2 = c1 + 10 cannot use c1 + 10 (value of type int8) as uint8 value in assignment
	
	c2 = uint8(c1) + 10
	fmt.Printf("char (%d)   = uchar(%d)\n",c1,c2)
	fmt.Printf("***\n")
	fmt.Printf("Cast int float, float int\n")

	f1 = 134.56
        //i1 = f1  
	i1 = int32(f1)
	fmt.Printf("int  (%d)   = float(%f)\n",i1,f1)
	fmt.Printf("int  (%d)   = float(%f)\n",i1,f1)
	// f1 = i1 
	f1 = float32(i1)
	fmt.Printf("float(%f)   = int  (%d)\n",f1,i1)  

	fmt.Printf("***\n")
	fmt.Printf("Autocast float double, double float \n")
	d1 = 134.56
        // f1 = d1 // cannot use d1 (variable of type float64) as float32 value in assignment
	f1 = float32(d1)
	fmt.Printf("float (%f)   = double (%f)\n",f1,d1)
        // d1 = f1  // cannot use f1 (variable of type float32) as float64 value in assignment
        d1 = float64(f1)
	fmt.Printf("double(%f)   = float  (%f)\n",d1,f1)
}
