// 1lwtrhead/routine 3tasks (modules) -- sequential
package main

import (
	"fmt"
	"time"
)

const LOOPS  =   30
const MSLEEP =  200
const DELTA  =   20

func routine_test(id int) {
	for i := 0; i < LOOPS; i++ {
		fmt.Println("I am the routine ",id)
		time.Sleep(MSLEEP * time.Millisecond)
	}
	fmt.Println("Routine:",id," finished")
}

func main() {
	routine_test(0)
	routine_test(1)
	for i := 0; i < LOOPS; i++ {
		fmt.Println("I am the main ")
		time.Sleep((MSLEEP+DELTA) * time.Millisecond)
	}
	fmt.Println("Bye")
}
