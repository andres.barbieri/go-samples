// https://go.dev/ref/spec
//
// Program execution begins by initializing the program and then invoking
// the function main in package main. When that function invocation returns,
// the program exits. It does not wait for other (non-main) goroutines
// to complete.
	
package main

import (
	"fmt"
	"time"
)

const LOOPS  =   30
const MSLEEP =  200
const DELTA  =    0
const TOP    =   20

func routine_test(id int) {
	for i := 0; i < LOOPS; i++ {
		fmt.Println("I am the routine ",id)
		time.Sleep(MSLEEP * time.Millisecond)
	}
	fmt.Println("Routine:",id," finished")
}

func main() {
	for i := 0; i < TOP; i++ {
		go routine_test(i)
	}
	for i := 0; i < LOOPS; i++ {
		fmt.Println("I am the main")
		time.Sleep((MSLEEP+DELTA) * time.Millisecond)
	}
	fmt.Println("Bye")
}
