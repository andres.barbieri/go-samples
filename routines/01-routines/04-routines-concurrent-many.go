// Count finished tasks
// go run 03-routines-concurrent-many-nowaitenough.go | grep "finish" | wc -l
// go run 04-routines-concurrent-many.go | grep "finish" | wc -l

package main

import (
	"fmt"
	"time"
)

const LOOPS  =   30
const MSLEEP =  200
const DELTA  =   50
const TOP    =   20

func routine_test(id int) {
	for i := 0; i < LOOPS; i++ {
		fmt.Println("I am the routine ",id)
		time.Sleep(MSLEEP * time.Millisecond)
	}
	fmt.Println("Routine:",id," finished")
}

func main() {
	for i := 0; i < TOP; i++ {
		go routine_test(i)
	}
	for i := 0; i < LOOPS; i++ {
		fmt.Println("I am the main")
		time.Sleep((MSLEEP+DELTA) * time.Millisecond)
	}
	fmt.Println("Bye")
}
