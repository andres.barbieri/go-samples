// 3lwthreads/routines - 3 tasks -- concurrent
// Check interleavings
// go run 02-routines-concurrent.go > 1.run
// go run 02-routines-concurrent.go > 2.run
// diff 1.run 2.run

/***
SpeedUp

time go run 01-routines-seq-routine.go > /dev/null 

real	0m18,773s
user	0m0,112s
sys	0m0,089s

time go run 02-routines-concurrent.go > /dev/null 

real	0m6,730s
user	0m0,128s
sys	0m0,071s

Speed_up = 18.773/6.730 = 2.7894

***/

package main

import (
	"fmt"
	"time"
)

const LOOPS  =   30
const MSLEEP =  200
const DELTA  =   20

func routine_test(id int) {
	for i := 0; i < LOOPS; i++ {
		fmt.Println("I am the routine ",id)
		time.Sleep(MSLEEP * time.Millisecond)
	}
	fmt.Println("Routine:",id," finished")
}

func main() {
	go routine_test(0)
	go routine_test(1)
	for i := 0; i < LOOPS; i++ {
		fmt.Println("I am the main")
		time.Sleep((MSLEEP+DELTA) * time.Millisecond)
	}
	fmt.Println("Bye")
}
