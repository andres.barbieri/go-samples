// 1lwthread/routine - 3tasks -- sequential
package main

import (
	"fmt"
	"time"
)

const LOOPS  =   20
const MSLEEP =  200

func main() {
	for i := 0; i < LOOPS; i++ {
		fmt.Println("I am the main as routine ",0)
		time.Sleep(MSLEEP * time.Millisecond)
	}

	for i := 0; i < LOOPS; i++ {
		fmt.Println("I am the main as routine ",1)
		time.Sleep(MSLEEP * time.Millisecond)
	}	
	for i := 0; i < LOOPS; i++ {
		fmt.Println("I am the main")
		time.Sleep(MSLEEP * time.Millisecond)
	}
	fmt.Println("Bye")
}
