package main

import (
	"fmt"
	"math/rand"
	"time"
	"sync"
)


const LOOP   =   1
const MSLEEP = 100

type Msg struct {
	data int
	id   int
}
var msg   = make(chan Msg)

//var done  = make(chan int)
var done sync.WaitGroup

//https://go.dev/blog/concurrency-timeouts
func produce(id int) {
	timeout := make(chan bool, 1)
	go func() {
		time.Sleep(10 * time.Second)
		timeout <- true
	}()
	for i := 0; i < LOOP; i++ {
		// produce
		fmt.Println("Producing ... :",id)
		v := rand.Intn(100)+1
		// wait
		w := rand.Intn(5)
		time.Sleep((time.Duration(w)+MSLEEP) * time.Millisecond)
		// Send msg
                select {
		case msg <- Msg{v, id}:
			fmt.Println("Producer:",id)
		case <- timeout:
			// giving up
			fmt.Println("Producer:",id," giving up")
		}
	}
	done.Done()
	fmt.Println("Producer:",id," has finished")
}

func produce2(id int) {
	for i := 0; i < LOOP; i++ {
		// produce
		fmt.Println("Producing ... :",id)
		v := rand.Intn(100)+1
		// wait
		w := rand.Intn(5)
		time.Sleep((time.Duration(w)+MSLEEP) * time.Millisecond)
		// Send msg
                select {
		case msg <- Msg{v, id}:
			fmt.Println("Producer:",id)
		 case <-time.After(time.Second * 10):
			// giving up
			fmt.Println("Producer:",id," giving up")
		}
	}
	done.Done()
	fmt.Println("Producer:",id," has finished")
}


func consume(id int) {
	for i := 0; i < LOOP; i++ { 
		// We are late
		time.Sleep(time.Duration(10) * time.Second)
		// Got msg
		v := <- msg
		fmt.Println("Consumer:", id, "got from:",v.id, "msg:",v.data)
		// consume
		time.Sleep(MSLEEP * time.Millisecond * time.Duration(v.data))
	}
	done.Done()
	fmt.Println("Consumer:",id," has finished")
}

func main() {
	rand.Seed(time.Now().UnixNano())
	done.Add(3)
	go produce2(0)
	go produce2(1)		
	go consume(0)
	//go consume(1) // If comment: Deadlock consumer exits and one producer is "deadlocked" 
	done.Wait()
	fmt.Println("DONE")
}
