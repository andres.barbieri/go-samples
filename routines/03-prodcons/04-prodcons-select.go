package main

import (
    "fmt"
    "math/rand"
    "time"
)

const LOOP = 10
type Msg struct {
	data int
	id   int
}

var msg1   = make(chan int)
var msg2   = make(chan int)
var done   = make(chan int)

func produce1(id int) {
	for i := 0; i < LOOP; i++ {
		// produce
		v := rand.Intn(100)+1
		// wait
		w := rand.Intn(5)
		time.Sleep(time.Duration(w) * time.Second)
		// Send msg
                msg1 <- v
		fmt.Println("Producer:",1)
	}
	done <- id
	fmt.Println("Producer-T1:",id," has finished")
}

func produce2(id int) {
	for i := 0; i < LOOP; i++ {
		// produce
		v := rand.Intn(100)+1
		// wait
		w := rand.Intn(5)
		time.Sleep(time.Duration(w) * time.Second)
		// Send msg
		fmt.Println("Producer:",2)
                msg2 <- v
	}
	done <- id
	fmt.Println("Producer-T2:",id," has finished")
}
func consume(id int) {
	var v int
	for i := 0; i <LOOP*2; i++ {
		select {
			case v = <- msg1:
			       fmt.Println("Consumer:", id, "from:", 1, "msg:",v)
			case v = <- msg2:
			       fmt.Println("Consumer:", id, "from:", 2, "msg:",v)
		}
	}
	done <- id
	fmt.Println("Consumer",id," has finished")
}

func main() {
	rand.Seed(time.Now().UnixNano())
	go produce1(0)	
	go produce2(1)
	go consume(0)
	for i:=0;i<3;i++ {
		<- done
	}
	fmt.Println("DONE")
}
