package main

import (
	"fmt"
	"math/rand"
	"time"
	"container/list"
	
)

const  TOP   =   4
const LOOP   =  10
const MSLEEP = 100
const MAX    =  10

type Msg struct {
	data int
	id   int
}

var putMsg   = make(chan Msg)
var getMsg   = make(chan Msg)
var done     = make(chan int)

func produce(id int) {
	//time.Sleep(MSLEEP * 10 * time.Millisecond)
	for i := 0; i < LOOP; i++ {
		// produce
		fmt.Println("Producing ... :",id)
		v := rand.Intn(100)+1
		// wait 
		w := rand.Intn(5)
		time.Sleep((time.Duration(w)+MSLEEP) * time.Millisecond)
		// Send msg
                putMsg <- Msg{v, id}
		fmt.Println("Producer:",id)
	}
	done <- id
	fmt.Println("Producer:",id," has finished")
}

func consume(id int) {
	fmt.Println("Consumer: ",id,"started")
	for i := 0; i < LOOP; i++ {
		// Got msg
		v := <- getMsg
		fmt.Println("Consumer:", id, "got from:",v.id, "msg:",v.data)
		// consume
		time.Sleep(MSLEEP * time.Millisecond * time.Duration(v.data))
	}
	done <- id
	fmt.Println("Consumer:",id," has finished")
}

func maybe(b bool, c chan Msg) chan Msg {
	if !b {
		return nil
	}
	return c
}

func showList(l *list.List) {
	for e := l.Front(); e != nil; e = e.Next() {
		fmt.Print(e.Value,",") 
	}
	fmt.Println()
}

func adm() {
	var buffer    *list.List = list.New()
	var tmpInMsg    Msg
	var tmpOutMsg   Msg = Msg{0,-1}
	fmt.Print("I",buffer.Len());showList(buffer)
	for {
		select {
		// Consume, getMsg	
			//when len>0	
		case maybe((buffer.Len()>0),getMsg) <- tmpOutMsg:
			buffer.Remove(buffer.Front())
			if (buffer.Len()>0) {tmpOutMsg = (buffer.Front().Value).(Msg)}
			//////////////////////////////////////
			fmt.Print("G",buffer.Len());showList(buffer)
			//////////////////////////////////////
		// Produce, putMsg	
			// when len<MAX
		case tmpInMsg = <- maybe((buffer.Len()<MAX),putMsg):
			if (buffer.Len() == 0) { tmpOutMsg = tmpInMsg }
			buffer.PushBack(tmpInMsg)
			//////////////////////////////////////
			fmt.Print("P",buffer.Len());showList(buffer)
			//////////////////////////////////////
		}
	}
}

func main() {
	rand.Seed(time.Now().UnixNano())
	go adm()
	for i:=0;i<TOP;i++ {
		go produce(i)	
		go consume(i)
	}
	for i:=0;i<TOP*2;i++ {
		<- done
	}
	fmt.Println("DONE")
}
