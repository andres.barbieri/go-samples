package main

import (
	"fmt"
	"math/rand"
	"time"
	"sync"
)

const TOP    =   4
const LOOP   =  10
const MSLEEP = 200
const MAX    =  10

type Msg struct {
	data int
	id   int
}

var msgs   = make(chan Msg,MAX)

//var done  = make(chan int)
var done sync.WaitGroup

func produce(id int) {
	for i := 0; i < LOOP; i++ {
		// produce
		fmt.Println("Producing ... :",id)
		v := rand.Intn(100)+1
		// wait
		w := rand.Intn(5)
		time.Sleep((time.Duration(w)+MSLEEP) * time.Millisecond)
		// Send msg
                msgs <- Msg{v, id}
		// msgs is an address
		//fmt.Println("P",msgs)
		fmt.Println("Producer:",id)
	}
	done.Done()
	fmt.Println("Producer:",id," has finished")
}

func consume(id int) {
	for i := 0; i < LOOP; i++ { 
		// We are late
		//time.Sleep(time.Duration(10) * time.Second)
		// Got msg
		msg := <- msgs
		//fmt.Println("G",msgs)
		fmt.Println("Consumer:", id, "got from:",msg.id, "msg:",msg.data)
		// consume
		time.Sleep(MSLEEP * time.Millisecond * time.Duration(msg.data))
	}
	done.Done()
	fmt.Println("Consumer:",id," has finished")
}

func main() {
	rand.Seed(time.Now().UnixNano())
	done.Add(TOP*2)
	//fmt.Println("I",msgs)
	for i:=0;i<TOP;i++ {
		go produce(i)	
		go consume(i)
	}
	done.Wait()
	fmt.Println("DONE")
}
