package main

import (
	"fmt"
	"math/rand"
	"time"
	"sync"
)

const TOP       =   8
const TOPPRODS  = TOP-5
const TOPCONS   = TOP-TOPPRODS
const LOOP      =   1
const MSLEEP    = 100

type Msg struct {
	data int
	id   int
}
var msg   = make(chan Msg)

//var done  = make(chan int)
var done               sync.WaitGroup
var countProds         sync.Mutex
var countProdsRun      int  = TOPPRODS

func produce(id int) {
	for i := 0; i < LOOP; i++ {
		// produce
		fmt.Println("Producing ... :",id)
		v := rand.Intn(100)+1
		// wait
		w := rand.Intn(5)
		time.Sleep((time.Duration(w)+MSLEEP) * time.Millisecond)
		// Send msg
                msg <- Msg{v, id}
		fmt.Println("Producer:",id)
	}
	countProds.Lock()
	countProdsRun--
	// Sender/Producer should close
	if (countProdsRun == 0) { close(msg) }
	countProds.Unlock()
	done.Done()
	fmt.Println("Producer:",id," has finished")
}

func consume(id int) {
	for i := 0; i < LOOP; i++ { 
		// We are late
		time.Sleep(time.Duration(10) * time.Second)
		// Got msg
		v, ok := <- msg
		if ok {
			fmt.Println("Consumer:", id, "got from:",v.id, "msg:",v.data)
			// consume
			time.Sleep(MSLEEP * time.Millisecond * time.Duration(v.data))
		} else {
			fmt.Println("channel closed")
		}
	}
	done.Done()
	fmt.Println("Consumer:",id," has finished")
}

func main() {
	rand.Seed(time.Now().UnixNano())
	done.Add(TOPPRODS+TOPCONS)
	for i:=0;i<TOPPRODS;i++ {go produce(i)} // TOPPRODS<TOPCONS
	for i:=0;i<TOPCONS;i++  {go consume(i)} 
	done.Wait()
	fmt.Println("DONE")
}
