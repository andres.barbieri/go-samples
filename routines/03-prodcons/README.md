Ejemplo de concurrencia con Productores/Consumidores en GO (golang)

* 00-prodcons-chan-infwait.go

Productores/Consumidores, Unbuffered Channel, reduce la concurrencia, pero funciona. Main() permite que ejecutar rutinas esperando en un waitgroup de forma infinita. Nadie lo "llama". Las rutinas terminan, y el programa dará un error de deadlock porque main() queda bloqueado.

* 01-prodcons-chan-waitchan.go

Similar a la anterior, pero el main() termina cuando todos los Productores/Consumidores se lo comunican mediante un canal para este propósito. Se debe saber la cantidad de rutinas a esperar por el main().


* 01-prodcons-shmem-waitchan.go

Productores/Consumidores, con memoria compartida accedida con Mutex y se avisan entre productores a los consumidores mediante variables Condition como monitores indicando estado ocupado o no el buffer de un solo elemento. También reduce la concurrencia, pero funciona. El main() termina de forma similar al anterior, con canal de espera.

* 02-prodcon-chan-waitgroup.go

Similar a 01-prodcons-chan-waitchan.go, pero en este caso el main() espera mediante un waitGroup.

* 03-prodcon-chan-waitgroup-deadlock.go

Similar a la anterior, 02-prodcon-chan-waitgroup.go, pero se lanzan menos consumidores, por lo tanto un productor quedará a la espera de que se reciba su producción y quedará en deadlock. Sucedería algo similar si se lanzaran menos productores, los consumidores se quedarían esperando y se generaría deadlock.

* 03-prodcon-chan-waitgroup-deadlock-avoid-close.go

Evita el deadlock producido eventualmente por lanzarse menos productores, TOPPRODS<TOPCONS, avisando a los consumidores el último productor que no esperen cerrando el canal y los consumidores chequean esta condición. Los que mandan cierran el canal con close().

* 03-prodcon-chan-waitgroup-deadlock-avoid-tmout.go

Menos consumidores, los productores evitan el deadlock con timeout. Se muestran dos técnicas. Select con timeout o select con tarea reloj que envía un mensaje al canal timeout. Los timeouts prematuros podrían generar deadlock al otro grupo, ya que no se deposita la producción suficiente para que los consumidores la reciban. Ver el siguiente ejemplo.

* 03-prodcon-chan-waitgroup-deadlock-short-tmout.go

Timeout demasiado corto, no esperan lo suficiente que se consuma su producción. Se genera deadlock.

* 03-prodcon-chan-waitgroup-deadlock-tmoutvsdefault.go

Muestra alternativa de Select con Default en lugar de timeout. Inmediatamente de no poder comunicarse sigue la ejecución. Generar deadlock, similar a timeout muy corto.

* 04-prodcons-select.go

Muestra uso de Select con opciones de poder recibir de diferentes tipos de productores.

* 05-prodcons-array-admchan-waitchan.go

Los Productores/Consumidores no se comunican directamente entre si sino que lo hacen mediante un administrador. El administrador hace seguro el acceso a la memoria compartida. En lugar de tener un buffer de un solo mensaje lo maneja con un arreglo. Se Generan guardas para hacer esperar si el buffer/arreglo esta lleno o vacío. Las guardas se generan con la técnica "channel-valued closure". Ver https://groups.google.com/g/golang-nuts/c/ChPxr_h8kUM y https://groups.google.com/g/golang-nuts/c/M2xjN_yWBiQ. Si bien go no tiene un mecanismo de guardas explícito se puede implementar como en le ejemplo con la creación de la función maybe(). 

* 06-prodcons-list-admchan-waitchan.go

Similar al anterior, 05-prodcons-array-admchan-waitchan.go, pero con una lista en lugar de manejar un arreglo de buffer.

* 07-prodcons-shmem-array-waitchan.go
* 07-prodcons-shmem-array-waitchan-mods.go

Productores/Consumidores con buffer en memoria compartida accedida mediante variables Condition y Mutex. La versión "-mods", encapsula las operaciones  de depositar y extraer en put_msg() y get_msg().

* 08-prodcon-buffered-waitgroup.go

Productores/Consumidores con buffered channels, solución más simple y legible.
