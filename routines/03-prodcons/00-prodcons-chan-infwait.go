package main

import (
	"fmt"
	"math/rand"
	"time"
	"sync"
)


const TOP    =   4
const LOOP   =  10
const MSLEEP = 100

type Msg struct {
	data int
	id   int
}

var msg   = make(chan Msg)

var wait sync.WaitGroup

func produce(id int) {
	for i := 0; i < LOOP; i++ {
		// produce
		fmt.Println("Producing ... :",id)
		v := rand.Intn(100)+1
		// wait
	        w := rand.Intn(5)
		time.Sleep((time.Duration(w)+MSLEEP) * time.Millisecond)
		// Send msg
		msg <- Msg{v, id}
		fmt.Println("Producer:",id)
	}
	fmt.Println("Producer:",id," has finished")
}

func consume(id int) {
	for i := 0; i < LOOP; i++ {
		// Got msg
		v := <-msg
		fmt.Println("Consumer:", id," got from:", v.id, "msg:",v.data)
		// consume
		time.Sleep(MSLEEP * time.Millisecond * time.Duration(v.data))
	}
	fmt.Println("Consumer:",id," has finished")
}

func main() {
	rand.Seed(time.Now().UnixNano())
	defer fmt.Println("DONE")
	wait.Add(1)

	for i:=0;i<TOP;i++ {
		go produce(i)	
		go consume(i)
	}
	wait.Wait()
}
