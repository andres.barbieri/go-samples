package main

import (
	"fmt"
	"math/rand"
	"time"
	"sync"
)


const LOOP   =   1
const MSLEEP = 100

type Msg struct {
	data int
	id   int
}
var msg   = make(chan Msg)

//var done  = make(chan int)
var done sync.WaitGroup

func produce(id int) {
	for i := 0; i < LOOP; i++ {
		// produce
		fmt.Println("Producing ... :",id)
		v := rand.Intn(100)+1
		// wait
		w := rand.Intn(5)
		time.Sleep((time.Duration(w)+MSLEEP) * time.Millisecond)
		// Send msg
                msg <- Msg{v, id}
		fmt.Println("Producer:",id)
	}
	done.Done()
	fmt.Println("Producer:",id," has finished")
}

func consume(id int) {
	for i := 0; i < LOOP; i++ { 
		// We are late
		time.Sleep(time.Duration(10) * time.Second)
		// Got msg
		v := <- msg
		fmt.Println("Consumer:", id, "got from:",v.id, "msg:",v.data)
		// consume
		time.Sleep(MSLEEP * time.Millisecond * time.Duration(v.data))
	}
	done.Done()
	fmt.Println("Consumer:",id," has finished")
}

func main() {
	rand.Seed(time.Now().UnixNano())

	for i:=0;i<3;i++ {
		done.Add(1)
	}	
	go produce(0)
	go produce(1)		
	go consume(0)
	//go consume(2) // If comment: Deadlock consumer exits and one producer is deadlocked 
	done.Wait()
	fmt.Println("DONE")
}
