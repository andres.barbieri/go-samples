package main

import (
    "fmt"
    "math/rand"
    "time"
)

const  TOP   =   4
const LOOP   =  10
const MSLEEP = 100
const MAX    =  10

type Msg struct {
	data int
	id   int
}

var putMsg   = make(chan Msg)
var getMsg   = make(chan Msg)
var done     = make(chan int)

func produce(id int) {
	//time.Sleep(MSLEEP * 10 * time.Millisecond)
	for i := 0; i < LOOP; i++ {
		// produce
		fmt.Println("Producing ... :",id)
		v := rand.Intn(100)+1
		// wait 
		w := rand.Intn(5)
		time.Sleep((time.Duration(w)+MSLEEP) * time.Millisecond)
		// Send msg
                putMsg <- Msg{v, id}
		fmt.Println("Producer:",id)
	}
	done <- id
	fmt.Println("Producer:",id," has finished")
}

func consume(id int) {
	fmt.Println("Consumer: ",id,"started")
	for i := 0; i < LOOP; i++ {
		// Got msg
		v := <- getMsg
		fmt.Println("Consumer:", id, "got from:",v.id, "msg:",v.data)
		// consume
		time.Sleep(MSLEEP * time.Millisecond * time.Duration(v.data))
	}
	done <- id
	fmt.Println("Consumer:",id," has finished")
}

func maybe(b bool, c chan Msg) chan Msg {
	if !b {
		return nil
	}
	return c
}

func adm() {
	var buffer [MAX]Msg
	var len       int = 0
	var putindex  int = 0
	var getindex  int = 0
	// Init buffer
	for i:=0;i<MAX;i++ {
		buffer[i].id   = -1
		buffer[i].data =  0
	}
	fmt.Println("I",buffer)
	for {
		select {
		// Consume, getMsg
			//when len>0
		case maybe((len>0),getMsg) <- buffer[getindex]:
			///////////////////////////////////////
			// Mark as empty (only for visualization purpose)
			buffer[getindex] = Msg{0,-1}
			fmt.Println("G",buffer)
			//////////////////////////////////////
			len--
			getindex = (getindex + 1) % MAX
		// Produce, putMsg				
			// when len<MAX
		case buffer[putindex] = <- maybe((len<MAX),putMsg):
			//////////////////////////////////////
			fmt.Println("P",buffer)
			//////////////////////////////////////
			len++
			putindex = (putindex + 1) % MAX
		}
	}
}

func main() {
	rand.Seed(time.Now().UnixNano())
	go adm()
	for i:=0;i<TOP;i++ {
		go produce(i)	
		go consume(i)
	}
	for i:=0;i<TOP*2;i++ {
		<- done
	}
	fmt.Println("DONE")
}
