package main

import (
	"fmt"
	"math/rand"
	"time"
	"sync"
)

const  TOP   =   4
const LOOP   =   4
const MSLEEP = 100

type Msg struct {
	data int
	id   int
	busy bool
}

var msg        Msg
var mutex      sync.Mutex
var waitprods  *sync.Cond
var waitcons   *sync.Cond

var done  = make(chan int)

func produce(id int) {
	for i := 0; i < LOOP; i++ {
		// produce
		fmt.Println("Producing ... :",id)
		v := rand.Intn(100)+1
		// wait 
		w := rand.Intn(5)
		time.Sleep((time.Duration(w)+MSLEEP) * time.Millisecond)
		// Poke msg
		mutex.Lock()
		// c = Cond (conditional variable) waitprods)
		// Because c.L is not locked while Wait is waiting, the caller typically
		// cannot assume that the condition is true when Wait returns.
		// Instead, the caller should Wait in a loop:
		//if msg.busy { waitprods.Wait() }
		for msg.busy {
			fmt.Println("Producer:", id, "waiting...")
			// Wait atomically unlocks c.L and suspends execution of the
			// calling goroutine. After later resuming execution, Wait locks c.L
			// before returning. Unlike in other systems, Wait cannot return
			// unless awoken by Broadcast or Signal.
			waitprods.Wait() 
		}
		msg.data = v
		msg.id   = id
		msg.busy = true
		waitcons.Signal()
		mutex.Unlock()
		fmt.Println("Producer:",id)
	}
	done <- id
	fmt.Println("Producer:",id," has finished")
}

func consume(id int) {
	var v Msg
	for i := 0; i < LOOP; i++ {
		// Got msg
		mutex.Lock()
		//if !msg.busy {
		for !msg.busy {
			fmt.Println("Consumer:", id, "waiting...")
			waitcons.Wait()
		} 
		v.data = msg.data;msg.data = -1
		v.id   = msg.id; msg.id    = -1
		msg.busy = false
		waitprods.Signal()
		mutex.Unlock()
		fmt.Println("Consumer:", id, "got from:",v.id, "msg:",v.data)
		// consume
		time.Sleep(MSLEEP * time.Millisecond * time.Duration(v.data))
	}
	done <- id
	fmt.Println("Consumer:",id," has finished")
}

func main() {
	waitprods = sync.NewCond(&mutex)
	waitcons  = sync.NewCond(&mutex)
	msg.data = -1; msg.id = -1; msg.busy = false
	rand.Seed(time.Now().UnixNano())
	for i:=0;i<TOP;i++ {
		go produce(i)	
		go consume(i)
	}
	for i:=0;i<TOP*2;i++ {
		<- done
	}
	fmt.Println("DONE")
}
