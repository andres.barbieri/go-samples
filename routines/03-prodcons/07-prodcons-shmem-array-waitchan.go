package main

import (
	"fmt"
	"math/rand"
	"time"
	"sync"
)

const  TOP   =   4
const LOOP   =  10
const MSLEEP = 100
const MAX    =  10

type Msg struct {
	data int
	id   int
}

var msg        Msg
var mutex      sync.Mutex
var waitprods  *sync.Cond
var waitcons   *sync.Cond

var done  = make(chan int)

// Shared Memory Buffer //
var buffer [MAX]Msg
var len       int = 0
var putindex  int = 0
var getindex  int = 0
//////////////////////////

func produce(id int) {
	for i := 0; i < LOOP; i++ {
		// produce
		fmt.Println("Producing ... :",id)
		v := rand.Intn(100)+1
		// wait 
		w := rand.Intn(5)
		time.Sleep((time.Duration(w)+MSLEEP) * time.Millisecond)
		// Poke msg
		mutex.Lock()
		// c = Cond (conditional variable) waitprods)
		// Because c.L is not locked while Wait is waiting, the caller typically
		// cannot assume that the condition is true when Wait returns.
		// Instead, the caller should Wait in a loop:
		for (len == MAX) {
			fmt.Println("Producer:", id, "waiting...")
			// Wait atomically unlocks c.L and suspends execution of the
			// calling goroutine. After later resuming execution, Wait locks c.L
			// before returning. Unlike in other systems, Wait cannot return
			// unless awoken by Broadcast or Signal.
			waitprods.Wait() 
		}
		//////////////////////
		buffer[putindex].id   = id
		buffer[putindex].data =	v 		
		//////////////////////////////////////
		fmt.Println("P",buffer)
		//////////////////////////////////////
		len++
		putindex = (putindex + 1) % MAX
		//////////////////////
		waitcons.Signal()
		mutex.Unlock()
		fmt.Println("Producer:",id)
	}
	done <- id
	fmt.Println("Producer:",id," has finished")
}

func consume(id int) {
	var msg Msg
	for i := 0; i < LOOP; i++ {
		// Got msg
		mutex.Lock()
		// Consume, getMsg
		for (len == 0) {
			fmt.Println("Consumer:", id, "waiting...")
			waitcons.Wait()
		}
		msg.id   = buffer[getindex].id
		msg.data = buffer[getindex].data
		////////////////////////////////
		// Mark as empty (only for visualization purpose)
		buffer[getindex] = Msg{0,-1}
		fmt.Println("G",buffer)
		//////////////////////////////////////
		len--
		getindex = (getindex + 1) % MAX
		///////////////////////////////
		waitprods.Signal()
		mutex.Unlock()
		fmt.Println("Consumer:", id, "got from:",msg.id, "msg:",msg.data)
		// consume
		time.Sleep(MSLEEP * time.Millisecond * time.Duration(msg.data))
	}
	done <- id
	fmt.Println("Consumer:",id," has finished")
}

func main() {
	waitprods = sync.NewCond(&mutex)
	waitcons  = sync.NewCond(&mutex)
	rand.Seed(time.Now().UnixNano())
	for i:=0;i<MAX;i++ {
		buffer[i].id   = -1
		buffer[i].data =  0
	}
	for i:=0;i<TOP;i++ {
		go produce(i)	
		go consume(i)
	}
	for i:=0;i<TOP*2;i++ {
		<- done
	}
	fmt.Println("DONE")
}
