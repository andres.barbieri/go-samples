// Semaphore with buffered channels

package main

// Based on: https://github.com/syafdia/go-exercise/blob/master/src/concurrency/semaphore/semaphore.go

import (
	"fmt"
	"math/rand"
	"time"
	"sync"
)


const LOOP    =    4
const MSLEEP  =  500

var wait sync.WaitGroup


type Semaphore interface {
	P() // Acquire
	V() // Release
}

type semaphore struct {
	semC chan struct{}
}

func New(maxConcurrency int) Semaphore {
    return &semaphore{
        semC: make(chan struct{}, maxConcurrency),
    }
}
func (s *semaphore) P() {
    s.semC <- struct{}{}
}
func (s *semaphore) V() {
    <-s.semC
}

func task(id int, sem Semaphore) {
	for i := 0; i < LOOP; i++ {
		w := rand.Intn(2500)
		time.Sleep((time.Duration(w)+MSLEEP) * time.Millisecond)	
		sem.P()
		fmt.Println("Task ",id," accessing")
		time.Sleep(MSLEEP * time.Millisecond)	
		sem.V()
	}
	wait.Done()
}

func main() {
	rand.Seed(time.Now().UnixNano())
	defer fmt.Println("DONE")

	sem := New(1)
	go task(0,sem)
	go task(1,sem)
	go task(2,sem)
	
	wait.Add(3)	
	wait.Wait()
}
