package main

// Based on: https://groups.google.com/g/golang-nuts/c/ChPxr_h8kUM

import (
	"fmt"
	"math/rand"
	"time"
	"sync"
)

const MAX     =    4
const LOOP    =    4
const MSLEEP  =  500
const MAXSEMS =    1

var wait sync.WaitGroup

var v[MAX] chan bool
var p[MAX] chan bool

func maybe(b bool, c chan bool) chan bool {
	if !b {
		return nil
	}
	return c
}
func semaphoreAdmin(id int, initVal int) {
	var val int = initVal
	for {
		select {
		case <- maybe((val>0),p[id]):
			val--
		case <-v[id]:
			val++
		}
	}
}

func task(id int, sem int) {
	for i := 0; i < LOOP; i++ {
		w := rand.Intn(2500)
		time.Sleep((time.Duration(w)+MSLEEP) * time.Millisecond)	
		p[sem] <- true
		fmt.Println("Task ",id," accessing")
		time.Sleep(MSLEEP * time.Millisecond)	
		v[sem] <- true	
	}
	wait.Done()
}

func main() {
	rand.Seed(time.Now().UnixNano())
	defer fmt.Println("DONE")
	for i:=0;i<MAXSEMS;i++ {
		v[i] = make(chan bool)
		p[i] = make(chan bool)
		go semaphoreAdmin(i, 1)
	}
	
	go task(0,0)
	go task(1,0)
	go task(2,0)
	
	wait.Add(3)	
	wait.Wait()
}
