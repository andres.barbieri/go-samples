// Semaphore with condition variables

package main

// Similar to: https://gist.github.com/israelchen/e53a535d528154752f5f

import (
	"fmt"
	"math/rand"
	"time"
	"sync"
)

const LOOP    =    4
const MSLEEP  =  500

var wait sync.WaitGroup

type Semaphore interface {
	P()
	V()
}

type semaphore struct {
	 val    int
	 cond  *sync.Cond
}

func New(maxConcurrency int) Semaphore {
	m := &sync.Mutex{}
	s := semaphore{
		val:      maxConcurrency,
		cond:     sync.NewCond(m),
	}

	return &s
}

func (s *semaphore) P() {
	s.cond.L.Lock()
	defer s.cond.L.Unlock()
	for (s.val==0) {
		s.cond.Wait()
	}
	s.val--
}

func (s *semaphore) V() {
	s.cond.L.Lock()
	defer s.cond.L.Unlock()
	s.val++
	//s.cond.Broadcast()
	s.cond.Signal()
}

func task(id int, sem Semaphore) {
	for i := 0; i < LOOP; i++ {
		w := rand.Intn(2500)
		time.Sleep((time.Duration(w)+MSLEEP) * time.Millisecond)	
		sem.P()
		fmt.Println("Task ",id," accessing")
		time.Sleep(MSLEEP * time.Millisecond)	
		sem.V()
	}
	wait.Done()
}

func main() {
	rand.Seed(time.Now().UnixNano())
	defer fmt.Println("DONE")

	sem := New(1)
	go task(0,sem)
	go task(1,sem)
	go task(2,sem)
	
	wait.Add(3)	
	wait.Wait()
}
