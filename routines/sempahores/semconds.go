// Semaphore with condition variables

package main

// Similar to: https://pkg.go.dev/github.com/hegh/basics/sync/semaphore

import (
	"fmt"
	"math/rand"
	"time"
	"sync"
)

const LOOP    =    4
const MSLEEP  =  500

var wait sync.WaitGroup

type Semaphore interface {
	P()
	V()
}

type semaphore struct {
	 val    int
	 mutex sync.Mutex
	 cond  *sync.Cond
}

func New(maxConcurrency int) Semaphore {
	var s semaphore
	s.val = maxConcurrency
	s.cond = sync.NewCond(&s.mutex)
	return &s
}

func (s *semaphore) P() {
	s.mutex.Lock()
	for (s.val==0) {
		s.cond.Wait()
	}
	s.val--
	s.mutex.Unlock()
}

func (s *semaphore) V() {
	s.mutex.Lock()
	s.val++
	//s.cond.Broadcast()
	s.cond.Signal()
	s.mutex.Unlock()
}

func task(id int, sem Semaphore) {
	for i := 0; i < LOOP; i++ {
		w := rand.Intn(2500)
		time.Sleep((time.Duration(w)+MSLEEP) * time.Millisecond)	
		sem.P()
		fmt.Println("Task ",id," accessing")
		time.Sleep(MSLEEP * time.Millisecond)	
		sem.V()
	}
	wait.Done()
}

func main() {
	rand.Seed(time.Now().UnixNano())
	defer fmt.Println("DONE")

	sem := New(1)
	go task(0,sem)
	go task(1,sem)
	go task(2,sem)
	
	wait.Add(3)	
	wait.Wait()
}
