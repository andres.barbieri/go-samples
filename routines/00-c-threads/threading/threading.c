// gcc threading.c -lpthread
// ps -elf | egrep "<prg>|PID"
// ps -T -p <pid>

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

#define   LOOPS      1000
#define   USLEEP     2000
#define   USEC       1000

void* thread_test(void *arg)
{
 int param = *((int*)arg);

 for(int i=0;i<LOOPS;i++)
 {
    printf("I am the thread number %d\n", param);
    //sleep(1);
    usleep(USLEEP * USEC);
 }
}

int main()
{
 pthread_t t1,t2;
 int       arg1 = 1;
 int       arg2 = 2;
 int       **result;

 // Create one thread
 if (pthread_create(&t1,NULL,thread_test,&arg1) != 0)
 {
  perror("error at forking thread");
  exit(1);        
 } 
 //printf("A new thread number %lu has been created \n",t1);

 // Create another thread
 if (pthread_create(&t2,NULL,thread_test,&arg2) != 0)
 {
  perror("error at forking thread");
  exit(1);        
 } 
 //printf("A new thread number %lu has been created \n",t2);

 for(int i=0;i<LOOPS;i++)
 {
    printf("I am the main\n");
    //sleep(1);
    usleep(USLEEP * USEC);
 }
 return 0;
}

