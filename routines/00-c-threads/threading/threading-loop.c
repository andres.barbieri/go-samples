// gcc threading.c -lpthread
// ps -elf | grep <prg>
// ps -T -p <pid>

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

#define LOOPS        1000
#define TOP            10

#define   USLEEP     2000
#define   USEC       1000

void* thread_test(void *arg)
{
 int param = *((int*)arg);

 for(int i=0;i<LOOPS;i++)
 {
    printf("I am the thread number %d\n", param);
    //sleep(1);
    usleep(USLEEP * USEC);
 }
}

int main()
{
 pthread_t t1;
 int       arg1 = 1;
 int       **result;

 for(int i=0;i<TOP;i++) {
   arg1 = i;
   // Create one thread
   if (pthread_create(&t1,NULL,thread_test,&arg1) != 0)
     {
       perror("error at forking thread");
       exit(1);        
     }
 }

 for(int i=0;i<LOOPS;i++)
 {
    printf("I am the main\n");
    //sleep(1);
    usleep(USLEEP * USEC);
 }
 return 0;
}

