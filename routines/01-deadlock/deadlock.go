package main

import (
    "fmt"
    //"math/rand"
    //"time"
)


const LOOPS = 10

var msg1   = make(chan int)
var msg2   = make(chan int)
var done   = make(chan int)

func task1(id int) {
	for i := 0; i < LOOPS; i++ {
		msg1 <- 1
		<-msg2
	}
	done <- id
}

func task2(id int) {
	for i := 0; i < LOOPS; i++ {
		msg2 <- 2
		<-msg1
	}
	done <- id
}

func main() {
	//rand.Seed(time.Now().UnixNano())
	go task1(1)	
	go task2(1)
	for i:=0;i<2;i++ {
		<- done
	}
	fmt.Println("DONE")
}
