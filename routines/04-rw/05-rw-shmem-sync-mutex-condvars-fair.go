package main

import (
	"fmt"
	"math/rand"
	"time"
	"sync"
)

const TOP      =    10
const MSLEEP   =  2000

var mutex         sync.Mutex
//var mutexReaders  sync.Mutex
//var mutexWriters  sync.Mutex
var waitRead     *sync.Cond
var waitWrite    *sync.Cond

// Writers and Readers counters //
var         W int = 0
var  waitingW int = 0
var         R int = 0
var  waitingR int = 0      
//////////////////////////////////

func writer(id int) {
	fmt.Println("I am the writer:",id)
	for  {
		//////////////////////////////
		mutex.Lock()
		for ((R > 0) || (W == 1)) {
			waitingW++
			waitWrite.Wait()
			waitingW--
		}
		W = 1
		//////////////////////////////
		fmt.Println("W=", W, " Writer ",id," accessing")
		//DBInt = id
                write(id)
		fmt.Println("Writer Id ",id,"DBInt:",DBInt)
		fmt.Println("W=", W, " Writer ",id," ending")
		//////////////////////////////
		W = 0
		if (waitingR>0) {
			waitRead.Signal()
		} else {
			waitWrite.Signal()
		}
		mutex.Unlock()
		//////////////////////////////
	}
}

func reader(id int) {
	fmt.Println("I am the reader:",id)
	for  {
		//////////////////////////////
		mutex.Lock()
		for ((W == 1)||(waitingW>0)) {
			waitingR++
			waitRead.Wait()
			waitingR--
		}
		R++
		waitRead.Signal()
		//////////////////////////////
		fmt.Println("R=", R, " Reader ",id," accessing")
		//val := DBInt
		val := read()
		fmt.Println("Reader Id ",id,"DBInt:",val)
		fmt.Println("R=", R, " Reader ",id," ending")
		//////////////////////////////
		R--
		if (R == 0) {
			waitWrite.Signal()
		}
		mutex.Unlock()
		//////////////////////////////
 	}
}

func main() {
	rand.Seed(time.Now().UnixNano())
	waitRead   = sync.NewCond(&mutex)
	waitWrite  = sync.NewCond(&mutex)
	for i:=0;i<TOP;i++ {
		go writer(i)
		go reader(i)
	}
	for {
		time.Sleep(MSLEEP * time.Millisecond)	
	}
}
