package main

import (
	"math/rand"
	"time"
)

// SHARED MEMORY //
var DBInt int = -1
///////////////////

func write(val int) {
	// Simulate write operation
	w := rand.Intn(500)	
	time.Sleep((time.Duration(w)+500.0) * time.Millisecond)
	// CRITICAL REGION //
	DBInt = val
	////////////////////
}

func read() int {
	var tmp int
	// Simulate read operation
	w := rand.Intn(300)
	time.Sleep((time.Duration(w)+200.0) * time.Millisecond)
	// CRITICAL REGION //	
	tmp = DBInt
	/////////////////////
	return tmp
}

func empty() bool {
	return (DBInt == -1)
}
