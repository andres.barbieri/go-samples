package main

import (
	"fmt"
	"math/rand"
	"time"
//	"sync"
)


const TOP      = 10

var readerStart   = make(chan int)
var readerEnd     = make(chan int)
var writerStart   = make(chan int)
var writerEnd     = make(chan int)

func writer(id int) {
	fmt.Println("I am the writer:",id)
	for  {
		writerStart <- id
		//DBInt = id
                write(id)
		fmt.Println("Writer Id ",id,"DBInt:",DBInt)
		writerEnd <- id
	}
}

func reader(id int) {
	fmt.Println("I am the reader:",id)
	for  {
		readerStart <- id
                //val := DBInt
		val := read()
		fmt.Println("Reader Id ",id,"DBInt:",val)
		readerEnd <- id
	}
}

func main() {
	rand.Seed(time.Now().UnixNano())

	for i:=0;i<TOP;i++ {
		go writer(i)
		go reader(i)
	}
	var id int
	for {
		// Only one Reader or one Writer at a time
		select {
		case id = <- readerStart:
			fmt.Println("Reader ",id," accessing")
			<- readerEnd
		case id = <- writerStart:
			fmt.Println("Writer ",id," accessing")
			<- writerEnd
		}	
	}
}
