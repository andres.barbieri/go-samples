package main

import (
	"fmt"
	"math/rand"
	"time"
//	"sync"
)


const TOP      =  10
const MAXRA    =   5

var readerStart   = make(chan int)
var readerEnd     = make(chan int)
var writerStart   = make(chan int)
var writerEnd     = make(chan int)

// Writers and Readers counters //
var W int = 0
var R int = 0
//////////////////////////////////

func writer(id int) {
	fmt.Println("I am the writer:",id)
	for  {
		writerStart <- id
		//DBInt = id
                write(id)
		fmt.Println("Writer Id ",id,"DBInt:",DBInt)
		writerEnd <- id
	}
}

func reader(id int) {
	fmt.Println("I am the reader:",id)
	for  {
		readerStart <- id
                //val := DBInt
		val := read()
		fmt.Println("Reader Id ",id,"DBInt:",val)
		readerEnd <- id
	}
}

func maybe(b bool, c chan int) chan int {
	if !b {
		return nil
	}
	return c
}

func main() {
	var RA int = 0 // ReaderAccessing
	rand.Seed(time.Now().UnixNano())
	for i:=0;i<TOP;i++ {
		go writer(i)
		go reader(i)
	}
	var id int
	for {
		// Only one Reader or one Writer at a time
		select {
			//when W==0 && DBInt !=-1
		case id = <- maybe(((W==0)&&(!empty())&&(RA<MAXRA)),readerStart):
			R++
			RA++
			fmt.Println("R=", R, "RA=", RA," Reader ",id," accessing")
		case <- readerEnd:
			R--
			fmt.Println("R=", R, " Reader ",id," ending")
			//when W==0 && R==0
		case id = <- maybe(((W==0)&&(R==0)),writerStart):
			W++
			RA=0
			fmt.Println("W=", W, " Writer ",id," accessing")
		case <- writerEnd:
			W--
			fmt.Println("W=", W, " Writer ",id," ending")
		}	
	}
}
