package main

import (
	"fmt"
	"math/rand"
	"time"
//	"sync"
)


const TOP     = 10
const SSLEEP  = 30

func writer(id int) {
	fmt.Println("I am the writer:",id)
	for  {
		write(id)
		fmt.Println("Writer Id ",id,"DBInt:",DBInt)		
	}
}

func reader(id int) {
	fmt.Println("I am the reader:",id)
	for  {
		val := read()
		fmt.Println("Reader Id ",id,"DBInt:",val)		
	}
}

func main() {
	rand.Seed(time.Now().UnixNano())
	for i:=0;i<TOP;i++ {
		go writer(i)
		go reader(i)
	}
	// Wait 
	time.Sleep(SSLEEP * time.Second)
}
