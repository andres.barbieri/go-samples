go run 03-rw-chan-sync-guards-nostarvation.go read-write.go > 3.txt
grep Writer 3.txt | grep accessing  | wc -l
grep Reader 3.txt | grep accessing  | wc -l

go run 02-rw-chan-sync-guards.go read-write.go > 2.txt
grep Reader 2.txt | grep accessing  | wc -l
grep Writer 2.txt | grep accessing  | wc -l
