* read-write.go

 Operaciones de lectura/escritura encapsuladas, para compilar, correr, agregar este archivo. Por ejemplo:go run 00-rw-shmem-nosync-waitsleep.go read-write.go  

* 00-rw-shmem-nosync-waitsleep.go

Lectores/escritores sin sincronización, no hay control de acceso a los datos compartidos. No funciona como solución.

* 01-rw-chan-sync.go

Lectores/escritores con sincronización mediante main() como adm con canales (Unbuffered Channels). Limita la concurrencia. No permite más de un lector por vez.

* 02-rw-chan-sync-guards.go

Lectores/escritores con sincronización mediante main() como adm con canales. Controla con guarda condiciones de acceso. Los escritores podrían sufrir starvation (inanición).

* 03-rw-chan-sync-guards-nostarvation.go

Lectores/escritores con sincronización mediante main() como adm con canales (Unbuffered Channels) . Controla con guarda condiciones de acceso. Trata de evitar que escritores sufran starvation permitiendo un máximo de lectores MAXRA. Pero podría limitar lectores acceder cuando no hay escritores interesados.

* 04-rw-shmem-sync-mutex-no-init-writers-starvation.go

Lectores/escritores con sincronización mediante Mutex. El main() no cumple rol como adm. No se controla las lecturas sobre datos no inicializados. Los escritores pueden sufir starvation.

* 04-rw-shmem-sync-mutex-yes-init-writers-starvation.go

Lectores/escritores con sincronización mediante Mutex y waitGroup. El main() no cumple rol como adm. Se controla las lecturas sobre datos no inicializados. Los escritores pueden sufrir starvation.

* 04-rw-shmem-sync-mutex-readers-starvation.go

Lectores/escritores con sincronización mediante Mutex. El main() no cumple rol como adm. En este caso los lectores pueden sufir starvation.

* 04-rw-shmem-sync-rwmutex-fair.go

Lectores/escritores con sincronización mediante RWMutex. El main() no cumple rol como adm. En este caso es fair/justa porque RW mutex bloquea lecturas si hay escrituras esperando.

* 05-rw-shmem-sync-mutex-condvars-fair-error.go

Lectores/escritores con sincronización mediante Mutex y variables Condition. El main() no cumple rol como adm.
Se intenta hacer una solución sin startvation, fair, pero al faltar el Unlock se genera deadlock. No se detecta por el runtime este tipo de deadlock.

* 05-rw-shmem-sync-mutex-condvars-fair.go

Lectores/escritores con sincronización mediante Mutex y variables Condition. El main() no cumple rol como adm.
Se intenta hacer una solución sin startvation, fair.

