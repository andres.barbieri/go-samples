package main

import (
	"fmt"
	"math/rand"
	"time"
	"sync"
)

const TOP      =    10
const MSLEEP   =  2000

var mutex         sync.Mutex
var mutexReaders  sync.Mutex

// Writers and Readers counters //
const W int = 1
var   R int = 0
//////////////////////////////////

func writer(id int) {
	fmt.Println("I am the writer:",id)
	for  {
		//writerStart <- id
		mutex.Lock()
		fmt.Println("W=", W, " Writer ",id," accessing")
		//DBInt = id
                write(id)
		fmt.Println("Writer Id ",id,"DBInt:",DBInt)
		fmt.Println("W=", W, " Writer ",id," ending")
		//writerEnd <- id
		mutex.Unlock()
	}
}

func reader(id int) {
	fmt.Println("I am the reader:",id)
	for  {
		mutexReaders.Lock() 
		R++
		if (R == 1) { mutex.Lock() }
		mutexReaders.Unlock() 
		fmt.Println("R=", R, " Reader ",id," accessing")
		//val := DBInt
		val := read()
		fmt.Println("Reader Id ",id,"DBInt:",val)
		mutexReaders.Lock()
		R--
		if (R == 0) { mutex.Unlock() }
		mutexReaders.Unlock()
		fmt.Println("R=", R, " Reader ",id," ending")
	}
}

func main() {
	rand.Seed(time.Now().UnixNano())
	for i:=0;i<TOP;i++ {
		go writer(i)
		go reader(i)
	}
	for {
		time.Sleep(MSLEEP * time.Millisecond)	
	}
}
