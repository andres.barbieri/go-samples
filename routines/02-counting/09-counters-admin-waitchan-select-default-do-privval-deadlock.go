package main

import (
	"fmt"
	//      "time"
	//	"math/rand"
	//      "sync"
)

const LOOPS =    2000
const TOP   =      20

// SHARED MEMORY //
//var   VAL   =  0
///////////////////

//var adm_val    = 0

var done     = make(chan int)
var count    = make(chan int)
var result   = make(chan int)
var adm_end  = make(chan int)

func doSomething() {	
}

func routine_inc(id int) {
	fmt.Println("I am the routine inc",id)
	for i := 0; i < LOOPS; i++ {
		doSomething()
		count <- 1
	}
	fmt.Println("Finish inc",id)
	done <- id
	
}

func routine_dec(id int) {
	fmt.Println("I am the routine dec",id)
	for i := 0; i < LOOPS; i++ {
		doSomething() 
		count <- -1
	}
	fmt.Println("Finish dec",id)
	done <- id
}

func routine_adm() {
	var   VAL       = 0
	var adm_val     = 0
	for {
		select {
		case val := <- count:
			VAL=VAL+val
		case result <- VAL:
		default:
			doSomething() 
			adm_val++
		}
		// No selective blocking send generate deadlock
		adm_end <- adm_val
	}
}

func main() {
	go routine_adm()
	for i := 0; i < TOP; i++ {
		go routine_inc(i)
		go routine_dec(i)		
	}
	fmt.Println("I am the main")	
	for i :=0;i<TOP*2 ;i++ { <- done }
	val := <- result
        adm_val := <- adm_end  	
	fmt.Println("Bye")
	fmt.Println(val, adm_val)
}
