package main

import (
	"fmt"
	//      "time"
	//	"math/rand"
	"sync"
)

const LOOPS =    2000
const TOP   =      20

// SHARED MEMORY //
var   VAL   =  0
///////////////////

var done  sync.WaitGroup
var mutex sync.Mutex

func doSomething() {	
}

func routine_inc(id int) {
	fmt.Println("I am the routine inc",id)
	for i := 0; i < LOOPS; i++ {
		doSomething()		
		mutex.Lock()
		// CRITICAL REGION //
		VAL = VAL + 1
		/////////////////////
		mutex.Unlock()
	}
	fmt.Println("Finish inc",id)
	done.Done()
	
}
func routine_dec(id int) {
	fmt.Println("I am the routine dec",id)
	for i := 0; i < LOOPS; i++ {
		doSomething()		
		mutex.Lock()
		// CRITICAL REGION //
		VAL = VAL - 1
		/////////////////////
		mutex.Unlock()		
	}
	fmt.Println("Finish dec",id)
	done.Done()
}


func main() {
	done.Add(TOP*2)
	for i := 0; i < TOP; i++ {
		go routine_inc(i)
		go routine_dec(i)		
	}
	fmt.Println("I am the main")	
	done.Wait()
	fmt.Println("Bye")
	fmt.Println(VAL)
}
