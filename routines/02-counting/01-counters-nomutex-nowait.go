package main

import (
	"fmt"
//	"time"
//	"math/rand"
)

const LOOPS =    2000
const TOP   =      20

// SHARED MEMORY //
var   VAL   =  0
///////////////////


func doSomething() {	
}

func routine_inc(id int) {
	fmt.Println("I am the routine inc",id)
	for i := 0; i < LOOPS; i++ {
		doSomething()
		// CRITICAL REGION //
		VAL = VAL + 1
		/////////////////////
	}
	fmt.Println("Finish inc",id)	
}
func routine_dec(id int) {
	fmt.Println("I am the routine dec",id)
	for i := 0; i < LOOPS; i++ {
		doSomething()
		// CRITICAL REGION //
		VAL = VAL - 1
		/////////////////////
	}
	fmt.Println("Finish dec",id)
}

func main() {
	for i := 0; i < TOP; i++ {
		go routine_inc(i)
		go routine_dec(i)		
	}
	fmt.Println("I am the main")	
	fmt.Println("Bye")
	fmt.Println(VAL)
}
