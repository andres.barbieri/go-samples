package main

import (
	"fmt"
	//      "time"
	//	"math/rand"
	//      "sync"
)

const LOOPS =    2000
const TOP   =      20

// SHARED MEMORY //
var   VAL   =  0
///////////////////

var done  = make(chan int)
var count = make(chan int)
var ready = make(chan int)

func doSomething() {	
}

func routine_inc(id int) {
	fmt.Println("I am the routine inc",id)
	for i := 0; i < LOOPS; i++ {
		doSomething() 
		count <- 1
		<- ready
	}
	fmt.Println("Finish inc",id)
	done <- id
	
}

func routine_dec(id int) {
	fmt.Println("I am the routine dec",id)
	for i := 0; i < LOOPS; i++ {
		doSomething() 
		count <- -1
		<- ready
	}
	fmt.Println("Finish dec",id)
	done <- id
}

func routine_adm() {
	for {
		ready <- 1
		val := <- count
		VAL=VAL+val
	}	
}

func main() {
	go routine_adm()
	for i := 0; i < TOP; i++ {
		go routine_inc(i)
		go routine_dec(i)		
	}
	fmt.Println("I am the main")	
	for i :=0;i<TOP*2 ;i++ { <- done }
	fmt.Println("Bye")
	fmt.Println(VAL)
}
