/*
 *
 * Declaraciones en Rust
 *
 */

fn main()
{
    let i1 = 10;
    //i1 = i1 + 2; cannot assign twice to immutable variable    
    let i2: i32 = 10; 
    //let i3; consider giving `i3` an explicit typ
    // let i3: i16; used binding `i3` isn't initialized
    let i3: i16 = 0;
    let f1 = 23.345;
    let f2: f32 = 23.345;
    //f1 = f1 + f2; cannot assign twice to immutable variable

    //used binding `i3` isn't initialized
    println!(" i1={i1} i2={i2} i3={i3} f1={f1} f2={f2} ");
}

/*** EOF ***/
