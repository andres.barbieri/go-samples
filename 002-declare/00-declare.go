/*
 *
 * Ejemplo de declaraciones en Go
 *
 */

package main

import "fmt"
import "reflect"

func main() {
	var i1 int = 10
	var i2 int32 = 10
	i2b := 20 // Auto type (int)
        //var i2t; syntax error: unexpected semicolon, expected type
	var i3 int16;
	var i2t = 30; // Auto type (int)
        f1            := 23.345;   // Auto type (float64)
	f1b           := 23.0;     // Auto type (float64)
	var f2 float32 = 23.345;

	//f1 = f1 + f2

	fmt.Printf(" i1={%d} i2={%d} i2b={%d} i2t={%d} i3={%d} f1={%f} f1b={%f} f2={%f}\n",i1, i2, i3,
		i2b, i2t, f1, f1b, f2)
	fmt.Println("i2b::",reflect.TypeOf(i2b),"i2t::",reflect.TypeOf(i2t)," f1::",reflect.TypeOf(f1),
		"f1b::",reflect.TypeOf(f1b))
}

/*** EOF ***/
