/*
 *
 * Declaraciones en Go
 *
 */

package main

import "fmt"

func main() {
	var (
		i1       = 10
		i3 int16 = 0
		f1       = 23.345
	)
	const (
		i2 int32   = 10
		i2b        = 10
		I2 int32   = 10 
		F2 float32 = 23.345
	)
	i1 = i1 + 2
	f1 = f1 + float64(F2)
	fmt.Println(" i1={",i1,"} i2={",I2,"} i3={",i3,"} f1={",f1,"} F2={",F2,"} ");
}

/*** EOF ***/
