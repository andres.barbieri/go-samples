/*
 *
 * Declaraciones en Go
 *
 */

package main

import "fmt"

func main() {
	var (i1 = 10; i2 = 10; i3 = 10.1454)
	
	var (
		i1b = 10
		i2b = 10
		i3b = 10.24
	)
	
	//var i01, i02, i03 int32 = 10 //assignment mismatch: 3 variables but 1 value
	
	var i01, i02, i03 int32 = 10 ,10, 10
	
	i11,i22,i33 := 10,10,10

	//i11 = i12 = i33 = 20 // syntax error: unexpected = at end of statement
	i11 = 20
	i22 = 20
	i33 = 20
	
	fmt.Println(i1,i2,i3,i1b,i2b,i3b,i01,i02,i03,i11,i22,i33);
}	

/*** EOF ***/
