/*
 *
 * Declaraciones en Go
 *
 */

package main

//import "fmt"

var global1 int = 10
// package declarations must start with keyword var, func, etc.
// := globally prohibited
// global2      := 10 // syntax error: non-declaration statement outside function body
var global2     = 10

func main() {
	var local1       = 10
	var local2 int32 = 10
        local3          := 10 // locally allowed
	_ = local1
	_ = local2
	_ = local3
}

/*** EOF ***/
