/*
 *
 * Declaraciones en "C"
 *
 */

#include <stdio.h>

int main()
{
  int i1, i2, i3;
  //int* p1,p2,p3; // Only first var is type pointer, p2, p3 are int == int *p1,p2,p3
  int *p1, *p2, *p3;
  
  printf(" i1=%d i2=%d i3=%d p1=%p p2=%p p3=%p\n",i1,i2,i3,p1,p2,p3);
  i1 = i2 = i3 = 30;  
  p1 = p2 = p3 = NULL;
  printf(" i1=%d i2=%d i3=%d p1=%p p2=%p p3=%p\n",i1,i2,i3,p1,p2,p3);
  return 0;
}

/*** EOF ***/
