/*
 *
 * Declaraciones en Go
 *
 */

package main

import "fmt"

func main() {
	i1 := 10;
	i1 = i1 + 2; 
	const i2 int32 = 10;
	// i2 = i2 + 10; // cannot assign to i2 (constant 10 of type int32)
	const i2b = 10;
	const I2 int32 = 10;
	//  := not allowed in const declaration
	//const i3 := 10; //syntax error: unexpected :=, expected =
	var i3 int16 = 0; 
	f1 := 23.345;
	const F2 float32 = 23.345;
	f1 = f1 + float64(F2);
	fmt.Println(" i1={",i1,"} i2={",I2,"} i3={",i3,"} f1={",f1,"} F2={",F2,"} ");
}

/*** EOF ***/
