/*
 *
 * Declaraciones en Rust
 *
 */

fn main()
{
    let mut i1 = 10;
    i1 = i1 + 2; 
    // const i2: i32 = 10;
    //const i2 = 10;       // error: missing type for `const` item
    // const i2: i32 = 10; // warning: constant `i2` should have an upper case name
    const I2: i32 = 10; 
    //let mut i3: i16 = 0; // warning: variable does not need to be mutable
    let i3: i16 = 0; 
    let mut f1 = 23.345;
    const F2: f32 = 23.345;
    f1 = f1 + F2;
    println!(" i1={i1} i2={I2} i3={i3} f1={f1} f2={F2} ");
}

/*** EOF ***/
