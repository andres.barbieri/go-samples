/*
 *
 * Declaraciones en Rust
 *
 */

fn main()
{
    //let mut i1 = let mut i2 = let mut i3 =10;
    let (mut i1, mut i2, mut i3) = (10,10,10);
    
    // let (mut i1, mut i2, mut i3): (i32,i32,i32) = (10,10,10); // OK
    
    //let f1: f32 = let f2 = 34.3455;
    let (mut f1, mut f2) = ( 34.3455, 34.3455);
    println!(" i1={i1} i2={i2} i3={i3} f1={f1} f2={f2} ");
    //i1 = i2 = i3 = 30;  // mismatched types
    i1 = 30;
    i2 = 30;
    i3 = 30;
    //f1 = f2 = 30.0; // mismatched types
    f1 = 30.0;
    f2 = 30.0;
    println!(" i1={i1} i2={i2} i3={i3} f1={f1} f2={f2} ");    
}

/*** EOF ***/
