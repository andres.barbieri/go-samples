/*
 *
 * Declaraciones en "Go"
 *
 */

package main

import "fmt"

func main() {
	var i1,i2,i3 int
	var p1,p2,p3 *int
	fmt.Println("i1={",i1,"} i2={",i2,"} i3={",i3,"} p1={",p1,"} p2={",p2,"} p3={",p3,"} ")
}

/*** EOF ***/
