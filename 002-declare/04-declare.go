/*
 *
 * Declaraciones en Go
 *
 */

package main

import "fmt"

func main() {
	var     i1,i3,f1     = 10,0,23.345
	const   i2,i2b,I2,F2 =  10,10,10,23.345

	i1 = i1 + 2
	f1 = f1 + float64(F2)
	fmt.Println(" i1={",i1,"} i2={",I2,"} i3={",i3,"} f1={",f1,"} F2={",F2,"} ");
}

/*** EOF ***/
