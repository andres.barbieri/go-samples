/*
 *
 * Declaraciones en "C"
 *
 */

#include <stdio.h>

int main()
{
  // In declarations only last var has assignment a value
  //int i1, i2, i3 = 10;
  //float f1, f2 = 34.3455;
  int i1 = 10, i2 = 10, i3 = 10;
  float f1 = 34.3455 , f2 = 34.3455;
  printf(" i1=%d i2=%d i3=%d f1=%f f2=%f\n",i1,i2,i3,f1,f2);
  // In assginment statements all vars got the value
  i1 = i2 = i3 = 30;  
  f1 = f2 = 30;
  printf(" i1=%d i2=%d i3=%d f1=%f f2=%f\n",i1,i2,i3,f1,f2);
  return 0;
}

/*** EOF ***/
