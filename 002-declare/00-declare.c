/*
 *
 * Declaraciones en "C"
 *
 */

#include <stdio.h>

int main()
{
    //const int i1 = 10; assignment of read-only variable ‘i1’
    //int i1 = 10;
    int i1;
    int i2 = 10;
    i1 = i1 + 2;
    const short int i3;
    float f1 = 23.345;
    const float f2 = 23.345;
    f1 = f1 + f2; 
    printf(" i1=%d i2=%d i3=%d f1=%f f2=%f \n",i1,i2,i3,f1,f2);
    return (0);
}

/*** EOF ***/
