/*
 *
 * Hello world Program en "C"
 *
 * NOTE:
 *
 * compile: 
 *          gcc hola.c                        -- generate a.out
 *          gcc -o hola hola.c                -- generate hola
 *          gcc -Wall -std=c99 -o hola hola.c -- generate hola
 *          make                              -- generate hola
 */

#include <stdio.h>

int main() 
{
  printf("Hola\n");
}

/*** EOF ***/
