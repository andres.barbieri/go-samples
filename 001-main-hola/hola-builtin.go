package main

/***
 println is a function built into the language. It is in the Bootstrapping section of the spec of Go.

Current implementations provide several built-in functions useful during bootstrapping. These functions are documented for completeness but are not guaranteed to stay in the language. They do not return a result.

***/

func main() {
    println("Hola")
}
