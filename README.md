# Ejemplos de programas en "Go"

Ejemplo de programas en lenguaje "Go" con propósitos de enseñanza

## Organización

Los ejemplos están organizados en directorios individuales, y en un orden de acuerdo a como se pueden abordar los temas. Esta en version incompleta, no está aún finalizado.

## Modo de uso

Se puede analizar el código fuente incluido en cada directorio siguiendo el README que se acompaña.

```console
$ git clone https://gitlab.com/andres.barbieri/go-samples.git go-samples
$ cd go-samples/<dir>
$ cat README.md
$ ...
```

Se puede clonar usando ssh con también:

```console
$ git clone git@gitlab.com:andres.barbieri/go-samples.git
...
```

