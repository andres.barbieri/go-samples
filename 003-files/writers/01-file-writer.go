// Unbuffered version

package main

import  (
	//"fmt"
	"log"
	//"io"
	//"bufio"
	"os"
)

func usage(s string) {
	log.Println("filename argument needed")
	os.Exit(1)
}

func main() {
	var fileName  string 
	var err       error
	var aFile    *os.File
	
	if (len(os.Args) > 1) {
		fileName = os.Args[1]
	} else {
		usage(os.Args[0])
	}
	//aFile = os.OpenFile(fileName, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0600)
	aFile, err = os.Create(fileName)
	defer aFile.Close()
	
	// if we os.Open returns an error then handle it
	if (err != nil) {
		log.Println("Can't open file for writing",err)
		os.Exit(1)
		//panic(err)
	}
	// writer := bufio.NewWriter(aFile)  // bufio.Writer
	b := make([]byte, 1) // One byte
	for i:=0;i<256;i++ {
		b[0] = byte(i)
		_ , err = aFile.Write(b) // Write one byte
		if (err !=nil) {
			log.Println("Error writing file",err)
			os.Exit(1)
			//panic(err)
		}			
	}
}
