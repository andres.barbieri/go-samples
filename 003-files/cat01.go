package main

import  (
	//"fmt"
	"log"
	//"io"
	//"bufio"
	"os"
)


func cat(infileName string) bool {
	var err                     error
	var rFile                  *os.File
	var result                  int
	var b                       []byte

	if (infileName == "") {
		rFile = os.Stdin
	} else {
		///////////////////////////////////////////////////	
		rFile, err = os.Open(infileName)
		defer rFile.Close()	
		// if we os.Open returns an error then handle it
		if (err != nil) {
			log.Println("No such file or directory ",infileName)
			//os.Exit(1)
			//panic(err)
			return false
		}
	}
	///////////////////////////////////////////////////	
	b = make([]byte, 1) // One byte
	result, err = rFile.Read(b)  // Read one byte
	for ( (result>0) && (err == nil)) {  //io.EOF
		os.Stdout.Write(b) // Write one byte
		result, err = rFile.Read(b)  // Read one byte
	}
	return true
}

func main() {
	var result bool = true
	var res    bool
	if (len(os.Args) == 1) {
		cat("")
		result = true
	} else {
		for i:=1;i<len(os.Args);i++ {
			res = cat(os.Args[i])
			result = result && res
		}
	}
	if (result) {os.Exit(0)} else { os.Exit(1) }
}

