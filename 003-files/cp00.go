package main

import  (
	//"fmt"
	"log"
	//"io"
	//"bufio"
	"os"
)

func usage(s string) {
	log.Println("Copy a file byte by byte")
	log.Println("usage: ",s, " inputfile outputfile")
	log.Println("input and output filename must be provided")
	os.Exit(1)
}

func main() {
	var infileName,outfileName  string 
	var err                     error
	var rFile,wFile            *os.File
	var result                  int 
	
	if (len(os.Args) > 2) {
		infileName = os.Args[1]
		outfileName = os.Args[2]
		
	} else {
		usage(os.Args[0])
	}
	///////////////////////////////////////////////////	
	rFile, err = os.Open(infileName)
	defer rFile.Close()	
	// if we os.Open returns an error then handle it
	if (err != nil) {
		log.Println("Can't open file for reading",err)
		os.Exit(1)
		//panic(err)
	}
	///////////////////////////////////////////////////
	wFile, err = os.Create(outfileName)
	defer wFile.Close()
	// if we os.Open returns an error then handle it
	if (err != nil) {
		log.Println("Can't open file for writing",err)
		os.Exit(1)
		//panic(err)
	}
	///////////////////////////////////////////////////	
	//reader := bufio.NewReader(rFile)  // bufio.Reader
	//writer := bufio.NewReader(wFile)  // bufio.Writer
	b := make([]byte, 1) // One byte
	result, err = rFile.Read(b)  // Read one byte
	for ( (result>0) && (err == nil)) {  //io.EOF
		_ , err     = wFile.Write(b) // Write one byte
		result, err = rFile.Read(b)  // Read one byte
	}
}
