package main

import  (
	//"fmt"
	"log"
	//"io"
	//"bufio"
	"os"
)

func main() {
	var infileName              string 
	var err                     error
	var rFile                  *os.File
	var result                 int


	if (len(os.Args) > 1) {
		infileName = os.Args[1]		
		///////////////////////////////////////////////////	
		rFile, err = os.Open(infileName)
		defer rFile.Close()	
		// if we os.Open returns an error then handle it
		if (err != nil) {
			log.Println("Can't open file for reading",err)
			os.Exit(1)
			//panic(err)
		}
	} else {
		rFile = os.Stdin
	}
	///////////////////////////////////////////////////	
	b := make([]byte, 1) // One byte
	result, err = rFile.Read(b)  // Read one byte
	for ( (result>0) && (err == nil)) {  //io.EOF
		os.Stdout.Write(b) // Write one byte
		result, err = rFile.Read(b)  // Read one byte
	}
}
