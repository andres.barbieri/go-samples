package main

import  (
	"fmt"
	"log"
	//"io/ioutil" //deprecated
	"io"
	"os"
)

func usage(s string) {
	log.Println("filename argument needed")
	os.Exit(1)
}

//const FBSIZE = 1024
const FBSIZE =  256

func main() {
	var fileName  string 
	var err       error
	var aFile    *os.File
	
	if (len(os.Args) > 1) {
		fileName = os.Args[1]
	} else {
		usage(os.Args[0])
	}
	aFile, err = os.Open(fileName)
	defer aFile.Close()
	
	// if we os.Open returns an error then handle it
	if (err != nil) {
		log.Println("Can't open file for reading",err)
		os.Exit(1)
		//panic(err)
	}
	// byteValue, _ := ioutil.ReadAll(aFile) 
	byteValue, _ := io.ReadAll(aFile)
	fmt.Print(string(byteValue))
}
