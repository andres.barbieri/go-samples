package main

import  (
	"fmt"
	"log"
	//"io"
	"bufio"
	"os"
)

func usage(s string) {
	log.Println("filename argument needed")
	os.Exit(1)
}

//const FBSIZE = 1024
const FBSIZE =  256

func main() {
	var fileName  string 
	var err       error
	var aFile    *os.File
	var result    int 
	
	if (len(os.Args) > 1) {
		fileName = os.Args[1]
	} else {
		usage(os.Args[0])
	}
	aFile, err = os.Open(fileName)
	defer aFile.Close()
	
	// if we os.Open returns an error then handle it
	if (err != nil) {
		log.Println("Can't open file for reading",err)
		os.Exit(1)
		//panic(err)
	}
	reader := bufio.NewReader(aFile)  // bufio.Reader
	b := make([]byte, FBSIZE) // One block
	result, err = reader.Read(b) // Read one block
	for (result>0) && (err == nil) {  //io.EOF
		// Show array as string
		fmt.Print(string(b))
		// Or show array byte by byte
		//for i:=0;i<result;i++ {fmt.Print(string(b[i]))}
		// Blank array
		for i,_:=range(b){b[i] = 0} // Or: b = make([]byte, FBSIZE)
		result, err = reader.Read(b) // Read one block
	}
}
