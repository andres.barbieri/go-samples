package main

import  (
	"fmt"
	"log"
	//"io"
	"bufio"
	"os"
)

func usage(s string) {
	log.Println("filename argument needed")
	os.Exit(1)
}

func main() {
	var fileName  string 
	var err       error
	var aFile    *os.File
	var result    int
	
	if (len(os.Args) > 1) {
		fileName = os.Args[1]
	} else {
		usage(os.Args[0])
	}
	aFile, err = os.Open(fileName)
	defer aFile.Close()
	
	// if we os.Open returns an error then handle it
	if (err != nil) {
		log.Println("Can't open file for reading",err)
		os.Exit(1)
		//panic(err)
	}
	reader := bufio.NewReader(aFile)  // bufio.Reader
	b := make([]byte, 1) // One byte
	result, err = reader.Read(b) // Read one byte
	for ( (result>0) && (err == nil)) {  //io.EOF
		fmt.Print(string(b)) // Show byte
		result, err = reader.Read(b) // Read one byte
	}
}
