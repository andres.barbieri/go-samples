package main

import  (
	"fmt"
	"log"
	//"io"
	"bufio"
	"os"
)

func usage(s string) {
	log.Println("filename argument needed")
	os.Exit(1)
}

func main() {
	var fileName  string 
	var err       error
	var txtFile   *os.File
	var result    bool
	
	if (len(os.Args) > 1) {
		fileName = os.Args[1]
	} else {
		usage(os.Args[0])
	}
	txtFile, err = os.Open(fileName)
	defer txtFile.Close()
	
	// if we os.Open returns an error then handle it
	if (err != nil) {
		log.Println("Can't open file for reading",err)
		os.Exit(1)
		//panic(err)
	}
	scanner := bufio.NewScanner(txtFile) // bufio.Scanner
	result = scanner.Scan()
	for ( (result) && (err == nil)) {
		err    = scanner.Err()
		fmt.Println(scanner.Text())
		result = scanner.Scan()
	}
}
