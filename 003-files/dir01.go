package main

import (
	"fmt"
	"os"
	"log"
)

func main() {
	var dirName string
	
	if (len(os.Args)>1) {
		dirName = os.Args[1]
	} else {
		dirName = "."
	}
	files, err := os.ReadDir(dirName)
	if (err != nil) {
		log.Fatal(err)
	}

	for _, file := range files {
		fmt.Println(file.Name())
	}
}
