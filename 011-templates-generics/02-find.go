package main

import "fmt"

func includes[T comparable](a []T, elem T) bool {
	for _,v := range a {
		if (v == elem) {
			return true
		}
	}
	return false
}


func index[T comparable](a []T, elem T) int {
	for i,v := range a {
		if (v == elem) {
			return i
		}
	}
	return -1
}


func occurrences[T comparable](a []T, elem T) int {
	count := 0
	for _,v := range a {
		if (v == elem) {
			count++
		}
	}
	return count
}


type Algo struct {
	x int
	y string
}

func main() {
	a := []int{ 10, 30, 30, 50, 120, 53, 34, 4353, -44, }
	b := []Algo {{23,"ddd"},Algo{34,"jjj"}}
	c := []float64{ 10.3, 30.4, 30.45, 50.454, 120, 53.455, -34.44, 4353.44, -44.333, }
	fmt.Println(includes[int](a,34))
	fmt.Println(includes(a,-30))
	fmt.Println(index[int](a,50))
	fmt.Println(index(a,-30))
	fmt.Println(occurrences[int](a,50))
	fmt.Println(occurrences(a,-30))	
	fmt.Println(occurrences(a,30))

	fmt.Println(occurrences(c,30))

	fmt.Println(includes(b,Algo{23,"ddd"}))
	fmt.Println(includes(b,Algo{24,"ddd"}))		
}
