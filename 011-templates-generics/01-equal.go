package main

import "fmt"

func equals[T comparable](a,b T) bool {	
	return (a == b)
}

/***
func equalsAnyTemp[T any](a,b T) bool {	
	return (a == b) // a == b (incomparable types in type set)
}
***/


func equalsAny(a,b any) bool {
	return (a == b)
}


func isNil(a any) bool {
	return (a == nil)
}

/***
func isNilTemp[T any](a T) bool {
	return (a == nil) // invalid operation: a == nil (mismatched types T and untyped nil)
}
***/




type Algo struct {
	x int
	y string
}

func main() {
	var a int = 20
	var b int = 30
	var c int = 20	
	fmt.Println(a,b,equals(a,b),equals(a,c))
	fmt.Println(a,b,equalsAny(a,b),equalsAny(a,c))
	x := Algo{23,"uno"}
	y := Algo{1000,"otro"}
        z := Algo{1000,"otro"}
	fmt.Println(x,y,equals(x,y),equals(y,z))
	fmt.Println(x,y,equalsAny(x,y),equalsAny(y,z))	
}
