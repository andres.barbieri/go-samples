package main

import "fmt"

func swp[T any](a,b *T) {
	aux := *a
	*a = *b
	*b = aux
}

func swp1[T any](a,b *T) {
	var aux T = *a
	*a = *b
	*b = aux
}

func swp2[T any](a,b *T) {
	*a, *b = *b, *a
}

func swp3[T interface {}](a,b *T) {
	*a, *b = *b, *a
}

type Algo struct {
	x int
	y string
}

func main() {
	var a int = 20
	var b int = 30
	fmt.Println(a,b)
	swp(&a,&b)
	fmt.Println(a,b)
	swp1(&a,&b)
	fmt.Println(a,b)	
	swp2(&a,&b)
	fmt.Println(a,b)
	swp3(&a,&b)
	fmt.Println(a,b)	
	swp2[int](&a,&b)
	fmt.Println(a,b)
	x := Algo{23,"uno"}
	y := Algo{1000,"otro"}
	fmt.Println(x,y)		
	swp(&x,&y)
	fmt.Println(x,y)	
}
