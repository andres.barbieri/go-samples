/*
 *  Program using templatelist package
 *  Check go.mod file references templatelist
 *
 */

package main

import "fmt"
import L "templatelist" 


func dec(x int) int {
	return x-1
}

func print(x int) {
	fmt.Print(x," ")
}


func main() {
	// Not needed
	//l := L.New[int]()
	var l L.List[int]
	//l = L.New[int]()
	var inc = func(x int) int { return x+1 }
	
	fmt.Println(L.Len[int](l))	
	L.PushFront[int](&l,1)
	L.PushFront[int](&l,2)
	L.Push[int](&l,3,4,5)
	L.PushBack[int](&l,300)
	L.PushFront[int](&l,300)
	// L.PushFront[float32](&l,300.33) type mismatch		
	fmt.Println(L.IsEmpty[int](l))
	fmt.Println(L.Len[int](l))
	L.Each[int](l, print);fmt.Println()
	L.Iterate[int](l, inc)
	L.Each[int](l, print);fmt.Println()	
	L.Iterate[int](l, dec)
	L.Remove[int](&l)
	L.Remove[int](&l)
	fmt.Println(L.Len[int](l))
//        for aux:=l;!L.IsEmpty[int](aux);aux=L.Next[int](aux) {
	for aux:=l;aux!=nil;aux=L.Next[int](aux) {		
		fmt.Println(L.FrontElement[int](aux))
	}
}

