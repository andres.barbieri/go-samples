/*
 * Integer Single Linked List as ADT, not as OOP
 * Impl2, with test operations and variadic Push

 */
package templatelist

//import "fmt"

type Node[T any] struct {
	elem       T
	next       *Node[T]
}

type List[T any] *Node[T]


func New[T any]() List[T] {
	return List[T](nil)
}

func IsEmpty[T any] (self List[T]) bool {
  return (self == nil)
}

func Len[T any] (self List[T]) int {
  if (IsEmpty(self)) {
      return 0;
    } else {
    return 1 + Len(Next(self));
  }
} 

func FrontElement[T any] (this List[T]) T {
	return this.elem
}

/****
func FrontElementTest[T any] (this List[T]) (T,bool) {
	if (!IsEmpty(this)) {
		return this.elem,true
	} else {
		return Whats,false
	}
}
***/

func Next[T any] (this List[T]) List[T] {
  return this.next
}

func NextTest[T any] (this List[T]) (List[T],bool) {
	if (!IsEmpty(this)) {
		return this.next,true
	} else {
		return nil,false
	}	
}

func Push[T any]( this *List[T], elems ...T) {
	for _, e := range elems {
		PushFront(this,e)
	}
}

func PushFront[T any] (this *List[T], elem T) {
	aux := new(Node[T])
	aux.elem = elem
	aux.next = *this
	*this = aux
}

func PushBack[T any] (this *List[T], elem T) {
	if (IsEmpty[T](*this)) {
		aux := new(Node[T])
		aux.elem = elem
		aux.next = nil
		*this = aux
	} else {
		PushBack( (*List[T])(&((*this).next)) ,elem )
	}
}


func Remove[T any] (this *List[T])  T {
	elem := (*this).elem
	*this = ((*this).next)
	return elem;
}

func RemoveTest[T any] (this *List[T], null T) (T,bool) {
	if (IsEmpty(*this)) {
		return null,false
	}	
	elem := (*this).elem
	*this = ((*this).next)
	return elem,true
}


func Iterate[T any] (this List[T], fp func(T)T) {
  aux := this;
  for (!IsEmpty(aux)) {
      aux.elem = (fp(aux.elem));
      aux = Next(aux);
  }
}

func Each[T any] (this List[T], fp func(T)) {
  aux := this;
  for (!IsEmpty(aux)) {
      fp(aux.elem)
      aux = Next(aux)
  }
}
