// go mod init maxmin
// go mod tidy
// cat go.mod
// ...

package main

import "fmt"
import "golang.org/x/exp/constraints"

func max[T constraints.Ordered](a,b T) T {
	if (a>b) { 
//	if (a.GreatherThan(b)) {
		return a
	} else {
		return b
	}
}

func min[T constraints.Ordered](a,b T) T {	
	if (a<b) {
//	if (a.LessThan(b)) {
		return a
        }  else {
		return b
	}
}

func main() {
	var a int = 20
	var b int = 30
	aa := "zAAAA"
	bb := "bbbbbbb"
	fmt.Println(a,b,min(a,b),max(b,a))
	fmt.Println(aa,bb,min(aa,bb),max(aa,bb))	
}
