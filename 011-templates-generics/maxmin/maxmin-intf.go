package main

import "fmt"

type MyOrdered interface {
	LessThan(i MyOrdered) bool
	GreatherThan(i MyOrdered) bool
}

type IntValue int

func(x IntValue) LessThan(y IntValue) bool {
return int(x)<int(y)
}

func(x IntValue) GreatherThan(y IntValue) bool {
return int(x)>int(y)
}

func max[T MyOrdered](a,b T) T {
//	if (a>b) { 
	if (a.GreatherThan(b)) {
		return a
	} else {
		return b
	}
}

func min[T MyOrdered](a,b T) T {	
//	if (a<b) {
	if (a.LessThan(b)) {
		return a
        }  else {
		return b
	}
}

func main() {
	var a int = 20
	var b int = 30
        fmt.Println(a,b,min(IntValue(a),IntValue(b)))
}
