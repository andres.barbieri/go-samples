package main

import "fmt"

func SumIntsOrFloatsA[V int64 | float64 | int32 | float32 | int](a []V) V {
	var s V
	for _, v := range a {
		s += v
	}
	return s
}

func SumIntsOrFloatsM[K comparable, V int64 | float64](m map[K]V) V {
	var s V
	for _, v := range m {
		s += v
	}
	return s
}


func NumericSlice2Map[K int, V int64 | float64](s []V) map[K]V {
	m := make(map[K]V)
	for i , v := range s {
		m[K(i)] = v 
	}
	return m	
}
	
func main() {
	a := []int{0,1,2,3,4,5,6,7,8}
	f := []float32{0,1,2,3,4,5,6,7,8,9.9}
	b := []int64{0,1,2,3,4,5,6,7,8}
        m := map[string]float64{"a":1, "b":2, "c":3}
	
	fmt.Println(SumIntsOrFloatsA(a))
	fmt.Println(SumIntsOrFloatsA(f))
	fmt.Println(SumIntsOrFloatsM(m))
	// int does not satisfy int64 | float64 (int missing in int64 | float64)
	// fmt.Println(SumIntsOrFloatsM(NumericSlice2Map(a)))
	fmt.Println(SumIntsOrFloatsM(NumericSlice2Map(b)))
}
