package main

import "fmt"


func length[K comparable, V any](m map[K]V) int {
	return len(m)
}

//type []string of mapElems does not match map[K]V (cannot infer K and V)
//func len[K comparable, V any](m map[K]V) int {
//	return len(m)
//}

func elem[K comparable, V any](m map[K]V, k K) (V,bool) {
	v, ok := m[k]
	return v,ok
}

func mapElems[K comparable, V any](m map[K]V) []V {
	s := make([]V,0,len(m))
	for  _, value := range m {
		s = append(s, value)
	}
	return s
}

func key[K comparable, V comparable]( m map[K]V, e V) (K,bool) {
	var j K
      	for k,v := range m {
		if (e == V(v)) {return k,true} 
	}
	return j,false
}

func mapKeys[K comparable, V any](m map[K]V) []K {
	s := make([]K,0,len(m))
	for  key , _ := range m {
		s = append(s, key)
	}
	return s
}

func slice2Map[K int, V any](s []V) map[K]V {
	m := make(map[K]V)
	for i , v := range s {
		m[K(i)] = v 
	}
	return m	
}

type Map[K comparable, V any] map[K]V

func mapKeysFromMapType[K comparable, V any](m Map[K,V]) []K {
	s := make([]K,0,len(m))
	for  key , _ := range m {
		s = append(s, key)
	}
	return s
}

func main() {
	myMap    := make(map[int]string, 0)
        mySlice  :=  []string{"UNO", "DOS", "TRES"}
		
	myMap[10] = "diez"
	myMap[2]  = "dos"
	myMap[4]  = "cuatro"
	fmt.Println(myMap, len(myMap), length(myMap))	
	mapElems := mapElems(myMap)	
	mapKeys  := mapKeys(myMap)
	fmt.Println(mapElems, len(mapElems), mapKeys, len(mapKeys))
	e,b := elem[int,string](myMap,10)
	fmt.Println(e,b)
	e,b = elem(myMap,10)
	fmt.Println(e,b)
	e, b = elem[int,string](myMap,1)
	fmt.Println(e,b)
	i, b := key[int,string](myMap,"diez")
	fmt.Println(i,b)
	i, b = key[int,string](myMap,"uno")
	fmt.Println(i,b)
	fmt.Println(mySlice)
	fmt.Println(slice2Map[int,string](mySlice))

	var anotherMap Map[int,string] = make(map[int]string,0)
	fmt.Println(anotherMap)
	anotherMap[10] = "diez"
	fmt.Println(anotherMap)
	fmt.Println(mapKeysFromMapType(anotherMap))
}
