package main

import ("sort";
	"fmt")

/***
type Interface interface {
	Len() int
	Less(i, j int) bool
	Swap(i, j int)
}
***/

type Algo struct {
	x int
	y string
}

type CasiAlgoSlice []Algo

func (x CasiAlgoSlice) Len() int {
	return len(x)
}
func (x CasiAlgoSlice) Less(i, j int) bool {
	return ((x[i].x) < (x[j].x))
}
func (x CasiAlgoSlice) Swap(i, j int) {
  x[i].x, x[j].x, x[i].y, x[j].y = x[j].x, x[i].x , x[j].y, x[i].y
}


func main() {
	a := []int{4,5,4,3,1,4,55,67,42,-4,55,44,-44,0}
	b := a
	fmt.Println(a,b)
	sort.Ints(a)
	fmt.Println(a,b)
	// f(i) is true, assuming that on the range [0, n), f(i) == true implies f(i+1) == true.
	// Search needs >= and not ==
	x := 44
	i := sort.Search(len(a), func(i int) bool { return (a[i] >= x) })
	fmt.Println(i)
	fmt.Println(sort.SearchInts(a,44))
	//sort.Sort(sort.Reverse(b)) // annot use b (variable of type []int) as sort.Interface value in argument to sort.Reverse: []int does not implement sort.Interface
	// IntSlice cast slice to Interface object (sortable)
	sort.Sort(sort.Reverse(sort.IntSlice(b)))
	fmt.Println(a,b)

	c := []Algo{{4,"4"},{5,"5"},{4,"C"},{3,"D"},{55,"DDD"},{67,"DDD"},{-44,"ddd"},{0,"0"}}

	fmt.Println(c,sort.IsSorted(CasiAlgoSlice(c)))
	fmt.Println(CasiAlgoSlice(c))
	sort.Sort(CasiAlgoSlice(c))
	fmt.Println(c,sort.IsSorted(CasiAlgoSlice(c)))
}
