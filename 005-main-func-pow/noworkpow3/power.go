package power

func ipow(base int32 ,n uint32) int64 {
  var res int64  = 1
	for i:=0;i<int(n);i++ {
		res*= int64(base)
    }
  return res
}

func Ipower(base int32 ,n uint32) int64 {
  return ipow(base, n)
}

