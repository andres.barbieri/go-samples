// go run main.go func.go p1.go

// go build -o main main.go func.go p1.go
// or
// go build -o main main.go func.go p2.go
// or
// go build -o main main.go func.go p3.go
// or
// go build -o main -linkshared main.go func.go p3.go DOES NOT WORK
package main

import "fmt"

func main() {
  fmt.Printf("%d %d %d %d\n",
           ipower(2,2), ipower(2,5), ipower(2,10), ipower(2,15));
}

/*** EOF ***/

