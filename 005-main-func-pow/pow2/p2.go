package main

import "math"

func ipow(base int32 ,n uint32) int64 {
	return int64(math.Pow(float64(base), float64(n)));
}


