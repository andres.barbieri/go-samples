package main

func ipow(base int32 ,n uint32) int64 {
	if        (n == 0) {
		return 1;
	} else if (n == 1) {
		return int64(base);
	} else             {
		return int64(ipow(base,n-1)*int64(base));
	}
}


