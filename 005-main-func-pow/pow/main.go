// go run main.go func.go 
// go build -o main main.go func.go

package main

import "fmt"

func main() {
  fmt.Printf("%d %d %d %d\n",
           ipower(2,2), ipower(2,5), ipower(2,10), ipower(2,15));
}

/*** EOF ***/

