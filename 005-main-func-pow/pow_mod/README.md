# Package como Módulos en Go


Refs: https://go.dev/doc/tutorial/call-module-code

```console
mkdir pow_mod
cd pow_mod
```

To enable dependency tracking for your code, run the "go mod init" command, giving it the name of the module your code will be in.

```console
go mod init pow-mod
cat go.mod
-------------------
module pow_mod

go 1.20
-------------------
```

```console
vi pow_mod.go # package/lib go code
```

```console
cd ..

mkdir main
cd main
```

To enable dependency tracking for your code, run the "go mod init" command, giving it the name of the module your code will be in.

```console
go mod init main
```

The command specifies that "pow_mod" should be replaced with "../pow_mod" for the purpose of locating the dependency. After you run the command, the "go.mod" file in the hello directory should include a replace directive:

```console
go mod edit -replace pow_mod=../pow_mod
```

The command found the local code in the "pow_mod" directory, then added a require directive to specify that "main" requires "pow_mod." You created this dependency when you imported the "pow_mod" package in "main.go".

The number following the module path is a pseudo-version number -- a generated number used in place of a semantic version number (which the module doesn't have yet).

To reference a published module, a "go.mod" file would typically omit the replace directive and use a require directive with a tagged version number at the end.

```console
go mod tidy
```

```console
cat go.mod
-------------------
module main

go 1.20

replace pow_mod => ../pow_mod

require pow_mod v0.0.0-00010101000000-000000000000
-------------------
```

```console
vi main.go # main go code

```console
go run .

go run main.go
```
