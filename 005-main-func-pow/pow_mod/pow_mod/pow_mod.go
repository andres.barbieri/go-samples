package pow_mod

/*
 * Private func
 */

func ipow(base int32 ,n uint32) int64 {
  var res int64  = 1
	for i:=0;i<int(n);i++ {
		res*= int64(base)
    }
  return res
}

/*
 * Public funcs start in Capital letter/Uppercase
 */

// Aritmetic Power function:
// These function return the value of base raised to the power of n.
func Ipower(base int32 ,n uint32) int64 {
  return ipow(base, n)
}

