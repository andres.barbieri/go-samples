// 

package main

import "fmt"
import p "pow_mod"

func main() {
  fmt.Printf("%d %d %d %d\n",
           p.Ipower(2,2), p.Ipower(2,5), p.Ipower(2,10), p.Ipower(2,15));
}

/*** EOF ***/

