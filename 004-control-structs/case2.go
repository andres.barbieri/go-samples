package main

import "fmt"

func main() {

  for   i:=0;i<=10;i++ {
      switch { // empty switch expression
	case i==0:
	  fmt.Print("XS ");
	case i>=2 && i<=3:
	  fmt.Print("S ")
        case i>=4 && i<=6:
	  fmt.Print("M")
	  fmt.Print(" ")
	case i>=7 && i<=10:
	  fmt.Print("L ")
	default:
	  fmt.Print("U ")
      }
  }
  fmt.Println()
}

