package main

//import "fmt"

/*** Wrong syntax
func f(a int) int {
	if (a>0)  return  0
	if (a>10) return  1
	if (a>20) return  2
	if (a>40) return  3
        else return 4
}

func f(a int) int {
	if (a>0)  {return  0}
	if (a>10) {return  1}
	if (a>20) {return  2}
	if (a>40) {return  3}
        else      {return 4}
}

// missing return
func f(a int) int {
	if (a>0) {return 0}
}

****/

func f(a int) int {
	if (a>0)  {return  0}
	if (a>10) {return  1}
	if (a>20) {return  2}
	if (a>40) {return  3
	} else    {return 4}
}

func f_1(a int) int {
	if (a>0)  {return  0}
	if (a>10) {return  1}
	if (a>20) {return  2}
	if (a>40) {return  3}
	return 4
}

func f_2(a int) int {
	if (a>0)  {
		return  0
	} else if (a>10) {
		return  1
	} else if (a>20) {
		return  2
	} else if (a>40) {
		return  3
	} else {return 4}
}

func f_3(a int) int {
	switch { // empty switch expression
	case (a>0):  
		return  0
	case (a>10): 
		return  1
	case (a>20): 
		return  2
	case (a>40): 
		return  3
	default:
		return  4
	}
}

func main() {
}


