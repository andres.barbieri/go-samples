package main

import "fmt"

func main() {
	//for i, j := 0, 1; i < 10; i++, j++ { // syntax error
	for i, j := 0, 1; i < 10; i, j = i+1, j+1 {
		fmt.Println("Hola Go")
	}
}
