/***
 * Test comment
 */

package main

import "fmt"

func main() {
  for i:=0;i<10;i++ { // 0..9 (exclusive)
	  if ( (i % 2 == 0) && (i + 3 > 10) ) {
		  fmt.Println("something");
	  } else {
		  fmt.Println("something else");
	  }
  }
}
