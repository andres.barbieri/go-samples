
/***
// error: expected ‘;’ before ‘if’
int f_0(int a)  {
	if (a>0)  return  0
	if (a>10) return  1
	if (a>20) return  2
	if (a>40) return  3
        else return 4
}

//error: expected ‘;’ before ‘}’ token
int f_00 (int a) {
	if (a>0)  {return  0};
	if (a>10) {return  1};
	if (a>20) {return  2};
	if (a>40) {return  3};
        else      {return  4};
}

//error: ‘else’ without a previous ‘if’
int f_00 (int a) {
  if (a>0)  {return  0;};
  if (a>10) {return  1;};
  if (a>20) {return  2;};
  if (a>40) {return  3;};
  else      {return  4;};
}
***/

int f_0(int a)  {
  if (a>0)  return  0;
  if (a>10) return  1;
  if (a>20) return  2;
  if (a>40) return  3;
  else return 4;
}

int f_00 (int a) {
  if (a>0)  {return  0;};
  if (a>10) {return  1;};
  if (a>20) {return  2;};
  if (a>40) {return  3;}
  else      {return  4;};
}

// missing return allowed
int f_000(int a) {
  if (a>0) {return 0;};
}

// missing return allowed
int f_0000(int a) {
  if (a>0) return 0;
}

/***
int f_0000(int a) {
  if a>0 return 0; // error: expected ‘(’ before ‘a’
}
***/

int f(int a) {
  if (a>0)  {return  0;}
  if (a>10) {return  1;}
  if (a>20) {return  2;}
  if (a>40) {return  3;}
  else      return 4;
}

int f_1(int a) {
  if (a>0)  {return  0;}
  if (a>10) {return  1;}
  if (a>20) {return  2;}
  if (a>40) {return  3;}
  return 4;
}

int f_2(int a) {
	if (a>0)  {
	  return  0;
	} else if (a>10) {
	  return  1;
	} else if (a>20) {
	  return  2;
	} else if (a>40) {
	  return  3;
	} else {
	  return 4;
	}
}
int f_3(int a) {
	if (a>0) {
	  return  0;
	}
	else if (a>10) {
	  return  1;
	}
	else if (a>20) {
	  return  2;
	}
	else if (a>40) {
	  return  3;
	}
	else {
	  return 4;
	}
}

/***
// Not allowed in C empty switch expression
int f_3(int a) {
	switch { // empty switch expression
	case (a>0):  
	  return  0;
	case (a>10): 
	  return  1;
	case (a>20): 
	  return  2;
	case (a>40): 
	  return  3;
	default:
	  return  4;
	}
}

***/

int main() {
}


