package main

import "fmt"

func main() {

	for   i:=0;i<=10;i++ {
		fmt.Printf("%d\n",i);
		switch (i) {
		case 0:
			// case 1:  // try comment and uncomment
			fmt.Print("XS ");
			fallthrough //Instead in Go you can ask C/C++ case behavior with fallthrough:
		case 2,3:
			fmt.Print("S ")
			fallthrough
		case 4,5,6:
			fmt.Print("M")
			fmt.Print(" ")
			fallthrough
		case 7,8,9,10:
			fmt.Print("L ")
			fallthrough
		default:
			fmt.Print("U ")
	      
		}
		fmt.Printf("\n");
  }
  fmt.Println()
}

