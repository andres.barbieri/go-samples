package main

import "fmt"

func main() {

  for   i:=0;i<=10;i++ {
      switch (i) {
	case 0:
	// case 1:  // try comment and uncomment
	  fmt.Print("XS ");
	   //break; // Unlike most other languages like C, C++ or Java,
		    // you don't have to use break to stop one case
		// into continuing execution in following case.
	// case 2 ... 3 // Go Case does not allow ranges	
	case 2,3:
	  fmt.Print("S ")
        case 4,5,6:
	  fmt.Print("M")
	  fmt.Print(" ")
	case 7,8,9,10:
	  fmt.Print("L ")
	default:
	  fmt.Print("U ")
      }
  }
  fmt.Println()
}

