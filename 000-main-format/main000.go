/********************************************************
 * Programa m'inimo en "Go" con errores
 ********************************************************/

/***
go build main000.go 

When compiling multiple packages or a single non-main package,
build compiles the packages but discards the resulting object,
serving only as a check that the packages can be built.

go run main000.go 

package command-line-arguments is not a main package

***/

package core

func main() {
}
