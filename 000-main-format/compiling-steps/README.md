# Aprendiendo estructura de un programa b�sico en "Go"

## Compiladores

 Como para cualquier lenguaje abierto existen varios posibles compiladores para el lenguaje "Go". Tambi�n existen varios pasos en la generaci�n del ejecutable, a similitud como pasa con otros lenguajes compilados como "C" o "C++". En este caso se muestran los pasos para generar el ejecutable en diferentes versiones del compilador de referencia y otro ejemplo usando "cgo" basado en el set de herramientas de "GCC".
 
Probar ejecutando paso a paso y ver los resultados de la compilaci�n en etapas:

C: Fuente -> Pre-procesado -> Assembly -> Objeto (Bin) -> Ejecutable (bin) 

Go: Fuente -> (Assembly) -> Objeto (Bin) -. Ejecutable (bin)


```console
  cat main1.go

  /********************************************************
   * Programa m'inimo en "go"
   ********************************************************/
   package main

   func main() {
   }
```

Generar directamente el binario final:

```console
$ go build main1.go
```

O ejecutar paso a paso de forma manual:
```console

$ go tool compile main1.go 
```

Se genera el c�digo objeto y se "empaqueta" en formato ARCHIVE (AR).

```console
$ file main1.o
main1.o: current ar archive

$ ar -t main1.o
__.PKGDEF
_go_.o

```

Se ve que se se gener� el c�digo objeto y se meti� en un "archive". Para inspecci�n se puede extraer el contenido y luego analizar con alguna herramienta b�sica el binario. Se puede que hay algo de c�digo ASCII y luego binario. 

console```
$ ar -x main1.o _go_.o 

$ file _go_.o 
_go_.o: data

$ head _go_.o 
go object linux amd64 go1.20.1 GOAMD64=v1 X:unified,regabiwrappers,regabiargs,coverageredesign
main


!
go120ld
...
```

Para linkeditar, de acuerdo a la versi�n del compilador puede ser que el runtime se encuentra ya en binario o el linkeditor lo tiene que generar al vuelo.


console```
$ go tool link main1.o 

$ file main1
main1: ELF 64-bit LSB executable, x86-64, version 1 (SYSV), statically linked, Go BuildID=...

```

En el caso de necesitar genera el runtime, por ejemplo obtener el siguiente error:

console```
$ go tool link main1.o 
.../.go/go/pkg/tool/linux_amd64/link: error: unable to find runtime.a

$ go version
go version go1.20.1 linux/amd64
```

Se pueden ver los pasos que se realizan y luego ejecutarlos por separado por ejemplo:

```console
$ go build -x -work main1.go

WORK=/tmp/go-build624904903
mkdir -p $WORK/b001/
cat >/tmp/go-build624904903/b001/importcfg.link << 'EOF' # internal
packagefile command-line-arguments=/home/andres/.cache/go-build/87/87095c5c78d445113a93fc132132334c61d6671b31550e010d55d7510936db42-d
packagefile runtime=/home/andres/.cache/go-build/aa/aa2fe26fb6d31e6b4a758a274c4f0743fac08b996fb5b8d1204d6b1572bcd8f5-d
packagefile internal/abi=/home/andres/.cache/go-build/8e/8ec4968fc2f6138293acbc01913ec2aac072b016dfce5d21e2e0335321fe578d-d
packagefile internal/bytealg=/home/andres/.cache/go-build/c8/c8d52043c509f288e4b391dde6b152cc6b51b467463968c542fc4b909dcce9ce-d
packagefile internal/coverage/rtcov=/home/andres/.cache/go-build/4a/4aff17cb156129b8be1ad5a6261b6eed4f8164ba5dfdaccd459213dc4d0945cb-d
packagefile internal/cpu=/home/andres/.cache/go-build/8c/8c6dce5c0d4aaa979a98f0f59bc29c923d0dde2d28238b8170141278cf55a420-d
packagefile internal/goarch=/home/andres/.cache/go-build/98/98139ed2e1f6bcea287bcda02735e8168014f8ebd5644b652217998b47389f4f-d
packagefile internal/goexperiment=/home/andres/.cache/go-build/8f/8fcad58e667cc287c2d2dd3a8242ee1b11a7dc6c87f264a5c1c6887327799bc7-d
packagefile internal/goos=/home/andres/.cache/go-build/88/8896592ebc9bac9bdf84f1d745e10c35629fe48b5ede229aaa3730d192d00e71-d
packagefile runtime/internal/atomic=/home/andres/.cache/go-build/6c/6cffd83bf996524770297179443e2f354e6ebaf35ae6cb7676380666f90cfd50-d
packagefile runtime/internal/math=/home/andres/.cache/go-build/29/2972918356ed563072b08044c6f6886edbc9498ce1a68990112131af2d26d61f-d
packagefile runtime/internal/sys=/home/andres/.cache/go-build/2c/2c4c5958e67297b81dc412ac01e9004b24a97319a009ae588c3f776f9aeae230-d
packagefile runtime/internal/syscall=/home/andres/.cache/go-build/ed/ed791b27aa25ed20850a4ea7f0ea9bc3a67ebaa14b37182d8b8b0520172d61fd-d
modinfo "0w\xaf\f\x92t\b\x02A\xe1\xc1\a\xe6\xd6\x18\xe6path\tcommand-line-arguments\nbuild\t-buildmode=exe\nbuild\t-compiler=gc\nbuild\tCGO_ENABLED=1\nbuild\tCGO_CFLAGS=\nbuild\tCGO_CPPFLAGS=\nbuild\tCGO_CXXFLAGS=\nbuild\tCGO_LDFLAGS=\nbuild\tGOARCH=amd64\nbuild\tGOOS=linux\nbuild\tGOAMD64=v1\n\xf92C1\x86\x18 r\x00\x82B\x10A\x16\xd8\xf2"
EOF
mkdir -p $WORK/b001/exe/
cd .
/home/andres/.go/go/pkg/tool/linux_amd64/link -o $WORK/b001/exe/a.out -importcfg $WORK/b001/importcfg.link -buildmode=exe -buildid=4GF64nD_kHPWonMjrCVI/H2P2S192czJz3JkWWzrh/MoFTCAdOhZXY58wjetH1/4GF64nD_kHPWonMjrCVI -extld=gcc /home/andres/.cache/go-build/87/87095c5c78d445113a93fc132132334c61d6671b31550e010d55d7510936db42-d
/home/andres/.go/go/pkg/tool/linux_amd64/buildid -w $WORK/b001/exe/a.out # internal
mv $WORK/b001/exe/a.out main1
```

Linkeditar con la salida previamente generada, solamente para saber que hace:

console```
$ go tool link -importcfg /tmp/go-build624904903/b001/importcfg.link main1.o

$ file a.out 
a.out: ELF 64-bit LSB executable, x86-64, version 1 (SYSV), statically linked, with debug_info, not stripped
```

Ver el c�digo assembly:

console```
$ go tool objdump a.out  
TEXT internal/cpu.Initialize(SB) /home/andres/.go/go/src/internal/cpu/cpu.go
  cpu.go:125		0x401000		493b6610		CMPQ 0x10(R14), SP			
  cpu.go:125		0x401004		7638			JBE 0x40103e				
  cpu.go:125		0x401006		4883ec18		SUBQ $0x18, SP				
  cpu.go:125		0x40100a		48896c2410		MOVQ BP, 0x10(SP)			
  cpu.go:125		0x40100f		488d6c2410		LEAQ 0x10(SP), BP			
  cpu.go:125		0x401014		4889442420		MOVQ AX, 0x20(SP)			
  cpu.go:125		0x401019		48895c2428		MOVQ BX, 0x28(SP)			
  cpu.go:126		0x40101e		6690			NOPW					
  cpu.go:126		0x401020		e89b050000		CALL internal/cpu.doinit(SBldd hola        
...

$ go tool objdump a.out  | grep -A10 main1.go
TEXT main.main(SB) /.../main1.go
 main1.go:8		0x458040		c3			RET


go tool objdump main1.o 
TEXT main.main(SB) gofile../.../main1.go
  main1.go:8		0x410			c3			RET
```

En el caso de llamar a alguna funci�n como por ejemplo de I/O el c�digo ser�a un poco m�s extenso. En este caso es solo un main que arranca e inmediatamente termina.




