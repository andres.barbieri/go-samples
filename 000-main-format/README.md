# Programa m�nimo en el lenguaje "C" vs "Rust" vs "Go"

Ver primero que errores tiene los fuentes main0.go, main00.go y main000.go.

```console
 gcc -Wall main-exitcode.c -o main-exitcode-c

 rustc main-exitcode.rs  -o main-exitcode-rs

 go build -o main-exitcode main-exitcode.go
```

Probar ejecutando:

```console
./main-exitcode; echo $?
```



