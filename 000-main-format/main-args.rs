/********************************************************
 * Programa m'inimo en "rust"
 ********************************************************/

// Muestra los argumentos, par'ametros del programa

// Main return value

use std::process::ExitCode;
use std::env;

fn main() -> ExitCode
{
    for argument in env::args() 
    {
        print!("arg \"{argument}\" ");
    }
    println!();
    return ExitCode::SUCCESS
}



