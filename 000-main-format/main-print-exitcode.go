/********************************************************
 * Programa m'inimo en "Go"
 ********************************************************/

// go run main3.go 2> /dev/null ; echo $?
// go run main3.go > /dev/null ; echo $?
package main

//import "os"
//import "fmt"

//import ( "os" ; "fmt" )

import (
	"os"
	"fmt"
)

func main() {
	fmt.Println("bye");
	os.Exit(1)
}
