/*
 * Programa para leer de a un char/byte y lo env�a a la salida hasta EOF
 * Usa funciones de OS Low level (syscalls), STDIN/STDOUT
 */

#include <unistd.h>

#define STDIN  0
#define STDOUT 1
int main()
{
  int c;

  //while(read(Stdin,&c,sizeof(c))>0) {
  for(;read(STDIN,&c,sizeof(c));) {
    write(STDOUT,&c,sizeof(c));
      }
  return 0;
}

/*** EOF ***/
