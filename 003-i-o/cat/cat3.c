/*
 * Programa para leer de a un char/byte y lo env�a a la salida hasta EOF
 * Usa fuciones de stdio sobre STDIN/STDOUT buffered
 */

#include <stdio.h>

int main()
{
  int c;

  for(;fread(&c,sizeof(c),1,stdin);) {
    fwrite(&c,sizeof(c),1,stdout);
      }
  return 0;
}

/*** EOF ***/
