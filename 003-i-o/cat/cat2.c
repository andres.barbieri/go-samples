/*
 * Programa para leer de a un char/byte y los env�a a la salida hasta EOF
 */

#include <stdio.h>

int main()
{
  int c;

  while ((c =  getc(stdin)) != EOF) {
    putc(c,stdout);
    //fflush(stdout); // Not needed
  }
  return 0;
}

/*** EOF ***/
