/*
 * Programa para leer de a un char/byte y lo env�a a la salida hasta EOF
 * Funciones char oriented 
 */

#include <stdio.h>

int main()
{
  int c;

  while ((c = getchar()) != EOF) {
    putchar(c);
  }
  return 0;
}

/*** EOF ***/
