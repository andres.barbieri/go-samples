/*
 * Programa para leer de a un char/byte y los envía a la salida hasta EOF
 */

package main

import "fmt"
import "bufio"
import "os"

func main() {

	// var input byte
	// var err error
	// var in Reader
	in := bufio.NewReader(os.Stdin)
	c, err := in.ReadByte()
	for (err == nil) {
		fmt.Printf("%c",c)
		c, err = in.ReadByte()	
	}
}

/*** EOF ***/
