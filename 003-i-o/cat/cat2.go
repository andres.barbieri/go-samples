/*
 * Programa para leer de a un char/byte y los envía a la salida hasta EOF
 * Usa buffers sobre STDIN y STDOUT
 */

package main

import "bufio"
import "os"

func main() {

	// var input byte
	// var err error
	// var in Reader
	// var out Writer
	in  := bufio.NewReader(os.Stdin)
	out := bufio.NewWriter(os.Stdout)
	c, err := in.ReadByte()
	for (err == nil) {
		err = out.WriteByte(c)
		out.Flush()
		c, err = in.ReadByte()	
	}
}

/*** EOF ***/
