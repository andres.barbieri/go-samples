/*
 * Programa para leer de a un char/byte y los envía a la salida hasta EOF
 * Usa STDIN y STDOUT
 */

package main

import   "os"

func main() {

	// var input byte
	// var err error
	// var in  File
	// var out File

	in  := os.Stdin
	out := os.Stdout
	//c := make([]byte,1)
	c := []byte{' '}
	count,err := in.Read(c)
	for ((err == nil)&&(count>0)) {
		count,err = out.Write(c)
		count,err = in.Read(c)	
	}
}

/*** EOF ***/
