package main

import "fmt"
import "bufio"
import "os"

func main() {

	// var input byte
	// var err   error
	// var in    Reader
	in := bufio.NewReader(os.Stdin)
	c, err := in.ReadByte()
	if (err != nil) {
		fmt.Println("ERROR:",err)
	} else {
		fmt.Printf("[%c][%X]",c,c)
		in.Reset(os.Stdin) // input empty buffer skip_line
	}
	c, err = in.ReadByte()	
	if (err != nil) {
		fmt.Println("ERROR:",err)
	} else {
		fmt.Printf("[%c][%X]",c,c)
	}
	fmt.Println()
}

/*** EOF ***/
