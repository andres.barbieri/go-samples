/*
 * Programa para leer un char/byte y lo env�a a la salida
 */

#include <stdio.h>


void skip_line()
{    
  int c = 0;
  if (!feof(stdin))
    {
      while ((c = getchar()) != '\n' && c != EOF);
    }
}

//
// Make sure to call this function after a succesful read
//
void simple_skip_line()
{
  int c = 0;

  while ((c != EOF)&&(c!='\n')) {
    c = getchar();
  }
}

int main()
{
  int c;

  c = getchar(); // getchar() == getc(stdin), getc == MACRO, fgetc == func
  //putchar(c1);
  if (c != EOF) {
    printf("[%c][%X]",c,c);
    skip_line();
  } else {
    perror("ERROR:");
  }
  c = getchar(); // getchar() == getc(stdin), getc == MACRO, fgetc == func
  if (c != EOF) {
    printf("[%c][%X]",c,c);
   }   else {
    perror("ERROR:");
  }
  printf("\n");
  return 0;
}

/*** EOF ***/
