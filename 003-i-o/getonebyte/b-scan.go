/*
 * Programa para leer un solo char/byte y lo envía a la salida
 */

package main

import "fmt"

func main() {

	// var n int
	// var err error
	
	var c byte
	n , err := fmt.Scanf("%c",&c)
	if (err != nil) {
		fmt.Println("ERROR:",n,err)
	} else {
		fmt.Printf("[%c][%X]",c,c)
	}
	n, err = fmt.Scanf("%c",&c)	
	if (err != nil) {
		fmt.Println("ERROR:",n,err)
	} else {
		fmt.Printf("[%c][%X]",c,c)
	}
	fmt.Println()
}

/*** EOF ***/
