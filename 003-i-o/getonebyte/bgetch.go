/*
 * Programa para leer un solo char/byte y lo envía a la salida
 */

package main

import "fmt"
import "bufio"
import "os"

func main() {

	// var input byte
	// var err error
	// var in Reader
	in := bufio.NewReader(os.Stdin)
	c, err := in.ReadByte()
	if (err != nil) {
		fmt.Println("ERROR:",err)
	} else {
		fmt.Printf("[%c][%X]",c,c)
	}
	c, err = in.ReadByte()	
	if (err != nil) {
		fmt.Println("ERROR:",err)
	} else {
		fmt.Printf("[%c][%X]",c,c)
	}
	fmt.Println()
}

/*** EOF ***/
