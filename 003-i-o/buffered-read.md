
BufferedReader is a lot faster than Scanner because it buffers the character so you don't have to access the file each time you want to read a char from it.

Scanner are of particular use such as reading primitive data type directly and is also used for regular expressions.


BufferedReader is synchronous while Scanner is not. 
BufferedReader should be used if we are working with multiple threads.
BufferedReader has significantly larger buffer memory than Scanner.
The Scanner has a little buffer as opposed to the BufferedReader, but it’s more than enough.
BufferedReader is a bit faster as compared to scanner because the scanner does parsing of input data and BufferedReader simply reads a sequence of characters.