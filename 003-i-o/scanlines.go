package main

import (
    "bufio"
    "fmt"
    "log"
    "os"
)

func read1() {
    fmt.Println("input text:")
    scanner := bufio.NewScanner(os.Stdin)
    scanner.Scan()
    err := scanner.Err()
    if err != nil {
        log.Fatal(err)
    }
    fmt.Printf("read line: %s-\n", scanner.Text())
}


func read2() {
    fmt.Println("input text:")
    reader := bufio.NewReader(os.Stdin)
    line, err := reader.ReadString('\n')
    if err != nil {
        log.Fatal(err)
    }
    fmt.Printf("read line: %s-\n", line)
}


func read3() {
    fmt.Println("input text:")
    var w1, w2, w3 string
    n, err := fmt.Scanln(&w1, &w2, &w3)
    if err != nil {
        log.Fatal(err)
    }
    fmt.Printf("number of items read: %d\n", n)
    fmt.Printf("read line: %s %s %s-\n", w1, w2, w3)
}

func main() {
	read1()
	read2()
	read3()
}
