package main

import "fmt"

//a := 10 syntax error
var a = 10

func main() {
	//declaration inside a declaration is not allowed:
	//func f() { // syntax error
	// lambda declaration is allowed
	f := func() {
		a := 40
		fmt.Printf("%d\n",a);  		
	}
	fmt.Printf("%d\n",a);  
	a := 20;
	{
		a := 30;
		fmt.Printf("%d\n",a);
	}
	f()
	fmt.Printf("%d\n",a);
	f()
}
