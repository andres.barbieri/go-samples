
// let a = 10; // error: expected item, found keyword `let` const|static
// const a: i32 = 10;

fn  main()
{
  let a = 10;  
  println!("{}",a);  
  let a = 20;
  {
    let a = 30;
  println!("{}",a);  
  }
  println!("{}",a);      
}
