package main

import "fmt"

var a = 10

func main() {
  fmt.Printf("%d\n",a);  
  a := 20;
  {
    a := 30;
    fmt.Printf("%d\n",a);
  }
  fmt.Printf("%d\n",a);
}
