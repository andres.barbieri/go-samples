package main

import "fmt"

func main() {

	anArray   := [...]int{1,2,3}
	
	var mapAny    map[string]any
		
	var map3    = map[string]string{ "uno"  : "1.uno",
	 	                         "dos"  : "2.dos",
		                         "tres" : "3.tres", }
	
	mapAny = make(map[string]any)
	mapAny["uno"] = 1
	mapAny["dospunto3"] = 2.3
	mapAny["string"] = "string"
	//mapAny["me"] = mapAny // stack overflow at Println
	mapAny["nil"] = nil
        mapAny["array"] = anArray
	mapAny["anotherMap"] = map3
	
	for k,v:=range mapAny {
		fmt.Println(k,v)
	}
}
