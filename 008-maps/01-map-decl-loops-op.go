// Array: sequence of elements of a single type with a fixed length
package main

import "fmt"

func main() {
	var arr  =  [...]string{ "uno", "dos", "tres" }
	
	var map1    map[string]int
	
	var map2  = map[string]float32{ "uno"  : 1.0,
		                        "dos"  : 2.0,
		                        "tres" : 3.0, }
	
	var map3  = map[string]string{ "uno"  : "1.uno",
	 	                       "dos"  : "2.dos",
		                       "tres" : "3.tres", }
	
	fmt.Println("uno=",map1["uno"])
	fmt.Println("no existe=",map1["no existe"])
	fmt.Println("uno=",map2["uno"])
	fmt.Println("no existe=",map2["no existe"])
	fmt.Println("uno=",map1["uno"])
	fmt.Println("no existe=",map1["no existe"])
	fmt.Println("uno=",map3["uno"])
	fmt.Println("no existe=",map3["no existe"])
	_,ok := map3["no existe"]
	fmt.Println(ok)
	_,ok = map3["uno"]
	fmt.Println(ok)

	var map4 map[string]int = make(map[string]int)
	for i,v := range arr {
		map4[v] = i
	}
		
	fmt.Println(arr, map1, map2, map3, map4)

        for k,v:=range map2 {
		fmt.Println(k,v)
	}
	
	map2["cuatro"] = 4.0
	// map1["uno"] = 1 //panic: assignment to entry in nil map
	map1 = make(map[string]int)
	map1["uno"] = 1
	map1["dos"] = 2

	delete(map1,"uno")
	delete(map1,"no existe")
	fmt.Println(map1)
	
	for k,v:=range map2 {
		fmt.Println(k,v)
	}
	for k,v:=range map1 {
		fmt.Println(k,v)
	}
	for k,v:=range map3 {
		fmt.Println(k,v)
	}
}
