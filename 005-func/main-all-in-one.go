/*
 * Power test program
 *
 */

package main

import "fmt"

func ipow(base int32 ,n int32) int64 {
  var res int64  = 1
	for i:=0;i<int(n);i++ {
		res*= int64(base)
    }
  return res
}

func ipower(base int32 ,n int32) int64 {
  return ipow(base, n)
}

func main() {
  fmt.Printf("%d %d %d %d\n",
          ipower(2,2), ipower(2,5), ipower(2,10), ipower(2,15));
}

/*** EOF ***/
