//Under the hood, interface values can be thought of as a
// tuple of a value and a concrete type: (value, type)
// An interface value holds a value of a specific underlying concrete type.
// Calling a method on an interface value executes the method of the same
// name on its underlying type.
// https://go.dev/tour/methods/11

package main

import "fmt"
import "strconv"

type Intable interface {
	Int() int
	StringInt() string
}

type MyInt int
func (i MyInt) Int() int { return int(i) }
func (i MyInt) StringInt() string { return "["+strconv.Itoa(i.Int())+"]" }

type MyStr string
func (s1 MyStr) Int() int { return len(s1) }
func (s  MyStr) StringInt() string { return "["+strconv.Itoa(s.Int())+"]" }

type Algo struct {
	i int
	s string
}
func (a Algo) Int() int { return a.i}
func (a Algo) StringInt() string { return "["+strconv.Itoa(a.Int())+"]" }


func Sum(slice []Intable) int {
	var sum int = 0
	for i:=range slice {
		sum = sum + (slice[i]).Int()
	}
	return sum
}

func describe(i Intable) {
	fmt.Printf("(%v, %T)\n", i, i)
}

func main() {
	var anSlice []Intable = make([]Intable,3)
	
	var i MyInt = 10
	fmt.Println(i, i.Int(), i.StringInt())
	anSlice[0] = (i)

	var s MyStr = "diez"	
	fmt.Println(s, s.Int(), s.StringInt())
	anSlice[1] = (s)
	
	var h Intable
	h = i
	fmt.Println(h, h.Int(), h.StringInt())
	anSlice[2] = (h)
	
	var a Algo
	a.i = 10
	a.s = string(s)
	anSlice = append(anSlice,a)

	h = a
	fmt.Println(h, h.Int(), h.StringInt())	
	anSlice = append(anSlice,h)
	
	fmt.Println(anSlice)
	fmt.Println(Sum(anSlice))

	for _,v := range anSlice {
		describe(v)
	}
		
}
