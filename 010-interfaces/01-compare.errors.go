package main

import "fmt"
import "strconv"

type Comparable interface {
	Less(j Comparable) bool
	String() string
}

type MyInt int
//func (i MyInt) Less(j MyInt) bool { return i<j }
func (i MyInt) Less(j Comparable) bool { return i < (j.(MyInt))   }
func (i MyInt) String() string { return strconv.Itoa(int(i)) }

type MyStr string
func (s1 MyStr) Less(s2 Comparable) bool { return s1 < (s2.(MyStr))   }
//func (s1 MyStr) Less(s2 MyStr) bool { return s1 < s2}
func (s MyStr) String() string { return string(s) }

func main() {
	var i MyInt = 10
	var j MyInt = 20

	fmt.Println(i,"<" ,j,"=", i.Less(j))
	fmt.Println(j,"<",i ,"=", j.Less(i))
	fmt.Println(i,"==",j ,"=", i == j)

	var h Comparable
	h = i
	fmt.Println(h)

	var s MyStr = "diez"
	var w MyStr = "veinte"

	fmt.Println(s,"<" ,w,"=", s.Less(w))
	fmt.Println(w,"<",s ,"=", w.Less(s))
	fmt.Println(w,"==",s ,"=", w == s)

	//fmt.Println(i,"<" ,w,"=", i.Less(w)) // Run time error: panic: interface conversion: main.Comparable is main.MyStr, not main.MyInt
	//fmt.Println(i,"==",w ,"=", i == w) // Compile time error: invalid operation: i == w (mismatched types MyInt and MyStr)
}
