package main

import "fmt"
//import "strconv"

type Summable interface {
	Sum(j Summable) Summable
	Null() Summable
}

type MyInt int
func (i MyInt) Sum(j Summable) Summable { return i + (j.(MyInt))   }
func (i MyInt) Null() Summable { return MyInt(0) }

type MyStr string
func (i MyStr) Sum(j Summable) Summable { return i + (j.(MyStr))   }
func (i MyStr) Null() Summable { return MyStr("") }


func main() {
	var i MyInt = 10
	var j MyInt = 20
	//var k MyInt = (i.Null()) //  cannot use (i.Null()) (value of type Summable) as MyInt value in variable declaration: need type assertion
        var k MyInt = (i.Null()).(MyInt)
        var l Summable = (i.Null())
	
	fmt.Println(i,"+" ,j,"=", i.Sum(j))
	fmt.Println(k,"+" ,j,"=", k.Sum(j))
	fmt.Println(l,"+" ,k,"=", l.Sum(k))	
}
