package main

import "fmt"

func main() {
	var i interface{} // var i any

	i = "hola"
	s := i.(string)
	fmt.Println(s)

	s, ok := i.(string)
	fmt.Println(s, ok)

	f, ok := i.(float64)
	fmt.Println(f, ok)

	i = 20.34

	f, ok = i.(float64)
	fmt.Println(f, ok)
	//f = i.(float64) // panic
	//fmt.Println(f)
}
