// Under the hood, interface values can be thought of as a
// tuple of a value and a concrete type: (value, type)
//
// An interface value holds a value of a specific underlying concrete type.

// Calling a method on an interface value executes the method of the same
// name on its underlying type.
	
package main

import "fmt"
import "strconv"

type Intable interface {
	Int() int
	StringInt() string
}

type MyInt int
//func (i MyInt) Int() int { return int(i) }
func (i *MyInt) Int() int { return int(*i) } // Pointer Recvr
func (i MyInt) StringInt() string { return "["+strconv.Itoa(i.Int())+"]" }

type MyStr string
func (s1 MyStr) Int() int { return len(s1) }
func (s  MyStr) StringInt() string { return "["+strconv.Itoa(s.Int())+"]" }

type Algo struct {
	i int
	s string
}
func (a Algo) Int() int { return a.i}
func (a Algo) StringInt() string { return "["+strconv.Itoa(a.Int())+"]" }

func Sum(slice []Intable) int {
	var sum int = 0
	for i:=range slice {
		sum = sum + (slice[i]).Int()
	}
	return sum
}

func main() {
	//var anSlice []Intable = make([]Intable,3)
	
	var i MyInt = 10
	// Pointer methods Works on types 
	fmt.Println(i, i.Int(), i.StringInt()) 

	// Pointer on pointer method
	var j *MyInt
	j = new(MyInt)
	*j = 20
	fmt.Println(j, j.Int(), j.StringInt())
	
	var s MyStr = "diez"	
	fmt.Println(s, s.Int(), s.StringInt())
	
	var h Intable
	// Pointer methods not works on interfaces
	// h = i //cannot use i (variable of type MyInt) as Intable value in assignment: MyInt does not implement Intable (method Int has pointer receiver)
	h = &i
	fmt.Println(h, h.Int(), h.StringInt()) 	
}
