package main

import "fmt"
//import "strconv"

type Summable interface {
	Sum(j Summable) Summable
//	Null() Summable
}

type MyInt int
func (i MyInt) Sum(j MyInt) MyInt { return i + j }
//func (i MyInt) Dec() MyInt { return i-MyInt(1) }
//func (i MyInt) Null() MyInt { return MyInt(0) }
//func (i MyInt) One() MyInt { return MyInt(1) }

func SumSlice(s []Summable) Summable {
	var sum Summable
	for i:= range s {
		sum = sum.Sum(s[i]) 
	}
	return sum
}

func main() {
	anArray := []MyInt{1,2,3,4,5,6}   
	var i MyInt = 10
	var j MyInt = 20
        //var z MyInt = i.Null()
//	fmt.Println(i.Dec())
	//fmt.Println((i.Sum(j)).Sum(z))
	fmt.Println(anArray)
	fmt.Println(SumSlice(Summable(anArray)))
}
