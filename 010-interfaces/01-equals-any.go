package main

import "fmt"

// "interface {}" and "any" are alias

// Comparing interfaces
//
// When two interface values are compared the run-time will only panic if they
// are of the same type and it is non-comparable. If they contain different
// types then the result is false even if one or both types are non-comparable.

// What are non-comparable types? Basically, they are slices, maps,
// functions and any struct or array type that uses them.
	
func equals(a,b any) bool {
	return (a == b)
}

func notEquals(a,b any) bool {
	return (a != b)
}

func isNil(a any) bool {
	return (a == nil)
}

type Algo struct {
	x int
	y string
}

func main() {
	var a int = 20
	var b int = 30
	var c int = 20
	fmt.Println(a,b,equals(a,b),equals(a,c),equals(a,a))
	x := Algo{23,"uno"}
	y := Algo{1000,"otro"}
        z := Algo{1000,"otro"}
	fmt.Println(x,y,equals(x,y),equals(y,z),equals(y,y))
	fmt.Println(a,y,equals(a,y),equals(nil,z))
	py := &y
	pz := &z
	fmt.Println(py,y,equals(py,y),equals(nil,py))
	fmt.Println(py,pz,equals(py,pz),equals(nil,pz),isNil(pz))
	fmt.Println(py,pz,notEquals(py,pz),isNil(nil))

	var s []int = make([]int,0)
	var w []int = make([]int,0)	
	fmt.Println(equals(s,nil),isNil(s))	
	fmt.Println(equals(s,a))
	fmt.Println(equals(w,x))
	//fmt.Println(equals(s,w)) // panic: runtime error: comparing uncomparable type []int
	//fmt.Println(equals(s,s)) // panic: runtime error: comparing uncomparable type []int

	var m map[int]int
	var n map[int]int	
	fmt.Println(equals(s,m))
	//fmt.Println(equals(n,m)) //runtime error: comparing uncomparable type map[int]int
	fmt.Println(equals(m,nil),isNil(n))
	fmt.Println((m == nil),isNil(m))
	fmt.Println((n == nil),isNil(n)) 	
}
