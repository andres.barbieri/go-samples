package main

import "fmt"

func swp(a,b *any) {
	//*a, *b = *b, *a
	aux := *a
	*a = *b
	*b = aux
}

func main() {
	var a int = 20
	var b int = 30
	fmt.Println(a,b)
	swp(any(&a),any(&b))
	fmt.Println(a,b)	
}
