package main

import "fmt"
import "strconv"

type Comparable interface {
	Less(j Comparable) (bool,bool)
	String() string
}

type MyInt int
//func (i MyInt) Less(j MyInt) bool { return i<j }
func (i MyInt) Less(j Comparable) (bool,bool) {
	_, ok := j.(MyInt)
	if ok { return (i < (j.(MyInt))),true } else {return false,false}
}
func (i MyInt) String() string { return strconv.Itoa(int(i)) }

type MyStr string
func (s1 MyStr) Less(s2 Comparable) (bool,bool) {
	_, ok := s2.(MyStr)
	if ok { return (s1 < (s2.(MyStr))),true } else {return false,false }
}

//func (s1 MyStr) Less(s2 MyStr) bool { return s1 < s2}
func (s MyStr) String() string { return string(s) }

func main() {
	var i MyInt = 10
	var j MyInt = 20


	var h Comparable
	h = i
	fmt.Println(h)

	var s MyStr = "diez"
	var w MyStr = "veinte"

	result,ok := i.Less(j)
	if ok {fmt.Println(i,"<" ,j,"=",result)} else {fmt.Println("Nocompat")}
	result,ok = j.Less(i)
	if ok {fmt.Println(j,"<" ,i,"=",result)} else {fmt.Println("Nocompat")} 
	result,ok = w.Less(s)
	if ok {fmt.Println(s,"<" ,w,"=",result)} else {fmt.Println("Nocompat")} 
	result,ok = s.Less(w)
	if ok {fmt.Println(w,"<" ,s,"=",result)} else {fmt.Println("Nocompat")} 
	result,ok = s.Less(i)
	if ok {fmt.Println(s,"<" ,i,"=",result)} else {fmt.Println("Nocompat")}
	result,ok = i.Less(s)
	if ok {fmt.Println(i,"<" ,s,"=",result)} else {fmt.Println("Nocompat")} 
	
}
