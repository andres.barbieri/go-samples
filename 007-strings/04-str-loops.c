#include<stdio.h>  // printf
#include<string.h> // strlen

#define STR_LEN  10

#define STR_MACRO "hola"

int main()
{
  char str[] = "hola123456";
  char *str2 = "hola123456";

  // str should be terminated with \0

  printf("\n##\n"); for(int i=0;i<strlen(str);i++) printf("%c",str[i]);

  printf("\n##\n"); for(int i=0;i<STR_LEN;i++) printf("%c",str[i]);

  printf("\n##\n"); for(int i=0;str[i];i++) printf("%c",str[i]); 

  printf("\n##\n"); for(char *p=str;*p;p++) printf("%c",*p); 

  printf("\n##\n"); int j=0; while(str[j]) {printf("%c",str[j]); j++;};

  //                       while(str[j]!='\0') 
  printf("\n##\n"); j = 0; while(str[j]!=0) {printf("%c",str[j]); j++;};
  //lvalue required as increment operand
  // arrays are constant pointers, it means after declaration it's value can't be changed. 
  //for(;*str;str++) printf("%c",*str);

  // Qu� problema podemos tener ?  
  printf("\n##\n");
  printf("&str2=%p str2=%p\n",&str2,str2);
  for(;*str2;str2++) printf("%c",*str2); // str2 should be terminated with \0
  printf("\n&str2=%p str2=%p\n",&str2,str2);  
  return 0;
}
