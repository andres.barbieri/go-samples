// For strnlen as GNU extension
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include<stdio.h>  //printf
#include<string.h> //strlen

#define STR_LEN  9

#define STR_MACRO "hola"

int main()
{
  char str[]         =  "hola";
  char str1[]        =  {'h', 'o', 'l', 'a', 0 };
  char str2[5]       = { 'a', 'b', 'c', 'd', '\0' };
  char dst[STR_LEN];                      // Not init'd
  //char str3[STR_LEN] = "hola";            /* Incomplete init will be filled with 0 */
  //char str4[STR_LEN] = {'h','o','l','a'}; /* Incomplete init will be filled with 0 */
  //error: wide character array initialized from non-wide string
  //int  str5[]   = "hola";
  int  i;

  // String dimmensions
  // typeof(sizeof) == long unsigned int
  printf("sizeof(str)==%u len==%u\n",(unsigned int)sizeof(str),(unsigned int)strlen(str));
  printf("sizeof(str1)==%u len==%u\n",(unsigned int)sizeof(str1),(unsigned int)strlen(str1));
  printf("sizeof(str2)==%u\n",(unsigned int)sizeof(str2));
  printf("sizeof(STR_MACRO)==%u len==%u\n",(unsigned int)sizeof(STR_MACRO),(unsigned int)strlen(STR_MACRO)); 
  printf("sizeof(dst)==%u len==%u\n",(unsigned int)sizeof(dst),(unsigned int)strnlen(dst,STR_LEN)); 

   // String accessing
   printf("str=%s str2=%s ",str,str1);

   for(i=0;str[i];i++)
     {
       printf("%c",str[i]);
     }
   printf("\n###\n");
  
   // string's elements in reverse order
   for(i=strlen(str);i>=0;i--)
     {
       printf("%c",str[i]);
     }
   printf("\n###\n");
   // string's elements
   printf("dst[%d]=\'%c\'..dst[%d] =\'%c\'\n",0, dst[0],STR_LEN-1, dst[STR_LEN-1]);
   printf("\"hola\"[%d]=%c\n",0,"hola"[0]);

   //printf("str3[%d]=\'%c\'..str3[%d] =\'%c\'\n",0, str3[0],(int)strlen(str), str3[strlen(str)]);
   //printf("str3[%d]=\'%c\'..str3[%d] =\'%c\'\n",0, str4[0],STR_LEN-1, str4[STR_LEN-1]);
  return 0;
}


