package main

import "fmt"
import "unicode/utf8"


func reverse(s string) string {
    r := []rune(s)
    for i, j := 0, len(r)-1; i < len(r)/2; i, j = i+1, j-1 {
        r[i], r[j] = r[j], r[i]
    }
    return string(r)
}

func reverse2(s string) string {
	r := []rune(s)

	for i:=0; i<(len(r)/2);i++ {
	aux := r[i]
        r[i] = r[len(r)-i-1]
	r[len(r)-i-1] = aux
	}
	return string(r)
}

func reverse3(str string) string {
	if (len(str) == 0) {
		return str
	} else if (len(str) == 1) {
		return str
	} else {
		return reverse(str[1:])+string(str[0])
	}
}

func main() {
	fmt.Println(reverse("f̅ñHOLA QUE TAL COMO VAS áéí"))			
	fmt.Println(reverse2("f̅ñHOLA QUE TAL COMO VAS áéí"))
	fmt.Println(reverse3("f̅ñHOLA QUE TAL f̅COMO VAS áéí"))
}

