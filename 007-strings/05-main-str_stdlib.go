// https://pkg.go.dev/strings

package main

import "fmt"
import "strings"
import "unicode/utf8"

func main() {
	str  :=  "hola"
	var str1 string = "hola que tal la"
	var dst string
	str2 :=  `hola`
	var str4 string = "niñósS"
        var dst2 string
	
	fmt.Printf("len=%d count=%d\n",len(str),utf8.RuneCountInString(str))
	fmt.Printf("len=%d count=%d\n",len(str4),utf8.RuneCountInString(str4))
	
	//fmt.Printf("dst = \"%s\" %d\n",dst,len(dst))		
        //dst = str + " " + str1
	//fmt.Printf("dst = \"%s\" %d\n",dst,len(dst))
	
	fmt.Printf("\"%s\" == \"%s\" (%d)\n",str1,str1,strings.Compare(str1,str1))
	fmt.Printf("\"%s\" == \"%s\" (%d)\n",str,str4,strings.Compare(str,str4))
	fmt.Printf("\"%s\" == \"%s\" (%d)\n",str4,str,strings.Compare(str4,str))	
	fmt.Printf("\"%s\" == \"%s\" (%d)\n",str,str2,strings.Compare(str2,str))	
	fmt.Printf("\"%s\" == \"%s\" (%d)\n",str1,dst,strings.Compare(str1,dst))
	fmt.Printf("\"%s\" == \"%s\" (%d)\n",dst,str,strings.Compare(dst,str))
	
	fmt.Printf("\"%s\" in \"%s\" (%t)\n",str,str1,strings.Contains(str1,str))
	fmt.Printf("\"%s\" in \"%s\" (%t)\n",dst,str,strings.Contains(str,dst))
	fmt.Printf("\"%s\" in \"%s\" (%t)\n",str,str4,strings.Contains(str,str4))

	fmt.Printf("\"%s\" in \"%s\" (%t)\n",str1,"la",strings.Contains(str1,"la"))
	fmt.Printf("\"%s\" occurrs in \"%s\" (%d)\n","la",str1,strings.Count(str1,"la"))
	fmt.Printf("\"%s\" occurrs in \"%s\" (%d)\n","que",str1,strings.Count(str1,"que"))

	fmt.Printf("\"%s\" index \"%s\" (%d)\n","que",str1,strings.Index(str1,"que"))
	
	fmt.Println(strings.Repeat("A", 10))
	
	dst = strings.Replace(str1, "la", "LA", -1)
        fmt.Println(dst)
        fmt.Println(strings.Replace(str1, "la", "LA",  1))
        fmt.Println(strings.ToUpper(dst))
        fmt.Println(strings.ToLower(dst))

	
	dst2 = strings.Clone(str)
	fmt.Printf("\"%s\" == \"%s\" (%d)\n",dst2,str,strings.Compare(dst2,str))
	fmt.Printf("\"%s\" == \"%s\" (%t)\n",dst2,str, dst2 == str)
	
}


