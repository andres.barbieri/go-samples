package main

import "fmt"
import "unicode/utf8"


const STR_LEN =  4
const STR_POS =  1

func main() {
	str  :=  "hola"
	var str1 string = "que tal"
	var dst string
	str2 :=  `hola`
	var str4 string = "niñósS"
        var b byte = 'b'
	
	fmt.Printf("str[%d]=%c\n",STR_POS,str[STR_POS]);
	fmt.Printf("str[%d]=%c\n",STR_LEN-1,str[STR_LEN-1]);
	fmt.Printf("str[%d]=%d\n",STR_POS,str[STR_POS]);
	//str[STR_POS] = 'a'; // cannot assign to str[STR_POS] (value of type byte)

	fmt.Printf("len=%d count=%d\n",len(str),utf8.RuneCountInString(str))
	fmt.Printf("len=%d count=%d\n",len(str4),utf8.RuneCountInString(str4))
	
	fmt.Printf("dst = \"%s\" %d\n",dst,len(dst))		
        dst = str + " " + str1 + string('a') + string(b) // concat str + str , concat str = byte
	fmt.Printf("dst = \"%s\" %d\n",dst,len(dst))
	fmt.Printf("\"%s\" == \"%s\" (%t)\n",str1,str1,str1 == str1)
	fmt.Printf("\"%s\" == \"%s\" (%t)\n",str,str2,str == str2)
	fmt.Printf("\"%s\" == \"%s\" (%t)\n",str1,dst,str1 == dst)
	fmt.Printf("\"%s\" > \"%s\" (%t)\n",str,str2,str > str2)	
	fmt.Printf("\"%s\" < \"%s\" (%t)\n",str,str2,str > str2)		
}


