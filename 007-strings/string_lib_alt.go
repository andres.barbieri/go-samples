//package string_lib

package main

import "fmt"
import "strings"
import "unicode/utf8"
// import "unsafe"

func str_len(s string) uint {
	if (s == "") {
		return 0
	}
	return 1+str_len(s[1:])
}

func str_cmp(s1, s2 string) int {
	if ((s1=="")&&(s2=="")) {return  0}
	if ((s1=="")&&(s2!="")) {return  1}
	if ((s1!="")&&(s2=="")) {return -1}
	if (s1[0] < s2[0])      {return -1}
	if (s1[0] > s2[0])      {return  1}
	return str_cmp(s1[1:],s2[1:]) // s1[0] == s2[0] 
}

func str_cat (s1 string, s2 string) string {
   var aux string = s1
	for pos,_ := range s2 {
		aux = aux + s2[pos:pos+1]
	}
  return aux
}

func str_lastb (s string) string {
	if ( len(s)>0 ) {
		return s[len(s)-1:len(s)]
	} else {
		return ""
	}
}

func str_lastr (s string) string {
	if ( len(s)>0 ) {
		return string(rune(s[len(s)-1]))
	} else {
		return ""
	}
}

func str_last (s string) string {
	if (len(s)>0) {
		r,_ := utf8.DecodeLastRuneInString(s)
		return string(r)
	}
	return ""
}

func str_dup (s string) string {
   var aux string
	for pos,_ := range s {
		aux = aux + s[pos:pos+1]
	}
  return aux
}


/***
func str_dup2 (s string) string {
	if len(s) == 0 {
		return ""
	}
	b := make([]byte, len(s))
	copy(b, s)
	return unsafe.String(&b[0], len(b))
}
***/


func main() {
	s1 := "HOLA"
	s2 := "Jsããẽï"
        s3 := ""
        s4 := "hola que tal"
        s5 := "hola que tal"
        s6 :=  "1"
	
	fmt.Println(str_len(s1), str_len(s2), str_len(s3), str_len(s4))
	fmt.Println(str_cmp(s1,s5), str_cmp(s3,s1), str_cmp(s1,s4), str_cmp(s4,s1), str_cmp(s4,s5), str_cmp(s5,s4), str_cmp(s2,s2), str_cmp(s1,"HOLA"))
	fmt.Println(str_cat(s1,s5), str_cat(s1, "que tal?"))
        fmt.Println(str_last(s1), str_last(s2), str_last(s3), str_last(s4), str_last(s6))
        fmt.Println(str_lastr(s1), str_lastr(s2), str_lastr(s3), str_lastr(s4), str_lastr(s6))
        fmt.Println(str_lastb(s1), str_lastb(s2), str_lastb(s3), str_lastb(s4), str_lastb(s6))		
	fmt.Println(str_cmp(s4,s3), s4 == s3)
	s3 = str_dup(s4)
	fmt.Println(str_cmp(s4,s3), s4 == s3)
	fmt.Printf("%s%s%s == %s\n","\"",s4,"\"",s3)
        s7 := strings.Clone(s4)	
	fmt.Println(str_cmp(s4,s7), s4 == s7)
	fmt.Printf("%s%s%s == %s\n","\"",s7,"\"",s4)

}

/** EOF ** string_lib **/





