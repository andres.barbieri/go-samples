package main

import "fmt"
import "unicode/utf8"

const STR_LEN =  6 + 4

func main() {
	str  :=   "hola123456"
	str4 :=   "niñóS"
	
	fmt.Printf("\n##\n")
	for i:=0;i<len(str);i++ {
		fmt.Printf("%c",str[i])
	}
	fmt.Printf("\n##\n")
	for i:=0;i<STR_LEN;i++ {
		fmt.Printf("%c",str[i])
	}
	fmt.Printf("\n##\n")
		for _,char := range str {
		fmt.Printf("%c",char)
		}
	fmt.Printf("\n##\n")
	for pos,_ := range str {
		fmt.Printf("%c",str[pos])
	}
	fmt.Printf("\n##\n")
	for pos := range str {
		fmt.Printf("%c",str[pos])
	}
	fmt.Printf("\n##\n")
		for i:=0;i<utf8.RuneCountInString(str);i++ {
		fmt.Printf("%c",str[i])
	}
	fmt.Printf("\n##\n")
	// Incorrect
	for i:=0;i<utf8.RuneCountInString(str4);i++ {
		fmt.Printf("%c",str4[i])
	}
	fmt.Printf("\n##\n")
	// Correct
	for i:=0; i < len(str4); {
		r, size := utf8.DecodeRuneInString(str4[i:])
		fmt.Printf("%d\t%c ", i, r)
		i += size
	}
	// Correct
	fmt.Printf("\n##\n")
		for pos,char := range str4 {
		fmt.Printf("%d\t%c ",pos, char)
		}
	fmt.Printf("\n##\n")	
}


