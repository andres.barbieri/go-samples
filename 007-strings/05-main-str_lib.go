// go run 05-main-str_lib.go string_lib_alt.go
package main

import "fmt"
import "strings"
import "unicode/utf8"

func main() {
	str  :=  "hola"
	var str1 string = "hola que tal la"
	var dst string
	str2 :=  `hola`
	var str4 string = "niñósS"
        var dst2 string
	
	fmt.Printf("len=%d %d count=%d\n",str_len(str),len(str),utf8.RuneCountInString(str))
	fmt.Printf("len=%d %d count=%d\n",str_len(str4),len(str4),utf8.RuneCountInString(str4))
	
	fmt.Printf("\"%s\" == \"%s\" (%d)\n",str1,str1,str_cmp(str1,str1))
	fmt.Printf("\"%s\" == \"%s\" (%d)\n",str,str4,str_cmp(str,str4))
	fmt.Printf("\"%s\" == \"%s\" (%d)\n",str4,str,str_cmp(str4,str))	
	fmt.Printf("\"%s\" == \"%s\" (%d)\n",str,str2,str_cmp(str2,str))	
	fmt.Printf("\"%s\" == \"%s\" (%d)\n",str1,dst,str_cmp(str1,dst))
	fmt.Printf("\"%s\" == \"%s\" (%d)\n",dst,str,str_cmp(dst,str))
}


