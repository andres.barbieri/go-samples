#include <stdio.h>
#include <string.h>
#include <stdlib.h>



size_t strlen_null(const char *s)
{
  if (s == NULL)
    return 0;
  else
    return strlen(s);
}

char *strcat_stuff(char *dest, const char *src, const char *stuff)
{
  if (dest == NULL)
    {
      return strcat(dest, src);
    }
  else
    {
      strcat(dest,"$");
      strcat(dest,src);
    }
}
    
int main(int argc, char *argv[])
{
  int i;
  char *res = NULL;

  for(i=1;i<argc;i++)
    {
      res = realloc( res, strlen_null(res) + 2 + strlen_null(argv[i]) );
      if (i == 1)
	{
	  strcat(res, argv[i]);
	}
      else
	{
	  strcat_stuff(res, argv[i], "$");
	}
    }
  printf("**%s**\n",res);
  exit(EXIT_SUCCESS);
}
