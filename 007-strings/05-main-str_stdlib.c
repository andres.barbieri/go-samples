#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>


char *str2lower(char str[]) {
  for(int i = 0; str[i]; i++){
    str[i] = tolower(str[i]);
  }
  return str;
}
  

char *str2upper(char str[]) {
  for(int i = 0; str[i]; i++){
    str[i] = toupper(str[i]);
  }
  return str;   
}

char *strrepeat (char *str, size_t count) {
    if (count == 0) return NULL;
    char *ret = malloc (strlen (str) * count + 1);
    if (ret == NULL) return NULL;
    ret[strlen (str) * count ] = 0;
    strcpy (ret, str);
    while (--count > 0) {
      //strcat (ret, " ");
        strcat (ret, str);
    }
    return ret;
}
int strcount(const char *haystack, const char *needle) {
    int count = 0;
    const char *tmp = haystack;
    while(tmp = strstr(tmp, needle))
    {
        count++;
        tmp++;
    }
    return count;
}
      
int main()
{
  char str[]  =  "hola";
  char str1[] = "hola que tal la";
  char dst[]  = "";
  char str2[] =  {'h','o','l','a',0};
  char str4[] = "niñósS";
  char *dst2 = NULL;
	  
  printf("len=%zd sizeof=%zd\n",strlen(str),sizeof(str));
  printf("len=%zd sizeof=%zd\n",strlen(str4),sizeof(str4));
	
  
  printf("\"%s\" == \"%s\" (%d)\n",str1,str1,strcmp(str1,str1));
  printf("\"%s\" == \"%s\" (%d)\n",str,str4,strcmp(str,str4));
  printf("\"%s\" == \"%s\" (%d)\n",str4,str,strcmp(str4,str));	
  printf("\"%s\" == \"%s\" (%d)\n",str,str2,strcmp(str2,str));	
  printf("\"%s\" == \"%s\" (%d)\n",str1,dst,strcmp(str1,dst));
  printf("\"%s\" == \"%s\" (%d)\n",dst,str,strcmp(dst,str));
    
  printf("\"%s\" in \"%s\" (%d)\n",str,str1,strstr(str1,str)!=NULL);
  printf("\"%s\" in \"%s\" (%d)\n",dst,str,strstr(str,dst)!=NULL);
  printf("\"%s\" in \"%s\" (%d)\n",str4,str,strstr(str,str4)!=NULL);
  
  printf("\"%s\" in \"%s\" (%d)\n","la",str1,strstr(str1,"la")!=NULL);

  printf("\"%s\" occurrs in \"%s\" (%d)\n","la",str1,strcount(str1,"la"));
  printf("\"%s\" occurrs in \"%s\" (%d)\n","que",str1,strcount(str1,"que"));
  
  printf("\"%s\" index \"%s\" (%zd)\n","que",str1,strstr(str1,"que")-str1);

  printf("\"%s\"\n",dst2 = strrepeat("A",10));
  free(dst2);
  //dst = strings.Replace(str1, "la", "LA", -1);
  //fmt.Println(dst);
  //fmt.Println(strings.Replace(str1, "la", "LA",  1))
  printf("\"%s\"\n",str2upper(str1));  
  printf("\"%s\"\n",str2lower(str1));

  // dst2 = strdup(str2)
  dst2 = malloc(sizeof(str2));
  strcpy(dst2,str2);

  printf("\"%s\" == \"%s\" (%d)\n",dst2,str,strcmp(str2,str));
  printf("\"%s\" == \"%s\" (%d)\n",dst2,str, (dst2 == str));
  free(dst2);
  printf("\"%s\" == \"%s\" (%d)\n",str,str,strcmp(str,str));
  printf("\"%s\" == \"%s\" (%d)\n",str,str, (str == str));

  printf("(%d)\n",strcount("11222221133333311eeeeeeffg111eeeeee511dddssds","qqq11"));
  return 0;
}
